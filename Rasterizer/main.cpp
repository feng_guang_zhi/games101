#define CVUI_IMPLEMENTATION

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "AppContext.hpp"
#include "DrawLineCmd.hpp"
#include "ShaderCmd.hpp"
#include "PrimitiveCmd.hpp"
#include "cvui.h"

using namespace cv;

#define UIWIND "UI"
#define DRAWWND "DRAW"

int main()
{
    int key = 0;
    int frameCount = 0;
    
    AppContext* globalAppContext = new AppContext();
    globalAppContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
    std::shared_ptr<ICommand> curCmd = nullptr;
    
    cv::Mat image(SCREEN_HEIGHT,SCREEN_WIDTH,CV_32FC3,globalAppContext->getRasterizer()->getFrameBuffer().data());
	
	cv::Mat frame = cv::Mat(700, 400, CV_8UC3);
    int count = 0;
 
    // Init a OpenCV window and tell cvui to use it.
    cv::namedWindow(UIWIND);
    cv::resizeWindow(UIWIND,700,200);
    cv::moveWindow(UIWIND,0,0);
    cv::namedWindow(DRAWWND);
    cv::resizeWindow(DRAWWND,SCREEN_WIDTH,SCREEN_HEIGHT);
    cv::moveWindow(DRAWWND,430,0);
    cvui::init(UIWIND);
    
    while (true) {
        
        frame = cv::Scalar(49, 52, 49);
    
        if (cvui::button(frame, 20, 30, "Line")) {
            count++;
            curCmd = CmdFactory::Create<DrawLineCmd>(globalAppContext);
            std::cout << (curCmd == nullptr) << std::endl;
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 60, "Circle")) {
            count++;
            curCmd = CmdFactory::Create<DrawCircleCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 90, "Ellipse")) {
            count++;
            curCmd = CmdFactory::Create<DrawEllipseCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 120, "Triangle Fill")) {
            count++;
            curCmd = CmdFactory::Create<TriangleFillCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 150, "Triangle Lerp")) {
            count++;
            curCmd = CmdFactory::Create<TriangleLerpCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 180, "Polygon Frame")) {
            count++;
            curCmd = CmdFactory::Create<PolygonFrameCmd>(globalAppContext);
            curCmd->Excute();
        }

        //正方形，长方形，凸多边形适用
        if (cvui::button(frame, 20, 210, "PolygonFillAET")) {
            count++;
            curCmd = CmdFactory::Create<PolygonFillAETCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 240, "PolygonFillBound")) {
            count++;
            curCmd = CmdFactory::Create<PolygonFillBoundCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 270, "PolygonFillSeed")) {
            count++;
            curCmd = CmdFactory::Create<PolygonFillSeedCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 300, "PolygonFillBound")) {
            count++;
            curCmd = CmdFactory::Create<PolygonFillBound8Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 330, "Transform2")) {
            count++;
            curCmd = CmdFactory::Create<Transform2Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 360, "Transform3")) {
            count++;
            curCmd = CmdFactory::Create<Transform3Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 390, "Projecttion-Perspective")) {
            count++;
            curCmd = CmdFactory::Create<ProjectionCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 420, "TriangleZBuffer")) {
            count++;
            curCmd = CmdFactory::Create<TriangleZBufferCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 450, "Bezier1")) {
            count++;
            curCmd = CmdFactory::Create<BezierCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 480, "Bezier2")) {
            count++;
            auto drawCircle = std::static_pointer_cast<BezierCmd>(curCmd);
            drawCircle->DrawCircle();
        }

        if (cvui::button(frame, 20, 510, "Bezier3")) {
            count++;
            curCmd = CmdFactory::Create<BezierPatchCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 540, "Bezier4")) {
            count++;
            curCmd = CmdFactory::Create<RationalBezierCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 570, "BezierSphere")) {
            count++;
            curCmd = CmdFactory::Create<BezierSphereCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 600, "CullCube")) {
            count++;
            curCmd = CmdFactory::Create<CubeCullCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 630, "CullFillCube")) {
            count++;
            curCmd = CmdFactory::Create<CubeCullFillCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 20, 660, "SphereFrame")) {
            count++;
            curCmd = CmdFactory::Create<SphereFrameCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 30, "SphereLight")) {
            count++;
            curCmd = CmdFactory::Create<LightingCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 60, "CubeLight")) {
            count++;
            curCmd = CmdFactory::Create<LightCubeCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 90, "Rabit")) {
            count++;
            curCmd = CmdFactory::Create<DrawRabitCmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 120, "Rabit2")) {
            count++;
            curCmd = CmdFactory::Create<DrawRabit2Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 150, "Rabit3")) {
            count++;
            curCmd = CmdFactory::Create<DrawRabit3Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 180, "Rabit4Normal")) {
            count++;
            curCmd = CmdFactory::Create<DrawRabit4Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 210, "Rabit5")) {
            count++;
            curCmd = CmdFactory::Create<DrawRabit5Cmd>(globalAppContext);
            curCmd->Excute();
        }

        if (cvui::button(frame, 150, 240, "diamond")) {
            count++;
            curCmd = CmdFactory::Create<DiamondCmd>(globalAppContext);
            curCmd->Excute();
        }
    
        cv::imshow(DRAWWND,image);
        cvui::update();
        cvui::imshow(UIWIND, frame);

        int key = cv::waitKey(30);
        if (key >= 0) 
        {  
            std::cout << "Key pressed: " << key << std::endl;
            if(curCmd != nullptr)
            {
                curCmd->OnKeyDown(key);
            }
            if (key == 27) 
            {  
                break;  
            }
        }
    }

    return 0;
}