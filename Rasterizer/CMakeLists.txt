cmake_minimum_required (VERSION 3.1)

project ("rasterizer")

if(CMAKE_COMPILER_IS_GNUCC)
    message("COMPILER IS GNUCC")    
    ADD_DEFINITIONS (-std=c++14)
endif(CMAKE_COMPILER_IS_GNUCC)

# OpenCV_DIR目录下要有OpenCVConfig.cmake，cmake才能找到opencv
set(OpenCV_DIR D:\\dev\\C++Lib\\OpenCV-MinGW-Build-OpenCV-4.5.2-x64\\x64\\mingw\\lib)
set(EIGEN_INCLUDE D:\\dev\\C++Lib\\eigen-3.4.0)
# find_package 会从 (PACKAGENAME)_DIR去找
find_package(OpenCV REQUIRED)

message(STATUS "opencv include path is: " ${OpenCV_INCLUDE_DIRS})
message(STATUS "opencv lib path is: " ${OpenCV_LIBS})

# 头文件目录
include_directories(include ${OpenCV_INCLUDE_DIRS} ${EIGEN_INCLUDE})

add_executable(rasterizer "src/Triangle.cpp" "src/Polygon.cpp" "src/Rasterizer.cpp" "src/LineRenderer.cpp"
"src/StackNode.cpp" "src/TriangleRenderer.cpp" "src/PolygonRenderer.cpp" "main.cpp"
"src/AppContext.cpp" "src/Pentagram.cpp" "src/Transform2.cpp" "src/Cube.cpp" 
"src/Transform3.cpp" "src/Projection.cpp" "src/CubicBezierCurve.cpp" "src/CubicBezierPatch.cpp"
"src/RationalBezierCurve.cpp" "src/BezierSphere.cpp" "src/Sphere.cpp"
"src/Sphere2.cpp" "src/Lighting.cpp" "src/LightSource.cpp" "src/Material.cpp"
"src/BezierMesh.cpp" "src/BezierSphereMesh.cpp" "src/Cube2.cpp" "src/Triangle2.cpp"
"src/ObjLoader.cpp" "src/Diamond.cpp")

# 添加opencv库文件目录
target_link_libraries(rasterizer ${OpenCV_LIBS})