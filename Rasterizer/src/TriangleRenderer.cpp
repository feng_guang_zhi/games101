#include "TriangleRenderer.hpp"

void TriangleRenderer::FillTriangle(Triangle& triangle,Vector3f color){
    triangle.sortY();
    int scanLineCnt = triangle.getC().y() - triangle.getA().y();
    spanLeft = new ColorVec3[scanLineCnt];
    spanRight = new ColorVec3[scanLineCnt];

    int delz = Util::Det(triangle.getA(),triangle.getB(),triangle.getC());
    if(delz > 0){
        edgeidx = 0;
        edgeFlag(triangle.getA(),triangle.getB(),true);
        edgeFlag(triangle.getB(),triangle.getC(),true);
        edgeidx = 0;
        edgeFlag(triangle.getA(),triangle.getC(),false);
    }else{
        edgeidx = 0;
        edgeFlag(triangle.getA(),triangle.getC(),true);
        edgeidx = 0;
        edgeFlag(triangle.getA(),triangle.getB(),false);
        edgeFlag(triangle.getB(),triangle.getC(),false);
    }

    for(int y = triangle.getA().y();y < (int)triangle.getC().y();y++){
        int i = y - triangle.getA().y();
        lineRender->MidPoint2(Vector3f(spanLeft[i].pt.x(),y,0),Vector3f(spanRight[i].pt.x(),y,0),color);
    }
    if(spanLeft){
        delete[] spanLeft;
        spanLeft = nullptr;
    }
    if(spanRight){
        delete[] spanRight;
        spanRight = nullptr;
    }
}

void TriangleRenderer::edgeFlag(const Vector3f& start,const Vector3f& end,bool isLeft){
    auto dx = end.x() - start.x();
    auto dy = end.y() - start.y();
    auto m = dx / dy;
    auto x = start.x();
    for(int y = start.y();y < (int)end.y();y++){
        if(isLeft){
            spanLeft[edgeidx++] = ColorVec3(Vector3f(round(x),y,0));
        }else{
            spanRight[edgeidx++] = ColorVec3(Vector3f(round(x),y,0));
        }
        x += m;
    }
}

void TriangleRenderer::FillLerp(Triangle& triangle){
    triangle.sortY();
    int scanLineCnt = triangle.getC().y() - triangle.getA().y();
    spanLeft = new ColorVec3[scanLineCnt];
    spanRight = new ColorVec3[scanLineCnt];

    int delz = Util::Det(triangle.getA(),triangle.getB(),triangle.getC());
    if(delz > 0){
        edgeidx = 0;
        edgeFlagLerp(triangle.getCA(),triangle.getCB(),true);
        edgeFlagLerp(triangle.getCB(),triangle.getCC(),true);
        edgeidx = 0;
        edgeFlagLerp(triangle.getCA(),triangle.getCC(),false);
    }else{
        edgeidx = 0;
        edgeFlagLerp(triangle.getCA(),triangle.getCC(),true);
        edgeidx = 0;
        edgeFlagLerp(triangle.getCA(),triangle.getCB(),false);
        edgeFlagLerp(triangle.getCB(),triangle.getCC(),false);
    }

    for(int y = triangle.getA().y();y < (int)triangle.getC().y();y++){
        int i = y - triangle.getA().y();
        for(int x = spanRight[i].pt.x();x < (int)spanLeft[i].pt.x();x++){
            auto color = Util::colorInterpret(x,spanLeft[i].pt.x(),spanRight[i].pt.x(),spanLeft[i].color,spanRight[i].color);
            rasterizer->setPixel(Vector3f(x,y,0),color);
        }
    }
    if(spanLeft){
        delete[] spanLeft;
        spanLeft = nullptr;
    }
    if(spanRight){
        delete[] spanRight;
        spanRight = nullptr;
    }
}

void TriangleRenderer::edgeFlagLerp(const ColorVec3& start,const ColorVec3& end,bool isLeft){
    auto dx = end.pt.x() - start.pt.x();
    auto dy = end.pt.y() - start.pt.y();
    auto m = dx / dy;
    auto x = start.pt.x();
    for(int y = start.pt.y();y < (int)end.pt.y();y++){
        auto color = Util::colorInterpret(y,start.pt.y(),end.pt.y(),start.color,end.color);
        if(isLeft){
            spanLeft[edgeidx++] = ColorVec3(Vector3f(round(x),y,0),color);
        }else{
            spanRight[edgeidx++] = ColorVec3(Vector3f(round(x),y,0),color);
        }
        x += m;
    }
}

void TriangleRenderer::FillTriangleBox(Triangle& triangle)
{
    auto a3 = triangle.getA();
    auto b3 = triangle.getB();
    auto c3 = triangle.getC();
    auto a2 = Vector2f(a3.x(),a3.y());
    auto b2 = Vector2f(b3.x(),b3.y());
    auto c2 = Vector2f(c3.x(),c3.y());
    int xMin = std::round(std::min(std::min(a3.x(),b3.x()),c3.x()));
    int yMin = std::round(std::min(std::min(a3.y(),b3.y()),c3.y()));
    int xMax = std::round(std::max(std::max(a3.x(),b3.x()),c3.x()));
    int yMax = std::round(std::max(std::max(a3.y(),b3.y()),c3.y()));

    std::cout << xMin << " " << xMax << "  " << yMin << "  " << yMax << std::endl;
    // 遍历包围盒内的每个像素
    for (int y = yMin; y <= yMax; y++) 
    {
        for (int x = xMin; x <= xMax; x++) 
        {
            // 判断像素是否在三角形内部
            if (Util::isPointInTriangle(Vector2f(x, y), a2, b2, c2)) {
                // 填充像素
                 rasterizer->setPixel(x,y,Vector3f(1,1,0));
            }
        }
    }
}

void TriangleRenderer::FillTriangleBoxZBuffer(Triangle& triangle)
{
    auto a3 = triangle.getA();
    auto b3 = triangle.getB();
    auto c3 = triangle.getC();
    auto a2 = Vector2f(a3.x(),a3.y());
    auto b2 = Vector2f(b3.x(),b3.y());
    auto c2 = Vector2f(c3.x(),c3.y());
    int xMin = std::round(std::min(std::min(a3.x(),b3.x()),c3.x()));
    int yMin = std::round(std::min(std::min(a3.y(),b3.y()),c3.y()));
    int xMax = std::round(std::max(std::max(a3.x(),b3.x()),c3.x()));
    int yMax = std::round(std::max(std::max(a3.y(),b3.y()),c3.y()));

    std::cout << xMin << " " << xMax << "  " << yMin << "  " << yMax << std::endl;
    // 遍历包围盒内的每个像素
    for (int y = yMin; y <= yMax; y++) 
    {
        for (int x = xMin; x <= xMax; x++) 
        {
            auto area = 0.5 * Util::sign(a2,b2,c2);
            auto area1 = 0.5 * Util::sign(Vector2f(x,y),b2,c2);
            auto area2 = 0.5 * Util::sign(Vector2f(x,y),a2,c2);
            //std::cout << area << " " << area1 << "  " << area2 << std::endl;
            auto alpha = area1 / area;
            auto beta = area2 / area;
            auto gamma = 1 - alpha - beta;
            if(alpha >=0 && beta >= 0 && gamma >= 0)
            {
                auto color = alpha * triangle.getAColor() + beta * triangle.getBColor() + (1 - alpha - beta) * triangle.getCColor();
                auto depth = alpha * a3.z() + beta * b3.z() + (1 - alpha - beta) * c3.z();
                if(depth <= rasterizer->getZBuffer(x,y))
                {
                    rasterizer->setZBuffer(x,y,depth);
                    rasterizer->setPixel(x,y,color);
                }
            }
        }
    }
}

void TriangleRenderer::FillTriangleBoxLerp(Triangle& triangle)
{
    auto a3 = triangle.getA();
    auto b3 = triangle.getB();
    auto c3 = triangle.getC();
    auto p0 = Vector2f(a3.x(),a3.y());
    auto p1 = Vector2f(b3.x(),b3.y());
    auto p2 = Vector2f(c3.x(),c3.y());
    int xMin = std::round(std::min(std::min(a3.x(),b3.x()),c3.x()));
    int yMin = std::round(std::min(std::min(a3.y(),b3.y()),c3.y()));
    int xMax = std::round(std::max(std::max(a3.x(),b3.x()),c3.x()));
    int yMax = std::round(std::max(std::max(a3.y(),b3.y()),c3.y()));

    for (int y = yMin; y <= yMax; y++) 
    {
        for (int x = xMin; x <= xMax; x++) 
        {
            auto area = p0.x() * p1.y() + p1.x() * p2.y() + p2.x() * p0.y() - p2.x() * p1.y() - p1.x() * p0.y() -  p0.x() * p2.y();
            auto area1 = x * p1.y() + p1.x() * p2.y() + p2.x() * y - p2.x() * p1.y() - p1.x() * y -  x * p2.y();
            auto area2 = p0.x() * y + x * p2.y() + p2.x() * p0.y() - p2.x() * y - x * p0.y() -  p0.x() * p2.y();
            auto area3 = p0.x() * p1.y() + p1.x() * y + x * p0.y() - x * p1.y() - p1.x() * p0.y() -  p0.x() * y;
            auto alpha = area1 / area;
            auto beta = area2 / area;
            auto gamma = area3 / area;
            if(alpha >=0 && beta >= 0 && gamma >= 0)
            {
                auto color = alpha * triangle.getAColor() + beta * triangle.getBColor() + gamma * triangle.getCColor();
                rasterizer->setPixel(x,y,color);
            }
        }
    }
}

void TriangleRenderer::SetTriangleVertData(VertexData* vertData)
{
    for(int i = 0;i < triData.size();i++)
    {
        triData[i] = vertData[i];
    }
}
void TriangleRenderer::PhongShader(Vector3f eys,Lighting* lighting)
{
    auto a3 = triData[0].pt;
    auto b3 = triData[1].pt;
    auto c3 = triData[2].pt;
    auto p0 = Vector2f(a3.x(),a3.y());
    auto p1 = Vector2f(b3.x(),b3.y());
    auto p2 = Vector2f(c3.x(),c3.y());
    int xMin = std::round(std::min(std::min(a3.x(),b3.x()),c3.x()));
    int yMin = std::round(std::min(std::min(a3.y(),b3.y()),c3.y()));
    int xMax = std::round(std::max(std::max(a3.x(),b3.x()),c3.x()));
    int yMax = std::round(std::max(std::max(a3.y(),b3.y()),c3.y()));

    for (int y = yMin; y <= yMax; y++) 
    {
        for (int x = xMin; x <= xMax; x++) 
        {
            auto area = p0.x() * p1.y() + p1.x() * p2.y() + p2.x() * p0.y() - p2.x() * p1.y() - p1.x() * p0.y() -  p0.x() * p2.y();
            auto area1 = x * p1.y() + p1.x() * p2.y() + p2.x() * y - p2.x() * p1.y() - p1.x() * y -  x * p2.y();
            auto area2 = p0.x() * y + x * p2.y() + p2.x() * p0.y() - p2.x() * y - x * p0.y() -  p0.x() * p2.y();
            auto area3 = p0.x() * p1.y() + p1.x() * y + x * p0.y() - x * p1.y() - p1.x() * p0.y() -  p0.x() * y;
            auto alpha = area1 / area;
            auto beta = area2 / area;
            auto gamma = area3 / area;
            if(alpha >=0 && beta >= 0 && gamma >= 0)
            {
                auto color = alpha * triData[0].color + beta * triData[1].color + gamma * triData[2].color;
                double zDepth = alpha * triData[0].pt.z() + beta * triData[1].pt.z() + gamma * triData[2].pt.z();
                Vector3f ptNormal = alpha * triData[0].normal + beta * triData[1].normal + gamma * triData[2].normal;
                auto color2 = lighting->Illuminate(eys,Vector3f(x,y,zDepth),color,ptNormal,lighting->getMaterial());
                //std::cout << "light color : " << color2 << " " << zDepth << std::endl;
                if(zDepth <= rasterizer->getZBuffer(x,y))
                {
                    rasterizer->setZBuffer(x,y,zDepth);
                    rasterizer->setPixel(x,y,color2);
                }
                
            }
        }
    }
}