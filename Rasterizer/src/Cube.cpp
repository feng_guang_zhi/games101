#include "Cube.hpp"

void Cube::OnDraw(AppContext* context)
{
    auto lineRenderer = context->getLineRenderer();
    auto color = Vector3f(0,0,1);
    const int faceNum = faces.size();
    Vector2f screenPts[4];
    for(int nFace = 0;nFace < faceNum;nFace++)
    {
        for(int nPt = 0;nPt < faces[nFace].vertIdxs.size();nPt++)
        {
            screenPts[nPt] = Vector2f(pts[faces[nFace].vertIdxs[nPt]].x(),pts[faces[nFace].vertIdxs[nPt]].y());
        }
        for(int i = 0;i < 4;i++)
        {
            auto startPt = Vector3f(screenPts[i % 4].x(),screenPts[i % 4].y(),0);
            auto endPt = Vector3f(screenPts[(i + 1) % 4].x(),screenPts[(i + 1) % 4].y(),0);
            lineRenderer->MidPoint2(startPt,endPt,color);
        }
        
    }
}

void Cube::ReadVertex()
{
    pts.push_back(Vector3f(0,0,0));
    pts.push_back(Vector3f(1,0,0));
    pts.push_back(Vector3f(1,1,0));
    pts.push_back(Vector3f(0,1,0));
    pts.push_back(Vector3f(0,0,1));
    pts.push_back(Vector3f(1,0,1));
    pts.push_back(Vector3f(1,1,1));
    pts.push_back(Vector3f(0,1,1));

    transform.SetVerts(&pts);
}

void Cube::ReadVertColor()
{
    verts.push_back({Vector3f(0,0,0),Vector3f(0,0,0)});
    verts.push_back({Vector3f(1,0,0),Vector3f(1,0,0)});
    verts.push_back({Vector3f(1,1,0),Vector3f(1,1,0)});
    verts.push_back({Vector3f(0,1,0),Vector3f(0,1,0)});
    verts.push_back({Vector3f(0,0,1),Vector3f(0,0,1)});
    verts.push_back({Vector3f(1,0,1),Vector3f(1,0,1)});
    verts.push_back({Vector3f(1,1,1),Vector3f(1,1,1)});
    verts.push_back({Vector3f(0,1,1),Vector3f(0,1,1)});

    transform.SetVertDatas(&verts);
}

void Cube::ReadFaces()
{
    faces.push_back(Face4(4,5,6,7));
    faces.push_back(Face4(0,3,2,1));
    faces.push_back(Face4(0,4,7,3));
    faces.push_back(Face4(1,2,6,5));
    faces.push_back(Face4(2,3,7,6));
    faces.push_back(Face4(0,1,5,4));
}

void Cube::DrawAxis(AppContext* context,const Vector3f& start,const Vector3f& end)
{
    auto lineRenderer = context->getLineRenderer();
    auto color = Vector3f(0,1,0);
    lineRenderer->MidPoint2(start,end,color);
}

void Cube::DrawCube(AppContext* context,const ProjectionType projectionType)
{
    auto color = Vector3f(0,0,1);
    const int faceNum = faces.size();
    Vector2f screenPts[4];
    auto projection = context->getProjection();
    auto lineRenderer = context->getLineRenderer();
    for(int nFace = 0;nFace < faceNum;nFace++)
    {
        for(int nPt = 0;nPt < faces[nFace].vertIdxs.size();nPt++)
        {
            if(projectionType == ProjectionType::Orthogonal)
            {
                screenPts[nPt] = projection->OrthogonalProjection(pts[faces[nFace].vertIdxs[nPt]]);
            }
            else if(projectionType == ProjectionType::Perspective)
            {
                screenPts[nPt] = projection->PerspectiveProjection(pts[faces[nFace].vertIdxs[nPt]]);
            }
        }
        for(int i = 0;i < 4;i++)
        {
            auto startPt = Vector3f(screenPts[i % 4].x(),screenPts[i % 4].y(),0);
            auto endPt = Vector3f(screenPts[(i + 1) % 4].x(),screenPts[(i + 1) % 4].y(),0);
            lineRenderer->MidPoint2(startPt,endPt,color);
        }
        
    }
}

void Cube::DrawCubeCull(AppContext* context)
{
    auto color = Vector3f(0,0,1);
    const int faceNum = faces.size();
    Vector2f screenPts[4];
    auto projection = context->getProjection();
    auto eye = projection->getEye();
    auto lineRenderer = context->getLineRenderer();
    for(int nFace = 0;nFace < faceNum;nFace++)
    {
        Vector3f viewDir = pts[faces[nFace].vertIdxs[0]] - eye;
        viewDir = viewDir.normalized();
        Vector3f a1 = pts[faces[nFace].vertIdxs[0]] - pts[faces[nFace].vertIdxs[1]];
        Vector3f a2 = pts[faces[nFace].vertIdxs[0]] - pts[faces[nFace].vertIdxs[2]];
        Vector3f faceNormal = a1.cross(a2);
        faceNormal = faceNormal.normalized();
        if(viewDir.dot(faceNormal) >= 0)
        {
            for(int nPt = 0;nPt < faces[nFace].vertIdxs.size();nPt++)
            {
                screenPts[nPt] = projection->PerspectiveProjection(pts[faces[nFace].vertIdxs[nPt]]);
            }
            for(int i = 0;i < 4;i++)
            {
                auto startPt = Vector3f(screenPts[i % 4].x(),screenPts[i % 4].y(),0);
                auto endPt = Vector3f(screenPts[(i + 1) % 4].x(),screenPts[(i + 1) % 4].y(),0);
                lineRenderer->MidPoint2(startPt,endPt,color);
            }
        }
        
        
    }
}

void Cube::DrawCubeFill(AppContext* context)
{
    auto color = Vector3f(0,0,1);
    const int faceNum = faces.size();
    Vector2f screenPts[4];
    auto projection = context->getProjection();
    auto eye = projection->getEye();
    auto triRenderer = context->getTriangleRender();
    auto lineRenderer = context->getLineRenderer();

    for(int nFace = 0;nFace < faceNum;nFace++)
    {
        Vector3f viewDir = verts[faces[nFace].vertIdxs[0]].pt - eye;
        viewDir = viewDir.normalized();
        Vector3f a1 = verts[faces[nFace].vertIdxs[0]].pt - verts[faces[nFace].vertIdxs[1]].pt;
        Vector3f a2 = verts[faces[nFace].vertIdxs[0]].pt - verts[faces[nFace].vertIdxs[2]].pt;
        Vector3f faceNormal = a1.cross(a2);
        faceNormal = faceNormal.normalized();
        if(viewDir.dot(faceNormal) >= 0)
        {
            for(int nPt = 0;nPt < faces[nFace].vertIdxs.size();nPt++)
            {
                screenPts[nPt] = projection->PerspectiveProjection(verts[faces[nFace].vertIdxs[nPt]].pt);
            }
            //Triangle triangle1(screenPts[0],screenPts[2],screenPts[3],verts[faces[nFace].vertIdxs[0]].color,verts[faces[nFace].vertIdxs[2]].color,verts[faces[nFace].vertIdxs[3]].color);
            Triangle triangle1(screenPts[0],screenPts[1],screenPts[2],Vector3f(1,0,0),Vector3f(0,1,0),Vector3f(0,0,1));
            triRenderer->FillTriangleBoxLerp(triangle1);
            //Triangle triangle2(screenPts[0],screenPts[1],screenPts[2],verts[faces[nFace].vertIdxs[0]].color,verts[faces[nFace].vertIdxs[1]].color,verts[faces[nFace].vertIdxs[2]].color);
            Triangle triangle2(screenPts[2],screenPts[3],screenPts[0],Vector3f(1,0,0),Vector3f(0,1,0),Vector3f(0,0,1));
            triRenderer->FillTriangleBoxLerp(triangle2);

            for(int i = 0;i < 4;i++)
            {
                auto startPt = Vector3f(screenPts[i % 4].x(),screenPts[i % 4].y(),0);
                auto endPt = Vector3f(screenPts[(i + 1) % 4].x(),screenPts[(i + 1) % 4].y(),0);
                lineRenderer->MidPoint2(startPt,endPt,color);
            }
        }
    }
}