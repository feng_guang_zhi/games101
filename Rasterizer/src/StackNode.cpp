#include "StackNode.hpp"

void StackNode::push(StackNode* headNode,Vector2f pt)
{
    auto cur = headNode;
	auto last = new StackNode;
	last->pixelPoint = pt;
	last->next = nullptr;
	if(cur == nullptr)
	{
		headNode = last;
		return;
	}
	while(cur->next != nullptr)
	{
		cur = cur->next;
	}
	cur->next = last;
}

//pop 后还得 delete 掉
StackNode* StackNode::pop(StackNode* headNode)
{
    auto cur = headNode;
    if(cur == nullptr)
    {
        return nullptr;
    }
    StackNode* last = nullptr;
	if(cur->next == nullptr)
	{
		headNode = nullptr;
		return cur;
	}
	while(cur->next != nullptr)
	{
		last = cur;
        cur = cur->next;
	}
    last->next = nullptr;
    return cur;
}