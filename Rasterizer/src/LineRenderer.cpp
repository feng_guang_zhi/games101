#include"LineRenderer.hpp"

const float PI = 3.1415926;

void LineRenderer::DDALine(Vector3f start,Vector3f end){
    int dx = round(end.x() - start.x());
    int dy = round(end.y() - start.y());
    int eps = std::max(abs(dx),abs(dy));
    float stepX = (float)dx / eps;
    float stepY = (float)dy / eps;
    Vector3f color(1.0f,0.0f,0.0f);
    Vector3f sx(start.x(),start.y(),start.z());
    for(int i = 1;i <= eps;i++){
        rasterizer->setPixel(sx,color);
        sx += Vector3f(stepX,stepY,0.0f);
    }
}

//单象限的
void LineRenderer::MidBresenhamLine1(Vector3f start,Vector3f end,Vector3f color){
    if(start.x() > end.x()){
        Vector3f tmp = start;
        start = end;
        end = tmp;
    }
    int dx = round(end.x() - start.x());
    int dy = round(end.y() - start.y());
    int d = dx - 2 * dy;
    int upInc = 2 * dx - 2 * dy;
    int downInc = -2 * dy;
    int sx = start.x();
    int sy = start.y();
    int ex = end.x();
    int ey = end.y();
    while(sx <= ex){
        rasterizer->setPixel(Vector3f(sx,sy,0),color);
        sx++;
        if(d < 0){
            sy++;
            d += upInc;
        }else{
            d += downInc;
        }
    }
}

void LineRenderer::MidPoint(Vector3f start,Vector3f end, Vector3f color){
    int dx = end.x() - start.x();
    int dy = end.y() - start.x();
    //即斜率 k <= 1的情况
    if(dx >= dy){
        int d = dy - dx / 2;
        int sx = start.x();
        int sy = start.y();
        while(sx <= end.x()){
            rasterizer->setPixel(Vector3f(sx,sy,0),color);
            sx++;
            if(d < 0){
                d += dy;
            }else{
                d += (dy - dx);
                sy++;
            }
        }
    }else{
        int d = dx - dy / 2;
        int sx = start.x();
        int sy = start.y();
        while(sy <= end.y()){
            rasterizer->setPixel(Vector3f(sx,sy,0),color);
            sy++;
            if(d < 0){
                d += dx;
            }else{
                d += (dx - dy);
                sx++;
            }
        }
    }
}

void LineRenderer::MidPoint2(Vector3f start,Vector3f end, Vector3f color){
    int dx = abs(end.x() - start.x());
    int dy = abs(end.y() - start.y());
    bool changed = false;
    int stepX = (end.x() > start.x()) ? 1 : (end.x() < start.x()) ? -1 : 0; 
    int stepY = (end.y() > start.y()) ? 1 : (end.y() < start.y()) ? -1 : 0;
    //即 斜率 k > 1时，y 方向为主位移方向
    if(dy > dx){
        std::swap(dx,dy);
        changed = true;
    }
    int e = dx - 2 * dy;
    int lx = start.x();
    int ly = start.y();
    for(int i = 1;i <= dx;i++){
        rasterizer->setPixel(Vector3f(lx,ly,0),color);
        if(changed){
            ly += stepY;
        }else{
            lx += stepX;
        }
        //e 小于0 说明中点在直线的下方
        //那么直线更靠近 y + 1,像素点应该取为y+1
        if(e < 0){
            if(changed){
                lx += stepX;
            }else{
                ly += stepY;
            }
            e += 2 * dx - 2 * dy; 
        }else{
            e -= 2 * dy;
        }
    }
}

void LineRenderer::Fllower(Vector3f center,float radius){
   int delta = 10;
   for(int i = 0;i <= 360;i += delta){
        float ex = center.x() + radius * cos((float)i / 180.0f * PI);
        float ey = center.y() + radius * sin((float)i / 180.0f * PI);
        //MidBresenhamLine1(center,Vector3f(ex,ey,0),Vector3f(1.0f,0.0f,0.0f));
        //DDALine(center,Vector3f(ex,ey,0));
        MidPoint2(center,Vector3f(ex,ey,0),Vector3f(1.0f,0.0f,0.0f));
   }
}

void LineRenderer::MidPointCircle(Vector3f center,float radius,Vector3f color){
    int e = 1 - (int)radius;
    int x = 0;
    int y = (int)radius;
    for(;x <= y;x++){
        CirclePoints(center,x,y,color);
        if(e < 0){
            e += 2 * x + 3;
        }else{
            e += 2 * (x - y) + 5;
            y--;
        }
    }
}

void LineRenderer::CirclePoints(Vector3f center,int x,int y,Vector3f color){
    rasterizer->setPixel(center + Vector3f(x,y,0),color);
    rasterizer->setPixel(center + Vector3f(y,x,0),color);
    rasterizer->setPixel(center + Vector3f(y,-x,0),color);
    rasterizer->setPixel(center + Vector3f(x,-y,0),color);

    rasterizer->setPixel(center + Vector3f(-x,-y,0),color);
    rasterizer->setPixel(center + Vector3f(-y,-x,0),color);
    rasterizer->setPixel(center + Vector3f(-y,x,0),color);
    rasterizer->setPixel(center + Vector3f(-x,y,0),color);
}

void LineRenderer::FlowCircle(Vector3f center,Vector3f color){
    int r = 20;
    int er = 280;
    for(;r <= er;r += 20){
        MidPointCircle(center,r,color);
    }
}

 void LineRenderer::MidPointEllipse(Vector3f center,float a,float b,Vector3f color,float theta){
    float x,y,d1,d2;
    x = 0;
    y = b;
    d1 = b * b + a * a * (-b + 0.25);
    EllipsePoints(center,x,y,color);
    while(b * b * (x + 1) < a * a * (y - 0.5)){
        if(d1 < 0){
            d1 += b * b * (2 * x + 3);
        }else{
            d1 += b * b * (2 * x + 3) + a * a * (-2 * y + 2);
            y--;
        }
        x++;
        EllipsePoints(center,x,y,color);
    }
    d2 = pow(b,2) * pow((x + 0.5),2) + pow(a,2) * pow((y - 1),2) - pow(a,2) * pow(b,2);
    while(y > 0){
        if(d2 < 0){
            d2 += b * b * (2 * x + 2) + a * a * (-2 * y + 3);
            x++;
        }else{
            d2 += a * a * (-2 * y + 3);
        }
        y--;
        EllipsePoints(center,x,y,color);
    }
    
 }

 void LineRenderer::EllipsePoints(Vector3f center,int x,int y,Vector3f color){
    rasterizer->setPixel(center + Vector3f(x,y,0),color);
    rasterizer->setPixel(center + Vector3f(-x,y,0),color);
    rasterizer->setPixel(center + Vector3f(x,-y,0),color);
    rasterizer->setPixel(center + Vector3f(-x,-y,0),color);
 }

 Vector3f LineRenderer::LinearInterp(double m,double start,double end,Vector3f cstart,Vector3f cend)
 {
    auto c = (start - m) / (end - start) * cstart + (m - start) / (end - start) * cend;
    return c;
 }

 void LineRenderer::MidPointLerp(Vector3f start,Vector3f end, Vector3f startc,Vector3f endc)
 {
    auto endx = std::round(end.x());
    auto endy = std::round(end.y());
    auto startx = std::round(start.x());
    auto starty = std::round(start.y());
    int dx = std::abs(endx - startx);
    int dy = std::abs(endy - starty);
    bool interchange = false;

    int signX = (end.x() > start.x()) ? 1 : (end.x() < start.x()) ? -1 : 0;
    int signY = (end.y() > start.y()) ? 1 : (end.y() < start.y()) ? -1 : 0;
    if(dy > dx)
    {
        int temp = dy;
        dy = dx;
        dx = temp;
        interchange = true;
    }
    int e = dx - 2 * dy;
    //从起点开始绘制直线
    Vector3f pt = start;
    Vector3f pcolor = startc;
    for(int i = 1;i <= dx;i++)
    {
        rasterizer->setPixel(Vector3f(pt.x(),pt.y(),0),pcolor);
        if(interchange)
        {
            pt += Vector3f(0,signY,0);
            pcolor = LinearInterp(pt.y(),start.y(),end.y(),startc,endc);
        }
        else
        {
            pt += Vector3f(signX,0,0);
            pcolor = LinearInterp(pt.y(),start.y(),end.y(),startc,endc);
        }
        if(e < 0)
        {
            if(interchange)
            {
                pt += Vector3f(signX,0,0);
            }
            else
            {
                pt += Vector3f(0,signX,0);
            }
            e += 2 * dx - 2 * dy;
        }
        else
        {
            e -= 2 * dy;
        }
    }
 }