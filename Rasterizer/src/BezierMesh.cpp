#include "BezierMesh.hpp"

BezierMesh::BezierMesh()
{

}

BezierMesh::~BezierMesh()
{

}

void BezierMesh::ReadCoffients()
{
    coffients << -1,3,-3,1,
                 3,-6,3,0,
                 -3,3,0,0,
                 1,0,0,0;
}

void BezierMesh::ReadCtrlPtsMat(const Eigen::Matrix<Vector3f, 4, 4>& mat)
{
    for(int i = 0;i < matrix.rows();i++)
    {
        for(int j = 0;j < matrix.cols();j++)
        {
            matrix(i,j) = mat(i,j);
        }
    }
}

Eigen::Matrix<Vector3f, 4, 4> BezierMesh::leftMultiplyMat(const Eigen::Matrix<double, 4, 4>& cMat,const Eigen::Matrix<Vector3f, 4, 4>& ptMat)
{
    Eigen::Matrix<Vector3f, 4, 4> res;
    for(int i = 0;i < ptMat.rows();i++)
    {
        for(int j = 0;j < ptMat.cols();j++)
        {
            res(i,j) = cMat(i,0) * ptMat(0,j) + cMat(i,1) * ptMat(1,j) + cMat(i,2) * ptMat(2,j) + cMat(i,3) * ptMat(3,j);
        }
    }
    return res;
}

Eigen::Matrix<Vector3f, 4, 4> BezierMesh::rightMultiplyMat(const Eigen::Matrix<Vector3f, 4, 4>& ptMat,const Eigen::Matrix<double, 4, 4>& cMat)
{
    Eigen::Matrix<Vector3f, 4, 4> res;
    for(int i = 0;i < ptMat.rows();i++)
    {
        for(int j = 0;j < ptMat.cols();j++)
        {
            res(i,j) = ptMat(i,0) * cMat(0,j) + ptMat(i,1) * cMat(1,j) + ptMat(i,2) * cMat(2,j) + ptMat(i,3) * cMat(3,j);
        }
    }
    return res;
}