#include "RationalBezierCurve.hpp"

void RationalBezierCurve::ReadCtrlPts()
{
    double radius = 200;
    ctrlPts.push_back({radius,0});
    ctrlPts.push_back({radius,radius});
    ctrlPts.push_back({0,radius});
    ctrlPts.push_back({-radius,radius});

    ctrlPts.push_back({-radius,0});
    ctrlPts.push_back({-radius,-radius});
    ctrlPts.push_back({0,-radius});
    ctrlPts.push_back({radius,-radius});

    for(auto& pv : ctrlPts){
        pv += Vector2f(200,200);
    }

    weights.resize(3);
    weights[0] = weights[2] = 1;
    weights[1] = std::sqrt(2.0) / 2.0;

    Pts.resize(4);
}

void RationalBezierCurve::DrawCurve(AppContext* context)
{
    auto color = Vector3f(0,0,1);
    auto lineRenderer = context->getLineRenderer();
    double tStep = 0.01;
    double bern02,bern12,bern22,denominator,tx,ty;
    Vector3f last;
    bool startFlag = false;
    for(double t = 0.0;t <= 1;t += tStep)
    {
        bern02 = (1 - t) * (1 - t);
        bern12 = 2 * t * (1 - t);
        bern22 = t * t;
        denominator = bern02 * weights[0] + bern12 * weights[1] + bern22 * weights[2];
        tx = (bern02 * Pts[0].x() * weights[0] + bern12 * Pts[1].x() * weights[1] + bern22 * Pts[2].x() * weights[2]) / denominator;
        ty = (bern02 * Pts[0].y() * weights[0] + bern12 * Pts[1].y() * weights[1] + bern22 * Pts[2].y() * weights[2]) / denominator;
        if(!startFlag)
        {
            last = Vector3f(std::round(tx),std::round(ty),0);
            startFlag = true;
        }
        else
        {
            auto end = Vector3f(tx,ty,0);
            lineRenderer->MidPoint2(last,end,color);
            last = end;
        }
    }
}

void RationalBezierCurve::DrawCirle(AppContext* context)
{
    Pts[0] = ctrlPts[0];
    Pts[1] = ctrlPts[1];
    Pts[2] = ctrlPts[2];
    DrawCurve(context);

    Pts[0] = ctrlPts[2];
    Pts[1] = ctrlPts[3];
    Pts[2] = ctrlPts[4];
    DrawCurve(context);

    Pts[0] = ctrlPts[4];
    Pts[1] = ctrlPts[5];
    Pts[2] = ctrlPts[6];
    DrawCurve(context);

    Pts[0] = ctrlPts[6];
    Pts[1] = ctrlPts[7];
    Pts[2] = ctrlPts[0];
    DrawCurve(context);
}