#include "Sphere2.hpp"

 Sphere2::Sphere2()
 {

 }

Sphere2::~Sphere2()
{
    
}

void Sphere2::ReadVertex()
{
    verts.clear();
    const double m = 0.5532;
    //第一卦限
    verts.push_back({0,1,0});
    verts.push_back({0,1,m});
    verts.push_back({0,m,1});
    verts.push_back({0,0,1});
    verts.push_back({m * m,1,m});
    verts.push_back({m,m,1});
    verts.push_back({m,0,1});
    verts.push_back({m,1,m * m});
    verts.push_back({1,m,m});
    verts.push_back({1,0,m});
    verts.push_back({m,1,0});
    verts.push_back({1,m,0});
    verts.push_back({1,0,0});
    //第二卦限
    verts.push_back({m,1,-m * m});
    verts.push_back({1,m,-m});
    verts.push_back({1,0,-m});
    verts.push_back({m * m,1,-m});
    verts.push_back({m,m,-1});
    verts.push_back({m,0,-1});
    verts.push_back({0,1,-m});
    verts.push_back({0,m,-1});
    verts.push_back({0,0,-1});
    //第三卦限
    verts.push_back({-m * m,1,-m});
    verts.push_back({-m,m,-1});
    verts.push_back({-m,0,-1});
    verts.push_back({-m,1,-m * m});
    verts.push_back({-1,m,-m});
    verts.push_back({-1,0,-m});
    verts.push_back({-m,1,0});
    verts.push_back({-1,m,0});
    verts.push_back({-1,0,0});
    //第四卦限
    verts.push_back({-m,1,m * m});
    verts.push_back({-1,m,m});
    verts.push_back({-1,0,m});
    verts.push_back({-m * m,1,m});
    verts.push_back({-m,m,1});
    verts.push_back({-m,0,1});
    //第五卦限
    verts.push_back({0,-m,1});
    verts.push_back({0,-1,m});
    verts.push_back({m,-m,1});
    verts.push_back({m * m,-1,m});
    verts.push_back({1,-m,m});
    verts.push_back({m,-1,m * m});
    verts.push_back({1,-m,0});
    verts.push_back({m,-1,0});
    //第六卦限
    verts.push_back({1,-m,-m});
    verts.push_back({m,-1,-m * m});
    verts.push_back({m,-m,-1});
    verts.push_back({m * m,-1,-m});
    verts.push_back({0,-m,-1});
    verts.push_back({0,-1,-m});
    //第七卦限
    verts.push_back({-m,-m,-1});
    verts.push_back({-m * m,-1,-m});
    verts.push_back({-1,-m,-m});
    verts.push_back({-m,-1,-m * m});
    verts.push_back({-1,-m,0});
    verts.push_back({-m,-1,0});
    //第八卦限
    verts.push_back({-1,-m,m});
    verts.push_back({-m,-1,m * m});
    verts.push_back({-m,-m,1});
    verts.push_back({-m * m,-1,m});
    verts.push_back({0,-1,0});
    
    transform.SetVerts(&verts);
}

void Sphere2::ReadPatch()
{
    faces.clear();
    //第一卦限面片
    Patch2<4,4> p0;
    p0.ptIndex << 3,2,1,0,
                  6,5,4,0,
                  9,8,7,0,
                  12,11,10,0;
    faces.push_back(p0);
    //第二卦限面片
    Patch2<4,4> p1;
    p1.ptIndex << 12,11,10,0,
                  15,14,13,0,
                  18,17,16,0,
                  21,20,19,0;
    faces.push_back(p1);
    //第三卦限面片
    Patch2<4,4> p2;
    p2.ptIndex << 21,20,19,0,
                  24,23,22,0,
                  27,26,25,0,
                  30,29,28,0;
    faces.push_back(p2);
    //第四卦限面片
    Patch2<4,4> p3;
    p3.ptIndex << 30,29,28,0,
                  33,32,31,0,
                  36,35,34,0,
                  3,2,1,0;
    faces.push_back(p3);
    //第五卦限面片
    Patch2<4,4> p4;
    p4.ptIndex << 61,38,37,3,
                  61,40,39,6,
                  61,42,41,9,
                  61,44,43,12;
    faces.push_back(p4);
    //第六卦限面片
    Patch2<4,4> p5;
    p5.ptIndex << 61,44,43,12,
                  61,46,45,15,
                  61,48,47,18,
                  61,50,49,21;
    faces.push_back(p5);
    //第七卦限面片
    Patch2<4,4> p6;
    p6.ptIndex << 61,50,49,21,
                  61,52,51,24,
                  61,54,53,27,
                  61,56,55,30;
    faces.push_back(p6);
    //第八卦限面片
    Patch2<4,4> p7;
    p7.ptIndex << 61,56,55,30,
                  61,58,57,33,
                  61,60,59,36,
                  61,38,37,3;
    faces.push_back(p7);
}

void Sphere2::Draw(AppContext* context)
{
    Eigen::Matrix<Vector3f, 4, 4> pts;
    int n = 3;
    mesh.ReadCoffients();
    for(int f = 0;f < 8;f++)
    {   
        
        for(int i = 0;i < 4;i++)
        {
            for(int j = 0;j < 4;j++)
            {
                pts(i,j) = verts[faces[f].ptIndex(i,j)];
            }
        }
        mesh.ReadCtrlPtsMat(pts);
        mesh.DrawCurvedPatch(context,n);
    }
}