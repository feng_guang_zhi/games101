#include "BezierSphere.hpp"

void BezierSphere::readVertex()
{
    const double m = 0.5523;
    //第一卦限
    ctrlPts.push_back({0.0,1.0,0.0});
    ctrlPts.push_back({0.0,1.0,m});
    ctrlPts.push_back({0.0,m,1.0});
    ctrlPts.push_back({0.0,0.0,1.0});
    ctrlPts.push_back({m * m,1.0,m});
    ctrlPts.push_back({m,m,1.0});
    ctrlPts.push_back({m,0.0,1.0});
    ctrlPts.push_back({m,1.0,m * m});
    ctrlPts.push_back({1.0,m,m});
    ctrlPts.push_back({1.0,0.0,m});
    ctrlPts.push_back({m,1.0,0.0});
    ctrlPts.push_back({1.0,m,0.0});
    ctrlPts.push_back({1.0,0.0,0.0});
    //第二卦限
    ctrlPts.push_back({m,1.0,-m * m});
    ctrlPts.push_back({1.0,m,-m});
    ctrlPts.push_back({1.0,0.0,-m});
    ctrlPts.push_back({m * m,1.0,-m});
    ctrlPts.push_back({m,m,-1.0});
    ctrlPts.push_back({m,0.0,-1.0});
    ctrlPts.push_back({0.0,1.0,-m});
    ctrlPts.push_back({0.0,m,-1.0});
    ctrlPts.push_back({0.0,0.0,-1.0});
    //第三卦限
    ctrlPts.push_back({-m * m,1.0,-m});
    ctrlPts.push_back({-m,m,-1.0});
    ctrlPts.push_back({-m,0.0,-1.0});
    ctrlPts.push_back({-m,1.0,-m * m});
    ctrlPts.push_back({-1.0,m,-m});
    ctrlPts.push_back({-1.0,0.0,-m});
    ctrlPts.push_back({-1.0,0.0,-m});
    ctrlPts.push_back({-m,1.0,0.0});
    ctrlPts.push_back({-1.0,m,0.0});
    ctrlPts.push_back({-1.0,0.0,0.0});
    //第四卦限
    ctrlPts.push_back({-m,1.0,m * m});
    ctrlPts.push_back({-1.0,m,m});
    ctrlPts.push_back({-1.0,0.0,m});
    ctrlPts.push_back({-m * m,1.0,m});
    ctrlPts.push_back({-m,m,1.0});
    ctrlPts.push_back({-m,0.0,1.0});
    //第五卦限
    ctrlPts.push_back({0.0,-m,1.0});
    ctrlPts.push_back({0.0,-1.0,m});
    ctrlPts.push_back({m,-m,1.0});
    ctrlPts.push_back({m * m,-1.0,m});
    ctrlPts.push_back({1.0,-m,m});
    ctrlPts.push_back({m,-1.0,m * m});
    ctrlPts.push_back({1.0,-m,0.0});
    ctrlPts.push_back({m,-1.0,0.0});
    //第六卦限
    ctrlPts.push_back({1.0,-m,-m});
    ctrlPts.push_back({m,-1.0,-m * m});
    ctrlPts.push_back({m,-m,-1.0});
    ctrlPts.push_back({m * m,-1.0,-m});
    ctrlPts.push_back({0.0,-m,-1.0});
    ctrlPts.push_back({0.0,-1.0,-m});
    //第七卦限
    ctrlPts.push_back({-m,-m,-1.0});
    ctrlPts.push_back({-m * m,-1.0,-m});
    ctrlPts.push_back({-1.0,-m,-m});
    ctrlPts.push_back({-m,-1.0,-m * m});
    ctrlPts.push_back({-1.0,-m,0.0});
    ctrlPts.push_back({-m,-1.0,0.0});
    //第八卦限
    ctrlPts.push_back({-1.0,-m,m});
    ctrlPts.push_back({-m,-1.0,m * m});
    ctrlPts.push_back({m,-m,1.0});
    ctrlPts.push_back({-m * m,-1.0,m});
    ctrlPts.push_back({0.0,-1.0,0.0});

    transform.SetVerts(&ctrlPts);
}

void BezierSphere::readFaces()
{
    faces.push_back({3,2,1,0, 6,5,4,0, 9,8,7,0, 12,11,10,0});
    faces.push_back({12,11,10,0, 15,14,13,0, 18,17,16,0, 21,20,19,0});
    faces.push_back({21,20,19,0, 24,23,22,0, 27,26,25,0, 30,29,28,0});
    faces.push_back({30,29,28,0, 33,32,31,0, 36,35,34,0, 3,2,1,0});

    faces.push_back({61,38,37,3, 61,40,39,6, 61,42,41,9, 61,44,43,12});
    faces.push_back({61,44,43,12, 61,46,45,15, 61,48,47,18, 61,50,49,21});
    faces.push_back({61,50,49,21, 61,52,51,24, 61,54,53,27, 61,56,55,30});
    faces.push_back({61,56,55,30, 61,58,57,33, 61,60,59,36, 61,38,37,3});
}

void BezierSphere::draw(AppContext* context)
{
    Eigen::Matrix<Vector3f, 4, 4> cPts;
    CubicBezierPatch bezier;
    for(int nPatch = 0;nPatch < 8;nPatch++)
    {
        for(int i = 0;i < 4;i++)
        {
            for(int j = 0;j < 4;j++)
            {
                cPts(i,j) = ctrlPts[faces[nPatch].ptIndex[i * 4 + j]];
            }
        }
        bezier.ReadCtrlPtsMat(cPts);
        bezier.ReadCoffients();
        bezier.DrawCurvePatch(context);
    }
}