#include "CubicBezierPatch.hpp"



void CubicBezierPatch::ReadCtrlPts()
{
    matrix(0,0) = Vector3f(0,20,200);
    matrix(0,1) = Vector3f(100,0,150);
    matrix(0,2) = Vector3f(100,-130,50);
    matrix(0,3) = Vector3f(50,-250,0);

    matrix(1,0) = Vector3f(100,100,150);
    matrix(1,1) = Vector3f(100,30,100);
    matrix(1,2) = Vector3f(100,-40,50);
    matrix(1,3) = Vector3f(100,-110,0);

    matrix(2,0) = Vector3f(90,280,140);
    matrix(2,1) = Vector3f(120,110,80);
    matrix(2,2) = Vector3f(130,0,30);
    matrix(2,3) = Vector3f(150,-100,-50);

    matrix(3,0) = Vector3f(30,350,150);
    matrix(3,1) = Vector3f(150,200,50);
    matrix(3,2) = Vector3f(200,50,0);
    matrix(3,3) = Vector3f(100,0,-70);

    for(int i = 0;i < matrix.rows();i++)
    {
        for(int j = 0;j < matrix.cols();j++)
        {
            matrix(i,j) += Vector3f(200,200,0);
        }
    }
}

void CubicBezierPatch::DrawCurvePatch(AppContext* context)
{
    auto rasterizer = context->getRasterizer();
    auto lineRenderer = context->getLineRenderer();
    auto color = Vector3f(0,0,1);
    auto tempMat1 = matrix;
    auto tempMat2 = rightMultiplyMat(tempMat1,coffients);
    auto tempMat = leftMultiplyMat(coffients.transpose(),tempMat2);

    double u0,u1,u2,u3,v0,v1,v2,v3;
    double tStep = 0.1;
    Vector3f last;
    bool startFlag = false;
    for(double u = 0;u <= 1;u += tStep)
    {
        for(double v = 0;v <= 1;v += tStep)
		{
			u3 = u * u * u;
            u2 = u * u;
            u1 = u;
            u0 = 1;
            v3 = v * v * v;
            v2 = v * v;
            v1 = v;
            v0 = 1;
			Vector3f pt = (u3 * tempMat(0,0) + u2 * tempMat(1,0) + u1 * tempMat(2,0) + u0 * tempMat(3,0) * v3)
                         +(u3 * tempMat(0,1) + u2 * tempMat(1,1) + u1 * tempMat(2,1) + u0 * tempMat(3,1) * v2)
                         +(u3 * tempMat(0,2) + u2 * tempMat(1,2) + u1 * tempMat(2,2) + u0 * tempMat(3,2) * v1)
                         +(u3 * tempMat(0,3) + u2 * tempMat(1,3) + u1 * tempMat(2,3) + u0 * tempMat(3,3) * v0);
            if(!startFlag)
            {
                last = Vector3f(std::round(pt.x()),std::round(pt.y()),0);
                startFlag = true;
            }
            else
            {
                auto end = Vector3f(pt.x(),pt.y(),0);
                lineRenderer->MidPoint2(last,end,color);
                last = end;
            }
		}	
    }
    // startFlag = false;
    // for(double v = 0;v <= 1;v += tStep)
    // {
    //     for(double u = 0;u <= 1;u += tStep)
	// 	{
	// 		u3 = u * u * u;
    //         u2 = u * u;
    //         u1 = u;
    //         u0 = 1;
    //         v3 = v * v * v;
    //         u2 = v * v;
    //         u1 = v;
    //         u0 = 1;
	// 		Vector3f pt = (u3 * tempMat(0,0) + u2 * tempMat(1,0) + u1 * tempMat(2,0) + u0 * tempMat(3,0) * v3)
    //                      +(u3 * tempMat(0,1) + u2 * tempMat(1,1) + u1 * tempMat(2,1) + u0 * tempMat(3,1) * v2)
    //                      +(u3 * tempMat(0,2) + u2 * tempMat(1,2) + u1 * tempMat(2,2) + u0 * tempMat(3,2) * v1)
    //                      +(u3 * tempMat(0,3) + u2 * tempMat(1,3) + u1 * tempMat(2,3) + u0 * tempMat(3,3) * v0);
    //         if(!startFlag)
    //         {
    //             last = Vector3f(std::round(pt.x()),std::round(pt.y()),0);
    //             startFlag = true;
    //         }
    //         else
    //         {
    //             auto end = Vector3f(pt.x(),pt.y(),0);
    //             lineRenderer->MidPoint2(last,end,color);
    //             last = end;
    //         }
	// 	}	
    // }
		
}