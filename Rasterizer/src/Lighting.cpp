#include "Lighting.hpp"

Lighting::Lighting()
{
    lightNum = 1;
    lightSource = new LightSource[lightNum];
    material = new Material();
    ambient = Vector3f(0.3,0.3,0.3);
}

Lighting::Lighting(int lightNum)
{
    this->lightNum = lightNum;
    lightSource = new LightSource[lightNum];
    material = new Material();
    ambient = Vector3f(0.3,0.3,0.3);
}

Lighting::~Lighting()
{
    if(lightSource)
    {
        delete[] lightSource;
    }
    if(material)
    {
        delete material;
    }
}

void Lighting::SetLightNum(int lightNum)
{
    this->lightNum = lightNum;
}
//计算光照
Vector3f Lighting::Illuminate(Vector3f viewPt,Vector3f pt,Vector3f ptCol,Vector3f normal,Material* material)
{
    //材质自身发光的初始值
    Vector3f resI = material->GetEmission();
    for(int i = 0;i < lightNum;i++)
    {
        if(lightSource[i].GetOnOff())
        {
            //反射光强
            Vector3f outI = {0,0,0};
            //光矢量
            Vector3f lDir = lightSource[i].GetLightPos() - pt;
            //光传播的距离
            auto lDis = lDir.norm();
            //std::cout << "lDis is :  " << lDis << std::endl;
            lDir = lDir.normalized();
            auto nDir = normal.normalized();
            //1 漫反射光
            double ndotl = std::max(nDir.dot(lDir),0.0f);
            //std::cout << "ndotl is :  " << ndotl << std::endl;
            auto ldiffuse = lightSource[i].GetDiffuse();
            auto mdiffuse = material->GetDiffuse();
            Vector3f cc = Vector3f(ldiffuse.x() * mdiffuse.x(),ldiffuse.y() * mdiffuse.y(),ldiffuse.z() * mdiffuse.z());
            Vector3f ccc = cc * ndotl;
            outI = outI + ccc;
            //2 镜面反射光
            Vector3f sDir = viewPt - pt;
            sDir = sDir.normalized();
            Vector3f hDir = (lDir + sDir) / (lDir + sDir).squaredNorm();
            double ndotH = std::max(nDir.dot(hDir),0.0f);
            double rs = std::pow(ndotH,material->GetExponent());
            auto lspecular = lightSource[i].GetSpecular();
            auto mspecular = material->GetSpecular();
            cc = Vector3f(lspecular.x() * mspecular.x(),lspecular.y() * mspecular.y(),lspecular.z() * mspecular.z());
            ccc = cc * rs;
            outI = outI + ccc;
            double c0 = lightSource[i].GetC0();
            double c1 = lightSource[i].GetC1();
            double c2 = lightSource[i].GetC2();
            double f = 1.0 / (c0 + c1 * lDis  + c2 * lDis * lDis);
            f = std::min(1.0,f);
            std::cout << "f is :  " << f << " " << outI << std::endl;
            resI += outI * f;
        }
        else
        {
            resI += ptCol;
        }
    }
    //std::cout << "resI is :  " << resI << std::endl;
    //4 加入环境光
    auto mAmbient = material->GetAmbient();
    resI += Vector3f(ambient.x() * mAmbient.x(),ambient.y() * mAmbient.y(),ambient.z() * mAmbient.z());
    //5 光强规范化到 [0,1]
    resI = resI.normalized();
    return resI;
}