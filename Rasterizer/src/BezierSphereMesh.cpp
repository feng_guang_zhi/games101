#include "BezierSphereMesh.hpp"

BezierSphereMesh::BezierSphereMesh()
{

}

BezierSphereMesh::~BezierSphereMesh()
{
    
}

void BezierSphereMesh::SetLight(Lighting* light)
{
    pLight = light;
}

void BezierSphereMesh::ReadCtrlPts()
{
    ReadCoffients();
}

void BezierSphereMesh::DrawCurvedPatch(AppContext* context,int recursion)
{
    Vector2f uvArray[4] = {{0,0},{1,0},{1,1},{0,1}};
    Tessellate(context,recursion,uvArray);
}

//细分曲面
void BezierSphereMesh::Tessellate(AppContext* context,int recursion,Vector2f* uvs)
{
    if(0 == recursion)
    {
        MeshGrid(context,uvs);
        Draw(context);
        return;
    }
    Vector2f midP = (uvs[0] + uvs[1] + uvs[2] + uvs[3]) / 4.0;
    Vector2f subT[4][4];
    //左下子曲面
    subT[0][0] = uvs[0];
    subT[0][1] = {midP.x(),uvs[0].y()};
    subT[0][2] = midP;
    subT[0][3] = {uvs[0].x(),midP.y()};
    Tessellate(context,recursion - 1,subT[0]);
    //右下子曲面
    subT[1][0] = {midP.x(),uvs[1].y()};
    subT[1][1] = uvs[1];
    subT[1][2] = {uvs[1].x(),midP.y()};
    subT[1][3] = midP;
    Tessellate(context,recursion - 1,subT[1]);
    //右上子曲面
    subT[2][0] = midP;
    subT[2][1] = {uvs[2].x(),midP.y()};
    subT[2][2] = uvs[2];
    subT[2][3] = {midP.x(),uvs[2].y()};
    Tessellate(context,recursion - 1,subT[2]);
    //右下子曲面
    subT[3][0] = {uvs[3].x(),midP.y()};
    subT[3][1] = midP;
    subT[3][2] = {midP.x(),uvs[3].y()};
    subT[3][3] = uvs[3];
    Tessellate(context,recursion - 1,subT[3]);
}
//四边形网格
void BezierSphereMesh::MeshGrid(AppContext* context,Vector2f* uvs)
{
    auto tempMat1 = matrix;
    auto tempMat2 = leftMultiplyMat(coffients,tempMat1);
    auto tempMat = rightMultiplyMat(tempMat2,coffients.transpose());

    double u0,u1,u2,u3,v0,v1,v2,v3;
    for(int i = 0;i < 4;i++)
    {
        u3 = std::pow(uvs[i].x(),3);
        u2 = std::pow(uvs[i].x(),2);
        u1 = uvs[i].x();
        u0 = 1;
        v3 = std::pow(uvs[i].y(),3);
        v2 = std::pow(uvs[i].y(),2);
        v1 = uvs[i].y();
        v0 = 1;
        Vector3f pt = (u3 * tempMat(0,0) + u2 * tempMat(1,0) + u1 * tempMat(2,0) + u0 * tempMat(3,0) * v3)
                     +(u3 * tempMat(0,1) + u2 * tempMat(1,1) + u1 * tempMat(2,1) + u0 * tempMat(3,1) * v2)
                     +(u3 * tempMat(0,2) + u2 * tempMat(1,2) + u1 * tempMat(2,2) + u0 * tempMat(3,2) * v1)
                     +(u3 * tempMat(0,3) + u2 * tempMat(1,3) + u1 * tempMat(2,3) + u0 * tempMat(3,3) * v0);
        gridPts[i].pt = pt;
        // 计算网格点法向量
        Vector3f uTangent,vTangent;
        // u方向偏导
        double pdu0,pdu1,pdu2,pdu3;
        pdu0 = 0;
        pdu1 = 1;
        pdu2 = 2 * std::pow(uvs[i].x(),1);
        pdu3 = 3 * std::pow(uvs[i].x(),2);
        uTangent =    (pdu3 * tempMat(0,0) + pdu2 * tempMat(1,0) + pdu1 * tempMat(2,0) + pdu0 * tempMat(3,0) * v3)
                     +(pdu3 * tempMat(0,1) + pdu2 * tempMat(1,1) + pdu1 * tempMat(2,1) + pdu0 * tempMat(3,1) * v2)
                     +(pdu3 * tempMat(0,2) + pdu2 * tempMat(1,2) + pdu1 * tempMat(2,2) + pdu0 * tempMat(3,2) * v1)
                     +(pdu3 * tempMat(0,3) + pdu2 * tempMat(1,3) + pdu1 * tempMat(2,3) + pdu0 * tempMat(3,3) * v0);
        // v方向偏导
        double pdv0,pdv1,pdv2,pdv3;
        pdv0 = 0;
        pdv1 = 1;
        pdv2 = 2 * std::pow(uvs[i].y(),1);
        pdv3 = 3 * std::pow(uvs[i].y(),2);
        uTangent =    (u3 * tempMat(0,0) + u2 * tempMat(1,0) + u1 * tempMat(2,0) + u0 * tempMat(3,0) * pdv3)
                     +(u3 * tempMat(0,1) + u2 * tempMat(1,1) + u1 * tempMat(2,1) + u0 * tempMat(3,1) * pdv2)
                     +(u3 * tempMat(0,2) + u2 * tempMat(1,2) + u1 * tempMat(2,2) + u0 * tempMat(3,2) * pdv1)
                     +(u3 * tempMat(0,3) + u2 * tempMat(1,3) + u1 * tempMat(2,3) + u0 * tempMat(3,3) * pdv0);
        gridNormals[i] = uTangent.cross(vTangent).normalized();
    }
    if(std::fabs(gridPts[0].pt.x() - gridPts[1].pt.x()) < 1e-4)
    {
        gridNormals[0] = gridNormals[1] = Vector3f(gridNormals[0]).normalized();
    }
    if(std::fabs(gridPts[2].pt.x() - gridPts[3].pt.x()) < 1e-4)
    {
        gridNormals[2] = gridNormals[3] = Vector3f(gridNormals[2]).normalized();
    }
}

void BezierSphereMesh::Draw(AppContext* context)
{
    auto lineRenderer = context->getLineRenderer();
    Vector3f eyePt = projection.getEye();
    Vector2f ScreenPts[4];
    Vector3f ScreenPtCols[4];
    
    for(int i = 0;i < 4;i++)
    {
        ScreenPts[i] = projection.PerspectiveProjection(gridPts[i].pt);
        //ScreenPtCols[i] = pLight->Illuminate(eyePt,gridPts[i].pt,gridPts[i].color,gridNormals[i],pLight->getMaterial());
        ScreenPtCols[i] = Vector3f(1,0,0);
    }
    
    Vector3f viewDir = eyePt - gridPts[0].pt;
    viewDir = viewDir.normalized();
    Vector3f v1 = gridPts[1].pt - gridPts[0].pt;
    Vector3f v2 = gridPts[2].pt - gridPts[0].pt;
    Vector3f v3 = gridPts[3].pt - gridPts[0].pt;
    Vector3f fNormalA = v1.cross(v2);
    Vector3f fNormalB = v2.cross(v3);
    Vector3f fNormal = (fNormalA + fNormalB).normalized();
    if(viewDir.dot(fNormal) >= 0)
    {
        // lineRenderer->MidPointLerp(Vector3f(ScreenPts[0].x(),ScreenPts[0].y(),0),Vector3f(ScreenPts[1].x(),ScreenPts[1].y(),0),ScreenPtCols[0],ScreenPtCols[1]);
        // lineRenderer->MidPointLerp(Vector3f(ScreenPts[1].x(),ScreenPts[1].y(),0),Vector3f(ScreenPts[2].x(),ScreenPts[2].y(),0),ScreenPtCols[1],ScreenPtCols[2]);
        // lineRenderer->MidPointLerp(Vector3f(ScreenPts[2].x(),ScreenPts[2].y(),0),Vector3f(ScreenPts[3].x(),ScreenPts[3].y(),0),ScreenPtCols[2],ScreenPtCols[3]);
        // lineRenderer->MidPointLerp(Vector3f(ScreenPts[3].x(),ScreenPts[3].y(),0),Vector3f(ScreenPts[0].x(),ScreenPts[0].y(),0),ScreenPtCols[3],ScreenPtCols[0]);
         lineRenderer->MidPoint2(Vector3f(ScreenPts[0].x(),ScreenPts[0].y(),0),Vector3f(ScreenPts[1].x(),ScreenPts[1].y(),0),ScreenPtCols[0]);
        lineRenderer->MidPoint2(Vector3f(ScreenPts[1].x(),ScreenPts[1].y(),0),Vector3f(ScreenPts[2].x(),ScreenPts[2].y(),0),ScreenPtCols[1]);
        lineRenderer->MidPoint2(Vector3f(ScreenPts[2].x(),ScreenPts[2].y(),0),Vector3f(ScreenPts[3].x(),ScreenPts[3].y(),0),ScreenPtCols[2]);
        lineRenderer->MidPoint2(Vector3f(ScreenPts[3].x(),ScreenPts[3].y(),0),Vector3f(ScreenPts[0].x(),ScreenPts[0].y(),0),ScreenPtCols[3]);
    }

}