#include "AppContext.hpp"

AppContext::AppContext()
{
    rasterizer = new Rasterizer(SCREEN_WIDTH,SCREEN_HEIGHT);
    rasterizer->clear(BufferType::Color | BufferType::Depth);
    lineRenderer = new LineRenderer(rasterizer);
    triangleRender = new TriangleRenderer(rasterizer,lineRenderer);
    polygonRenderer = new PolygonRenderer(rasterizer,lineRenderer);
    projection = new Projection();
}

AppContext::~AppContext()
{
    delete rasterizer;
    delete lineRenderer;
    delete triangleRender;
    delete polygonRenderer;
    delete projection;
}

Rasterizer* AppContext::getRasterizer()
{
    return rasterizer;
}

LineRenderer* AppContext::getLineRenderer()
{
    return lineRenderer;
}

TriangleRenderer* AppContext::getTriangleRender()
{
    return triangleRender;
}

PolygonRenderer* AppContext::getPolygonRenderer()
{
    return polygonRenderer;
}

Projection* AppContext::getProjection()
{
    return projection;
}