#include"Triangle.hpp"
#include<algorithm>
#include<array>
#include<stdexcept>


Triangle::Triangle(){
    for(int i = 0;i < 3;i++){
        vertexs[i].cvertexs.pt << 0,0,0;
        vertexs[i].cvertexs.color << 0,0,0;
        vertexs[i].texCoord << 0,0,0;
        vertexs[i].normal << 0,0,0;
    }
}

Triangle::Triangle(Vector3f a,Vector3f b,Vector3f c){
    vertexs[0].cvertexs.pt = a;
    vertexs[1].cvertexs.pt = b;
    vertexs[2].cvertexs.pt = c;
}

Triangle::Triangle(Vector2f a,Vector2f b,Vector2f c):Triangle(Vector3f(a.x(),a.y(),0),Vector3f(b.x(),b.y(),0),Vector3f(c.x(),c.y(),0))
{
    
}

Triangle::Triangle(Vector3f a,Vector3f b,Vector3f c,Vector3f ca,Vector3f cb,Vector3f cc){
    vertexs[0].cvertexs.pt = a;
    vertexs[1].cvertexs.pt = b;
    vertexs[2].cvertexs.pt = c;

    vertexs[0].cvertexs.color = ca;
    vertexs[1].cvertexs.color = cb;
    vertexs[2].cvertexs.color = cc;
}

Triangle::Triangle(Vector2f a,Vector2f b,Vector2f c,Vector3f ca,Vector3f cb,Vector3f cc):Triangle(Vector3f(a.x(),a.y(),0),Vector3f(b.x(),b.y(),0),Vector3f(c.x(),c.y(),0),ca,cb,cc)
{
    
}

void Triangle::setVertex(int vindex,Vector3f ver){
    vertexs[vindex].cvertexs.pt = ver;
}

void Triangle::setNormal(int nindex,Vector3f normal){
    vertexs[nindex].normal = normal;
}

void Triangle::setColor(int cindex,Vector3f color){
    vertexs[cindex].cvertexs.color = color;
}

void Triangle::setColor(int cindex,float r,float g,float b){
    if(r < 0.0 || r > 255.0 ||
       g < 0.0 || g > 255.0 ||
       b < 0.0 || b > 255.0){
        throw std::runtime_error("Invalid Color");
    }
    vertexs[cindex].cvertexs.color = Vector3f(r / 255.0,g / 255.0,b / 255.0);
}

void Triangle::setTexCoord(int tindex,Vector2f uv){
    vertexs[tindex].texCoord = uv;
}

void Triangle::setTexCoord(int tindex,float u,float v){
    vertexs[tindex].texCoord = Vector2f(u,v);
}

std::array<Eigen::Vector4f,3> Triangle::toVec4() const{
    std::array<Eigen::Vector4f,3> res;
    std::transform(std::begin(res),std::end(res),res.begin(),
        [](auto & vec){
            return Eigen::Vector4f(vec.x(),vec.y(),vec.z(),1.0f);
    });
    return res;
}

void Triangle::sortY(){
    std::sort(vertexs,vertexs + 3,[](const VertexInfo& a,const VertexInfo& b){
        if(a.cvertexs.pt.y() == b.cvertexs.pt.y()){
            return a.cvertexs.pt.x() < b.cvertexs.pt.x();
        }
        return a.cvertexs.pt.y() < b.cvertexs.pt.y();
    });
}