#include "LightSource.hpp"

 LightSource::LightSource()
 {
    diffuse = Vector3f(0,0,0);
    specular = Vector3f(1,1,1);
    postion = Vector3f(0,0,1000);
    c0 = 1;
    c1 = 0;
    c2 = 0;
    onOff = true;
 }

LightSource::~LightSource()
{

}
//漫反射
void LightSource::SetDiffuse(Vector3f color)
{
    diffuse = color;
}
//镜面反射
void LightSource::SetSpecular(Vector3f color)
{
    specular = color;
}
//光源位置
void LightSource::SetLightPos(Vector3f pos)
{
    postion = pos;
}
//光强衰减因子
void LightSource::SetAttenuationFactor(double c0,double c1,double c2)
{
    this->c0 = c0;
    this->c1 = c1;
    this->c2 = c2;
}
//设置光源开关
void LightSource::SetOnOff(bool on)
{
    onOff = on;
}

bool LightSource::GetOnOff()
{
    return onOff;
}

Vector3f LightSource::GetLightPos()
{
    return postion;
}

Vector3f LightSource::GetDiffuse()
{
    return diffuse;
}
Vector3f LightSource::GetSpecular()
{
    return specular;
}

double LightSource::GetC0()
{
    return c0;
}
double LightSource::GetC1()
{
    return c1;
}
double LightSource::GetC2()
{
    return c2;
}