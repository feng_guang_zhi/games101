#include "Projection.hpp"


Projection::Projection()
{
    viewDis = 1000;
    eyeDis = 800;
    setEye();
}
Vector2f Projection::OrthogonalProjection(const Vector3f& worldPt)
{
    Vector2f screenPt(worldPt.x(),worldPt.y());
    return screenPt;
}

Vector2f Projection::CavalierProjection(const Vector3f& worldPt)
{
    double m = 0.707;
    Vector2f screenPt(worldPt.x() - m * worldPt.z(),worldPt.y() - m * worldPt.z());
    return screenPt;
}

Vector2f Projection::CabinetProjection(const Vector3f& worldPt)
{
    double m = 0.3536;
    Vector2f screenPt(worldPt.x() - m * worldPt.z(),worldPt.y() - m * worldPt.z());
    return screenPt;
}

Vector2f Projection::PerspectiveProjection(const Vector3f& worldPt)
{
    Vector3f viewPt(worldPt.x(),worldPt.y(),viewDis - worldPt.z());
    double m = 0.3536;
    Vector2f screenPt(eyeDis * viewPt.x() / viewPt.z(),eyeDis * viewPt.y() / viewPt.z());
    return screenPt;
}

Vector3f Projection::getEye()
{
    return eyePt;
}

void Projection::setEye()
{
    eyePt = Vector3f(0,0,viewDis);
}