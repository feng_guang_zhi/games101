#include "Transform3.hpp"

void Transform3::SetVerts(std::vector<Vector3f>* pts)
{
    this->pts = pts;
}

void Transform3::SetVertDatas(std::vector<VertexData>* verts)
{
    this->vertDatas = verts;
}

void Transform3::MultiplyMatrix()
{
    MultiplyMatrix3();

    MultiplyMatrix2();
}

void Transform3::MultiplyMatrix2()
{
    if(vertDatas == nullptr)
    {
        std::cout << "vertDatas is not assigned" << std::endl;
        return;
    }
    auto pNumber = vertDatas->size();
    std::vector<Vector4f> temp;
    temp.resize(pNumber);
    
    for(int i = 0;i < pNumber;i++)
    {
        //这里注意 齐次坐标
        temp[i] = Vector4f((*vertDatas)[i].pt.x(),(*vertDatas)[i].pt.y(),(*vertDatas)[i].pt.z(),1);
    }
    for(int i = 0;i < pNumber;i++)
    {
        auto vec4 =  mat * temp[i];
        (*vertDatas)[i].pt = Vector3f(vec4.x(),vec4.y(),vec4.z());
    }
}

void Transform3::MultiplyMatrix3()
{
    if(pts == nullptr)
    {
        std::cout << "pts is not assigned" << std::endl;
        return;
    }
    auto pNumber = pts->size();
    std::vector<Vector4f> temp;
    temp.resize(pNumber);
    
    for(int i = 0;i < pNumber;i++)
    {
        //这里注意 齐次坐标
        temp[i] = Vector4f((*pts)[i].x(),(*pts)[i].y(),(*pts)[i].z(),1);
    }
    for(int i = 0;i < pNumber;i++)
    {
        auto vec4 =  mat * temp[i];
        (*pts)[i] = Vector3f(vec4.x(),vec4.y(),vec4.z());
    }
}

void Transform3::Translate(double tx,double ty,double tz)
{
    mat << 1,0,0,tx,
           0,1,0,ty,
           0,0,1,tz,
           0,0,0,1;
    MultiplyMatrix();
}

void Transform3::Scale(double sx,double sy,double sz)
{
    mat << sx,0,0,0,
           0,sy,0,0,
           0,0,sz,0,
           0,0,0,1;
    MultiplyMatrix();
}

void Transform3::Scale(double sx,double sy,double sz,Vector3f pt)
{
    
}

void Transform3::Rotate(double beta)
{
    mat << cos(beta * PI / 180),-sin(beta * PI / 180),0,
           sin(beta * PI / 180),cos(beta * PI / 180),0,
           0,0,1;
    MultiplyMatrix();
}

void Transform3::RotateX(double beta)
{
    auto angle = beta * PI / 180;
    mat << 1,0,0,0,
           0,cos(angle),sin(angle),0,
           0,-sin(angle),cos(angle),0,
           0,0,0,1;
    MultiplyMatrix();
}
void Transform3::RotateY(double beta)
{

}
void Transform3::RotateZ(double beta)
{
    
}

void Transform3::Rotate(double beta,Vector3f pt)
{
    Translate(-pt.x(),-pt.y(),-pt.z());
    Rotate(beta);
    Translate(pt.x(),pt.y(),-pt.z());
}

//轴在原点的
void Transform3::RotateArbitraryAxis(double theta, Vector3f axis)
{
    theta = theta * PI / 180;

    auto pNumber = pts->size();
    
    for(int i = 0;i < pNumber;i++)
    {
        rotateAroundAxis((*pts)[i] ,axis,theta);
        std::cout << "after : " << (*pts)[i] << std::endl;
    }
}

//轴不在原点的
void Transform3::rotatePointAroundArbitraryAxis(const Vector3f& axisPoint, const Vector3f& axisDirection, float theta)
{
    theta = theta * PI / 180;
    if(pts != nullptr)
    {
        auto pNumber = pts->size();
        for(int i = 0;i < pNumber;i++)
        {
            // 将轴和点移动至原点
            (*pts)[i] = (*pts)[i] - axisPoint;
            rotateAroundAxis((*pts)[i] ,axisDirection,theta);
            // 将点平移回原来的位置
            (*pts)[i] = (*pts)[i] + axisPoint;
        }
    }
    if(vertDatas != nullptr)
    {
        auto pNumber = vertDatas->size();
        for(int i = 0;i < pNumber;i++)
        {
            // 将轴和点移动至原点
            (*vertDatas)[i].pt = (*vertDatas)[i].pt - axisPoint;
            rotateAroundAxis((*vertDatas)[i].pt ,axisDirection,theta);
            // 将点平移回原来的位置
            (*vertDatas)[i].pt = (*vertDatas)[i].pt + axisPoint;
        }
    }
}

// 得到绕任意过原点的轴的旋转变换矩阵
// 使用罗德里格斯公式（lecture4-3）
// R(n,α)=cos(α)*I+(1-cos(α))*n*n(T)+sin(α)*N
// 矩阵N
// [0,-n(z),n(y)]
// [n(z),0,-n(x)]
// [-n(y),n(x),0]
Matrix4f Transform3::getRotMat(Vector3f axis, float angle)
{
    Eigen::Matrix4f rotation = Eigen::Matrix4f::Identity();
    float radians = (angle * PI) / 180;

    Eigen::Matrix3f N, R;
    N << 0, -axis[2], axis[1],
        axis[2], 0, -axis[0],
        -axis[1], axis[0], 0;

    // 所需的单位矩阵
    Eigen::Matrix3f I = Eigen::Matrix3f::Identity();

    R = cosf(radians) * I + (1 - cosf(radians)) * N * N.transpose() + sinf(radians) * N;

    rotation << R(0, 0), R(0, 1), R(0, 2), 0,
        R(1, 0), R(1, 1), R(1, 2), 0,
        R(2, 0), R(2, 1), R(2, 2), 0,
        0, 0, 0, 1;

    return rotation;
}

//罗德里格斯公式
void Transform3::rotateAroundAxis(Vector3f& v, const Vector3f& axis, double angle) {
    // 将旋转轴标准化
    Vector3f n = axis.normalized();
    
    // 计算旋转矩阵
    Matrix3f rotationMatrix;
    double cos_theta = cos(angle);
    double sin_theta = sin(angle);
    rotationMatrix << cos_theta + n.x() * n.x() * (1 - cos_theta), n.x() * n.y() * (1 - cos_theta) - n.z() * sin_theta, n.x() * n.z() * (1 - cos_theta) + n.y() * sin_theta,
                      n.y() * n.x() * (1 - cos_theta) + n.z() * sin_theta, cos_theta + n.y() * n.y() * (1 - cos_theta), n.y() * n.z() * (1 - cos_theta) - n.x() * sin_theta,
                      n.z() * n.x() * (1 - cos_theta) - n.y() * sin_theta, n.z() * n.y() * (1 - cos_theta) + n.x() * sin_theta, cos_theta + n.z() * n.z() * (1 - cos_theta);

    // 应用旋转矩阵
    v = rotationMatrix * v;
}

void Transform3::ReflectOrg()
{
    
}

void Transform3::ReflectX()
{

}

void Transform3::ReflectY()
{
    
}

void Transform3::Shear(double b,double c)
{

}