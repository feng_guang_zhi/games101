#include"Rasterizer.hpp"
#include<opencv2/opencv.hpp>
#include<math.h>
#include<stdexcept>

Rasterizer::Rasterizer(int w,int h){
    width = w;
    height = h;
    frameBuffer.resize(w * h);
    depthBuffer.resize(w * h);
}

int Rasterizer::getBufferIndex(int x,int y){
    return x * width + y;
}

void Rasterizer::setPixel(const Vector3f& pt,const Vector3f& color){
    if(pt.x() < 0 || pt.y() >= width ||
       pt.y() < 0 || pt.y() >= width){
        return;
    }
    int idx = getBufferIndex(pt.x(),pt.y());
    if((size_t)idx >= frameBuffer.size()){
        return;
    }
    frameBuffer[idx] = color;
}

void Rasterizer::setPixel(int x,int y,const Vector3f& color)
{
    setPixel(Vector3f(x,y,0),color);
}

Vector3f Rasterizer::getPixel(int x,int y)
{
    if(x < 0 || y >= width ||
       y < 0 || y >= width){
        std::cout << "over bound" << std::endl;
        return Vector3f(0,0,0);
    }
    int idx = getBufferIndex(x,y);
    if((size_t)idx >= frameBuffer.size()){
        std::cout << "over bound" << std::endl;
        return Vector3f(0,0,0);
    }
    return frameBuffer[idx];
}

Vector3f Rasterizer::getPixel(Vector2f xy)
{
    return getPixel(xy.x(),xy.y());
}

void Rasterizer::setZBuffer(Vector2f xy,float zvalue)
{
    if(xy.x() < 0 || xy.y() >= width ||
       xy.y() < 0 || xy.y() >= width){
        return;
    }
    int idx = getBufferIndex(xy.x(),xy.y());
    if((size_t)idx >= frameBuffer.size()){
        return;
    }
    depthBuffer[idx] = zvalue;
}

void Rasterizer::setZBuffer(int x,int y,float zvalue)
{
    setZBuffer(Eigen::Vector2f(x,y),zvalue);
}

double Rasterizer::getZBuffer(Vector2f xy)
{
    return getZBuffer(xy.x(),xy.y());
}

double Rasterizer::getZBuffer(int x,int y)
{
    if(x < 0 || y >= width ||
       y < 0 || y >= width){
        std::cout << "depth over bound" << std::endl;
        return 0;
    }
    int idx = getBufferIndex(x,y);
    if((size_t)idx >= depthBuffer.size()){
        std::cout << "depth over bound" << std::endl;
        return 0;
    }
    return depthBuffer[idx];
}

void Rasterizer::clear(BufferType buffType)
{
    if((buffType & BufferType::Color) == BufferType::Color){
        std::fill(frameBuffer.begin(),frameBuffer.end(),Eigen::Vector3f(0.0f,0.0f,0.0f));
    }

    if((buffType & BufferType::Depth) == BufferType::Depth){
        std::fill(depthBuffer.begin(),depthBuffer.end(),std::numeric_limits<float>::infinity());
    }
}

void Rasterizer::setModel(const Matrix4f& m)
{
    model = m;
}

void Rasterizer::setView(const Matrix4f& m)
{
    view = m;
}

void Rasterizer::setProjection(const Matrix4f& m)
{
    projection = m;
}

PosBufId Rasterizer::loadPositions(const std::vector<Vector3f>& positions)
{
    auto id = getNextId();
    posBuf.emplace(id,positions);
    return {id};
}

IndexBufId Rasterizer::loadIndices(const std::vector<Vector3i>& indices)
{
    auto id = getNextId();
    indicesBuf.emplace(id,indices);
    return {id};
}

void Rasterizer::rasterizeTriangle(const Triangle2 &triangle, const std::array<Vector3f, 3> &viewPos)
{
    auto v = triangle.toVector4();
    int min_x = INT_MAX;
    int max_x = INT_MIN;
    int min_y = INT_MAX;
    int max_y = INT_MIN;
    for (auto point: v) 
    {
        if (point[0] < min_x) min_x = point[0];
        if (point[0] > max_x) max_x = ceil(point[0]);
        if (point[1] < min_y) min_y = point[1];
        if (point[1] > max_y) max_y = ceil(point[1]);
    }
    for (int x = min_x; x <= max_x; x++) {
        for (int y = min_y; y <= max_y; y++) {
            // SSAA抗锯齿后没有明显效果，运行时间从原来的 8秒变为 26秒
            
            //以像素中心点作为采样点
            if (Util::insideTriangle((float) x + 0.5, (float) y + 0.5, triangle.v)) {

                //得到这个点的重心坐标
                auto abg = Util::computeBarycentric2D((float) x + 0.5, (float) y + 0.5, triangle.v);
                float alpha = std::get<0>(abg);
                float beta = std::get<1>(abg);
                float gamma = std::get<2>(abg);

                //z-buffer插值
                float w_reciprocal = 1.0 / (alpha / v[0].w() + beta / v[1].w() + gamma / v[2].w()); //归一化系数
                float z_interpolated =
                        alpha * v[0].z() / v[0].w() + beta * v[1].z() / v[1].w() + gamma * v[2].z() / v[2].w();
                z_interpolated *= w_reciprocal;

                if (abs(z_interpolated) < getZBuffer(x, y)) 
                {
                    Vector3f pixelXY = {x, y, 0};
                    // 颜色插值
                    Vector3f interpolated_color = Util::interpolate(alpha, beta, gamma, triangle.color[0], triangle.color[1],
                                                            triangle.color[2],1);
                    // 法向量插值
                    Vector3f interpolated_normal = Util::interpolate(alpha, beta, gamma, triangle.normal[0], triangle.normal[1],
                                                            triangle.normal[2],1);
                    // 纹理插值
                    auto interpolated_texcoords = Util::interpolate(alpha, beta, gamma, triangle.tex_coords[0], triangle.tex_coords[1],
                                                                triangle.tex_coords[2], 1);
                    // 内部点 3维空间插值
                    auto interpolated_shadingcoords = Util::interpolate(alpha, beta, gamma, viewPos[0], viewPos[1],
                                                                    viewPos[2], 1);

                    interpolated_normal = interpolated_normal.normalized();
                    FragmentShader frag(interpolated_color, interpolated_normal,
                                                    interpolated_texcoords, mtexture);

                    frag.viewPos = interpolated_shadingcoords;
                    auto pixel_color = fragmentShaderFunc(frag);
                    //auto pixel_color = Vector3f(1,0,0);
                    setPixel(pixelXY, pixel_color); //设置颜色
                    setZBuffer(x, y, abs(z_interpolated));//更新z值
                }
            }
        }
    }
}

void Rasterizer::drawTriangles(std::vector<Triangle2*> &triangleList)
{
    float f1 = (50 - 0.1) / 2.0;
    float f2 = (-50 + -0.1) / 2.0;

    Eigen::Matrix4f mvp = projection * view * model;
    for (const auto &t: triangleList) {
        Triangle2 newtri = *t;

        // 取得三维空间下的坐标 (还未投影到平面)
        std::array<Vector4f, 3> mm{
                (view * model * t->v[0]),
                (view * model * t->v[1]),
                (view * model * t->v[2])
        };

        std::array<Eigen::Vector3f, 3> viewspace_pos;

        std::transform(mm.begin(), mm.end(), viewspace_pos.begin(), [](auto &v) {
            return v.template head<3>();
        });

        Eigen::Vector4f v[] = {
                mvp * t->v[0],
                mvp * t->v[1],
                mvp * t->v[2]
        };

        //Homogeneous division
        for (auto &vec: v) {
            vec.x() /= vec.w();
            vec.y() /= vec.w();
            vec.z() /= vec.w();
        }

        Eigen::Matrix4f inv_trans = (view * model).inverse().transpose();
        Eigen::Vector4f n[] = {
                inv_trans * Util::toVec4(t->normal[0], 0.0f),
                inv_trans * Util::toVec4(t->normal[1], 0.0f),
                inv_trans * Util::toVec4(t->normal[2], 0.0f)
        };

        //Viewport transformation
        for (auto &vert: v) {
            vert.x() = 0.5 * width * (vert.x() + 1.0);
            vert.y() = 0.5 * height * (vert.y() + 1.0);
            vert.z() = vert.z() * f1 + f2;
        }

        for (int i = 0; i < 3; ++i) {
            //screen space coordinates
            newtri.setVertex(i, v[i]);
        }

        for (int i = 0; i < 3; ++i) {
            //view space normal
            newtri.setNormal(i, n[i].head<3>());
        }

        newtri.setColor(0, 148,121.0,92.0);
        newtri.setColor(1, 148,121.0,92.0);
        newtri.setColor(2, 148,121.0,92.0);

        // setColor会将颜色归一化
//        newtri.setColor(0, 255, 0.0, 0.0);
//        newtri.setColor(1, 255, 0.0, 0.0);
//        newtri.setColor(2, 255, 0.0, 0.0);

        // Also pass view space vertice position
        rasterizeTriangle(newtri, viewspace_pos);
    }
}

void Rasterizer::setVertexShader(std::function<Vector3f(VertexShader)> vertShader)
{
    vertexShaderFunc = vertShader;
}

void Rasterizer::setFragmentShader(std::function<Vector3f(FragmentShader)> fragShader)
{
    fragmentShaderFunc = fragShader;
}