#include "Diamond.hpp"

Diamond::Diamond()
{
    divideNum = 0;
    radius = 0.0;
}

Diamond::~Diamond()
{
    
}

void Diamond::OnDraw(AppContext* context)
{
    auto lineRenderer = context->getLineRenderer();
    for(int i = 0;i <= this->divideNum - 2;i++)
    {
        for(int j = i + 1;j <= this->divideNum;j++)
        {
            auto start = this->center + pts[i];
            auto end = this->center + pts[j];
            lineRenderer->MidPoint2(Vector3f(start.x(),start.y(),0),Vector3f(end.x(),end.y(),0),Vector3f(0,0,1));
        }
    }
}

void Diamond::ReadPoints()
{
    double theta = 2 * Util::PI / this->divideNum;
    for(int i = 0;i < this->divideNum;i++)
    {
        auto x = this->radius * std::cos(i * theta);
        auto y = this->radius * std::sin(i * theta);
        pts.push_back({x,y});
    }

}

void Diamond::SetParameter(int n,double r,Vector2f center)
{
    this->divideNum = n;
    this->radius = r;
    this->center = center;

}