#include "Transform2.hpp"

void Transform2::SetVerts(std::vector<Vector2f>* pts)
{
    this->pts = pts;
}

void Transform2::MultiplyMatrix()
{
    auto pNumber = pts->size();
    std::vector<Vector3f> temp;
    temp.resize(pNumber);
    
    for(int i = 0;i < pNumber;i++)
    {
        //这里注意 齐次坐标
        temp[i] = Vector3f((*pts)[i].x(),(*pts)[i].y(),1);
        std::cout << "before : " << temp[i] << std::endl;
    }
    for(int i = 0;i < pNumber;i++)
    {
        auto vec3 =  mat * temp[i];
        (*pts)[i] = Vector2f(vec3.x(),vec3.y());
        std::cout << "after : " << (*pts)[i] << std::endl;
    }
    
}

void Transform2::Translate(double tx,double ty)
{
    Identity();
    mat << 1,0,tx,
           0,1,ty,
           0,0,1;
    MultiplyMatrix();
}

void Transform2::Scale(double sx,double sy)
{

}

void Transform2::Scale(double sx,double sy,Vector2f pt)
{
    
}

void Transform2::Rotate(double beta)
{
    const float PI = 3.1415926;
    Identity();
    mat << cos(beta * PI / 180),-sin(beta * PI / 180),0,
           sin(beta * PI / 180),cos(beta * PI / 180),0,
           0,0,1;
    MultiplyMatrix();
}

void Transform2::Rotate(double beta,Vector2f pt)
{
    Translate(-pt.x(),-pt.y());
    Rotate(beta);
    Translate(pt.x(),pt.y());
}

void Transform2::ReflectOrg()
{
    
}

void Transform2::ReflectX()
{

}

void Transform2::ReflectY()
{
    
}

void Transform2::Shear(double b,double c)
{

}