#include "Material.hpp"

Material::Material()
{
    ambient = Vector3f(0.2,0.2,0.2);
    diffuse = Vector3f(0.8,0.8,0.8);
    specular = Vector3f(0.3,0.3,0.3);
    exp = 1.0;
}
Material::~Material()
{
    
}
//设置环境光的反射率
void Material::SetAmbient(Vector3f color)
{
    ambient = color;
}
//设置漫反射光的反射率
void Material::SetDiffuse(Vector3f color)
{
    diffuse = color;
}
//设置镜面反射光的反射率
void Material::SetSpecular(Vector3f color)
{
    specular = color;   
}
//设置自身辐射的颜色
void Material::SetEmission(Vector3f color)
{
    emission = color;
}
//设置高光指数
void Material::SetExponent(double e)
{
    exp = e;
}

Vector3f Material::GetEmission()
{
    return emission;
}

Vector3f Material::GetAmbient()
{
    return ambient;
}
Vector3f Material::GetDiffuse()
{
    return diffuse;
}
Vector3f Material::GetSpecular()
{
    return specular;
}

double Material::GetExponent()
{
    return exp;
}