#include "ObjLoader.hpp"
#include "Util.hpp"

ObjLoader::ObjLoader()
{

}

ObjLoader::~ObjLoader()
{

}

bool ObjLoader::LoadFile(std::string path)
{
    // If the file is not an .obj file return false
    if (path.substr(path.size() - 4, 4) != ".obj")
    {
        return false;
    }
       
    std::ifstream file(path);

    if (!file.is_open())
    {
        return false;
    }
        

    loadedMeshes.clear();
    loadedVertices.clear();
    loadedIndices.clear();

    std::vector<Vector3f> positions;
    std::vector<Vector2f> texCoords;
    std::vector<Vector3f> normals;

    std::vector<VertexData> vertices;
    std::vector<int> indices;

    std::vector<std::string> meshMatNames;

    bool listening = false;
    std::string meshname;

    MeshData tempMesh;

#ifdef OBJL_CONSOLE_OUTPUT
    const unsigned int outputEveryNth = 1000;
    unsigned int outputIndicator = outputEveryNth;
#endif

    std::string curline;
    while (std::getline(file, curline))
    {
#ifdef OBJL_CONSOLE_OUTPUT
        if ((outputIndicator = ((outputIndicator + 1) % outputEveryNth)) == 1)
        {
            if (!meshname.empty())
            {
                std::cout
                        << "\r- " << meshname
                        << "\t| vertices > " << Positions.size()
                        << "\t| texcoords > " << TCoords.size()
                        << "\t| normals > " << Normals.size()
                        << "\t| triangles > " << (Vertices.size() / 3)
                        << (!MeshMatNames.empty() ? "\t| material: " + MeshMatNames.back() : "");
            }
        }
#endif

        // Generate a Mesh Object or Prepare for an object to be created
        if (Util::firstToken(curline) == "o" || Util::firstToken(curline) == "g" || curline[0] == 'g')
        {
            if (!listening)
            {
                listening = true;

                if (Util::firstToken(curline) == "o" || Util::firstToken(curline) == "g")
                {
                    meshname = Util::tail(curline);
                }
                else
                {
                    meshname = "unnamed";
                }
            }
            else
            {
                // Generate the mesh to put into the array

                if (!indices.empty() && !vertices.empty())
                {
                    // Create Mesh
                    tempMesh = MeshData(vertices, indices);
                    tempMesh.MeshName = meshname;

                    // Insert Mesh
                    loadedMeshes.push_back(tempMesh);

                    // Cleanup
                    vertices.clear();
                    indices.clear();
                    meshname.clear();

                    meshname = Util::tail(curline);
                }
                else
                {
                    if (Util::firstToken(curline) == "o" || Util::firstToken(curline) == "g")
                    {
                        meshname = Util::tail(curline);
                    }
                    else
                    {
                        meshname = "unnamed";
                    }
                }
            }
#ifdef OBJL_CONSOLE_OUTPUT
            std::cout << std::endl;
            outputIndicator = 0;
#endif
        }
        // Generate a Vertex Position
        if (Util::firstToken(curline) == "v")
        {
            std::vector<std::string> spos;
            
            Util::split(Util::tail(curline), spos, " ");

            Vector3f vpos{std::stof(spos[0]),std::stof(spos[1]),std::stof(spos[2])};

            positions.push_back(vpos);
        }
        // Generate a Vertex Texture Coordinate
        if (Util::firstToken(curline) == "vt")
        {
            std::vector<std::string> stex;
            
            Util::split(Util::tail(curline), stex, " ");

            Vector2f vtex{std::stof(stex[0]),std::stof(stex[1])};

            texCoords.push_back(vtex);
        }
        // Generate a Vertex Normal;
        if (Util::firstToken(curline) == "vn")
        {
            std::vector<std::string> snor;

            Util::split(Util::tail(curline), snor, " ");

            Vector3f vnor{std::stof(snor[0]),std::stof(snor[1]),std::stof(snor[2])};

            normals.push_back(vnor);
        }
        // Generate a Face (vertices & indices)
        if (Util::firstToken(curline) == "f")
        {
            // Generate the vertices
            std::vector<VertexData> vVerts;
            GenVerticesFromRawOBJ(vVerts, positions, texCoords, normals, curline);

            // Add Vertices
            for (int i = 0; i < int(vVerts.size()); i++)
            {
                vertices.push_back(vVerts[i]);

                loadedVertices.push_back(vVerts[i]);
            }

            std::vector<unsigned int> iIndices;

            VertexTriangluation(iIndices, vVerts);

            // Add Indices
            for (int i = 0; i < int(iIndices.size()); i++)
            {
                unsigned int indnum = (unsigned int)((vertices.size()) - vVerts.size()) + iIndices[i];
                indices.push_back(indnum);

                indnum = (unsigned int)((loadedVertices.size()) - vVerts.size()) + iIndices[i];
                loadedIndices.push_back(indnum);

            }
        }
        // Get Mesh Material Name
        if (Util::firstToken(curline) == "usemtl")
        {
            meshMatNames.push_back(Util::tail(curline));

            // Create new Mesh, if Material changes within a group
            if (!indices.empty() && !vertices.empty())
            {
                // Create Mesh
                tempMesh = MeshData(vertices, indices);
                tempMesh.MeshName = meshname;
                int i = 2;
                while(1) {
                    tempMesh.MeshName = meshname + "_" + std::to_string(i);

                    for (auto &m : loadedMeshes)
                        if (m.MeshName == tempMesh.MeshName)
                            continue;
                    break;
                }

                // Insert Mesh
                loadedMeshes.push_back(tempMesh);

                // Cleanup
                vertices.clear();
                indices.clear();
            }

#ifdef OBJL_CONSOLE_OUTPUT
            outputIndicator = 0;
#endif
        }
        // Load Materials
        if (Util::firstToken(curline) == "mtllib")
        {
            // Generate LoadedMaterial

            // Generate a path to the material file
            std::vector<std::string> temp;
            Util::split(path, temp, "/");

            std::string pathtomat = "";

            if (temp.size() != 1)
            {
                for (int i = 0; i < temp.size() - 1; i++)
                {
                    pathtomat += temp[i] + "/";
                }
            }


            pathtomat += Util::tail(curline);

#ifdef OBJL_CONSOLE_OUTPUT
            std::cout << std::endl << "- find materials in: " << pathtomat << std::endl;
#endif

            // Load Materials
            LoadMaterials(pathtomat);
        }
    }

#ifdef OBJL_CONSOLE_OUTPUT
    std::cout << std::endl;
#endif

    // Deal with last mesh

    if (!indices.empty() && !vertices.empty())
    {
        // Create Mesh
        tempMesh = MeshData(vertices, indices);
        tempMesh.MeshName = meshname;

        // Insert Mesh
        loadedMeshes.push_back(tempMesh);
    }

    file.close();

    // Set Materials for each Mesh
    for (int i = 0; i < meshMatNames.size(); i++)
    {
        std::string matname = meshMatNames[i];

        // Find corresponding material name in loaded materials
        // when found copy material variables into mesh material
        for (int j = 0; j < loadedMaterials.size(); j++)
        {
            if (loadedMaterials[j].name == matname)
            {
                loadedMeshes[i].MeshMaterial = loadedMaterials[j];
                break;
            }
        }
    }

    if (loadedMeshes.empty() && loadedVertices.empty() && loadedIndices.empty())
    {
        return false;
    }
    else
    {
        return true;
    }
}

void ObjLoader::GenVerticesFromRawOBJ(std::vector<VertexData>& oVerts,
                                const std::vector<Vector3f>& iPositions,
                                const std::vector<Vector2f>& iTCoords,
                                const std::vector<Vector3f>& iNormals,
                                std::string icurline)
{
    std::vector<std::string> sface, svert;
    VertexData vVert;
    Util::split(Util::tail(icurline), sface, " ");

    bool noNormal = false;

    // For every given vertex do this
    for (int i = 0; i < int(sface.size()); i++)
    {
        // See What type the vertex is.
        int vtype;

        Util::split(sface[i], svert, "/");

        // Check for just position - v1
        if (svert.size() == 1)
        {
            // Only position
            vtype = 1;
        }

        // Check for position & texture - v1/vt1
        if (svert.size() == 2)
        {
            // Position & Texture
            vtype = 2;
        }

        // Check for Position, Texture and Normal - v1/vt1/vn1
        // or if Position and Normal - v1//vn1
        if (svert.size() == 3)
        {
            if (svert[1] != "")
            {
                // Position, Texture, and Normal
                vtype = 4;
            }
            else
            {
                // Position & Normal
                vtype = 3;
            }
        }

        // Calculate and store the vertex
        switch (vtype)
        {
            case 1: // P
            {
                vVert.pt = Util::getElement(iPositions, svert[0]);
                vVert.texCoord = Vector2f(0, 0);
                noNormal = true;
                oVerts.push_back(vVert);
                break;
            }
            case 2: // P/T
            {
                vVert.pt = Util::getElement(iPositions, svert[0]);
                vVert.texCoord = Util::getElement(iTCoords, svert[1]);
                noNormal = true;
                oVerts.push_back(vVert);
                break;
            }
            case 3: // P//N
            {
                vVert.pt = Util::getElement(iPositions, svert[0]);
                vVert.texCoord = Vector2f(0, 0);
                vVert.normal = Util::getElement(iNormals, svert[2]);
                oVerts.push_back(vVert);
                break;
            }
            case 4: // P/T/N
            {
                vVert.pt = Util::getElement(iPositions, svert[0]);
                vVert.texCoord = Util::getElement(iTCoords, svert[1]);
                vVert.normal = Util::getElement(iNormals, svert[2]);
                oVerts.push_back(vVert);
                break;
            }
            default:
            {
                break;
            }
        }
    }

    // take care of missing normals
    // these may not be truly acurate but it is the
    // best they get for not compiling a mesh with normals
    if (noNormal)
    {
        Vector3f A = oVerts[0].pt - oVerts[1].pt;
        Vector3f B = oVerts[2].pt - oVerts[1].pt;

        Vector3f normal = A.cross(B);

        for (int i = 0; i < int(oVerts.size()); i++)
        {
            oVerts[i].normal = normal;
        }
    }
}

void ObjLoader::VertexTriangluation(std::vector<unsigned int>& oIndices,
                                    const std::vector<VertexData>& iVerts)
{
    // If there are 2 or less verts,
    // no triangle can be created,
    // so exit
    if (iVerts.size() < 3)
    {
        return;
    }
    // If it is a triangle no need to calculate it
    if (iVerts.size() == 3)
    {
        oIndices.push_back(0);
        oIndices.push_back(1);
        oIndices.push_back(2);
        return;
    }

    // Create a list of vertices
    std::vector<VertexData> tVerts = iVerts;

    while (true)
    {
        // For every vertex
        for (int i = 0; i < int(tVerts.size()); i++)
        {
            // pPrev = the previous vertex in the list
            VertexData pPrev;
            if (i == 0)
            {
                pPrev = tVerts[tVerts.size() - 1];
            }
            else
            {
                pPrev = tVerts[i - 1];
            }

            // pCur = the current vertex;
            VertexData pCur = tVerts[i];

            // pNext = the next vertex in the list
            VertexData pNext;
            if (i == tVerts.size() - 1)
            {
                pNext = tVerts[0];
            }
            else
            {
                pNext = tVerts[i + 1];
            }

            // Check to see if there are only 3 verts left
            // if so this is the last triangle
            if (tVerts.size() == 3)
            {
                // Create a triangle from pCur, pPrev, pNext
                for (int j = 0; j < int(tVerts.size()); j++)
                {
                    if (iVerts[j].pt == pCur.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == pPrev.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == pNext.pt)
                        oIndices.push_back(j);
                }

                tVerts.clear();
                break;
            }
            if (tVerts.size() == 4)
            {
                // Create a triangle from pCur, pPrev, pNext
                for (int j = 0; j < int(iVerts.size()); j++)
                {
                    if (iVerts[j].pt == pCur.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == pPrev.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == pNext.pt)
                        oIndices.push_back(j);
                }

                Vector3f tempVec;
                for (int j = 0; j < int(tVerts.size()); j++)
                {
                    if (tVerts[j].pt != pCur.pt
                        && tVerts[j].pt != pPrev.pt
                        && tVerts[j].pt != pNext.pt)
                    {
                        tempVec = tVerts[j].pt;
                        break;
                    }
                }

                // Create a triangle from pCur, pPrev, pNext
                for (int j = 0; j < int(iVerts.size()); j++)
                {
                    if (iVerts[j].pt == pPrev.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == pNext.pt)
                        oIndices.push_back(j);
                    if (iVerts[j].pt == tempVec)
                        oIndices.push_back(j);
                }

                tVerts.clear();
                break;
            }

            // If Vertex is not an interior vertex
            float angle = Util::AngleBetweenV3(pPrev.pt - pCur.pt, pNext.pt - pCur.pt) * (180 / Util::PI);
            if (angle <= 0 && angle >= 180)
                continue;

            // If any vertices are within this triangle
            bool inTri = false;
            for (int j = 0; j < int(iVerts.size()); j++)
            {
                if (Util::inTriangle(iVerts[j].pt, pPrev.pt, pCur.pt, pNext.pt)
                    && iVerts[j].pt != pPrev.pt
                    && iVerts[j].pt != pCur.pt
                    && iVerts[j].pt != pNext.pt)
                {
                    inTri = true;
                    break;
                }
            }
            if (inTri)
                continue;

            // Create a triangle from pCur, pPrev, pNext
            for (int j = 0; j < int(iVerts.size()); j++)
            {
                if (iVerts[j].pt == pCur.pt)
                    oIndices.push_back(j);
                if (iVerts[j].pt == pPrev.pt)
                    oIndices.push_back(j);
                if (iVerts[j].pt == pNext.pt)
                    oIndices.push_back(j);
            }

            // Delete pCur from the list
            for (int j = 0; j < int(tVerts.size()); j++)
            {
                if (tVerts[j].pt == pCur.pt)
                {
                    tVerts.erase(tVerts.begin() + j);
                    break;
                }
            }

            // reset i to the start
            // -1 since loop will add 1 to it
            i = -1;
        }

        // if no triangles were created
        if (oIndices.size() == 0)
            break;

        // if no more vertices
        if (tVerts.size() == 0)
            break;
    }
}

bool ObjLoader::LoadMaterials(std::string path)
{
    // If the file is not a material file return false
    if (path.substr(path.size() - 4, path.size()) != ".mtl")
        return false;

    std::ifstream file(path);

    // If the file is not found return false
    if (!file.is_open())
        return false;

    MaterialData tempMaterial;

    bool listening = false;

    // Go through each line looking for material variables
    std::string curline;
    while (std::getline(file, curline))
    {
        // new material and material name
        if (Util::firstToken(curline) == "newmtl")
        {
            if (!listening)
            {
                listening = true;

                if (curline.size() > 7)
                {
                    tempMaterial.name = Util::tail(curline);
                }
                else
                {
                    tempMaterial.name = "none";
                }
            }
            else
            {
                // Generate the material

                // Push Back loaded Material
                loadedMaterials.push_back(tempMaterial);

                // Clear Loaded Material
                tempMaterial = MaterialData();

                if (curline.size() > 7)
                {
                    tempMaterial.name = Util::tail(curline);
                }
                else
                {
                    tempMaterial.name = "none";
                }
            }
        }
        // Ambient Color
        if (Util::firstToken(curline) == "Ka")
        {
            std::vector<std::string> temp;
            Util::split(Util::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.kambient = Vector3f(std::stof(temp[0]),std::stof(temp[1]),std::stof(temp[2]));
        }
        // Diffuse Color
        if (Util::firstToken(curline) == "Kd")
        {
            std::vector<std::string> temp;
            Util::split(Util::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.kdiffuse = Vector3f(std::stof(temp[0]),std::stof(temp[1]),std::stof(temp[2]));
        }
        // Specular Color
        if (Util::firstToken(curline) == "Ks")
        {
            std::vector<std::string> temp;
            Util::split(Util::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.kspecular = Vector3f(std::stof(temp[0]),std::stof(temp[1]),std::stof(temp[2]));
        }
        // Specular Exponent
        if (Util::firstToken(curline) == "Ns")
        {
            tempMaterial.specualrExp = std::stof(Util::tail(curline));
        }
        // Optical Density
        if (Util::firstToken(curline) == "Ni")
        {
            tempMaterial.density = std::stof(Util::tail(curline));
        }
        // Dissolve
        if (Util::firstToken(curline) == "d")
        {
            tempMaterial.dissolve = std::stof(Util::tail(curline));
        }
        // Illumination
        if (Util::firstToken(curline) == "illum")
        {
            tempMaterial.illum = std::stoi(Util::tail(curline));
        }
        // Ambient Texture Map
        if (Util::firstToken(curline) == "map_Ka")
        {
            tempMaterial.mapAmbient = Util::tail(curline);
        }
        // Diffuse Texture Map
        if (Util::firstToken(curline) == "map_Kd")
        {
            tempMaterial.mapDiffuse = Util::tail(curline);
        }
        // Specular Texture Map
        if (Util::firstToken(curline) == "map_Ks")
        {
            tempMaterial.mapSpecular = Util::tail(curline);
        }
        // Specular Hightlight Map
        if (Util::firstToken(curline) == "map_Ns")
        {
            tempMaterial.mapHightlight = Util::tail(curline);
        }
        // Alpha Texture Map
        if (Util::firstToken(curline) == "map_d")
        {
            tempMaterial.mapAlpha = Util::tail(curline);
        }
        // Bump Map
        if (Util::firstToken(curline) == "map_Bump" || Util::firstToken(curline) == "map_bump" || Util::firstToken(curline) == "bump")
        {
            tempMaterial.mapBump = Util::tail(curline);
        }
    }

    // Deal with last material

    // Push Back loaded Material
    loadedMaterials.push_back(tempMaterial);

    // Test to see if anything was loaded
    // If not return false
    if (loadedMaterials.empty())
        return false;
        // If so return true
    else
        return true;
}

