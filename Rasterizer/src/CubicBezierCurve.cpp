#include "CubicBezierCurve.hpp"


void CubicBezierCurve::ReadPoints()
{
    ctrlPts.push_back(Vector2f(200,200));
    ctrlPts.push_back(Vector2f(300,600));
    ctrlPts.push_back(Vector2f(700,650));
    ctrlPts.push_back(Vector2f(560,200));
}

void CubicBezierCurve::ReadCirclePts()
{
    double r = 150;
    double m = 0.5523;
    auto offset = Vector2f(200,200);
    ctrlPts.push_back(Vector2f(r,0) + offset);
    ctrlPts.push_back(Vector2f(r,r * m) + offset);
    ctrlPts.push_back(Vector2f(r * m,r) + offset);
    ctrlPts.push_back(Vector2f(0,r) + offset);

    ctrlPts.push_back(Vector2f(-r * m,r) + offset);
    ctrlPts.push_back(Vector2f(-r,r * m) + offset);
    ctrlPts.push_back(Vector2f(-r,0) + offset);
    ctrlPts.push_back(Vector2f(-r,-r * m) + offset);

    ctrlPts.push_back(Vector2f(-r * m,-r) + offset);
    ctrlPts.push_back(Vector2f(0,-r) + offset);
    ctrlPts.push_back(Vector2f(r * m,-r) + offset);
    ctrlPts.push_back(Vector2f(r,-r * m) + offset);
}

void CubicBezierCurve::DrawCurveFunc(AppContext* context)
{
    
}

void CubicBezierCurve::DrawCurveIterator(AppContext* context)
{
    auto color = Vector3f(0,0,1);
    auto lineRenderer = context->getLineRenderer();
    auto lastPt = ctrlPts[0];
    double tStep = 0.01;
    for(double t = 0;t < 1;t += tStep)
    {
        Vector2f p0,p1,p2,p3,p4,p5;
        p0 = (1 - t) * ctrlPts[0] + t * ctrlPts[1];
        p1 = (1 - t) * ctrlPts[1] + t * ctrlPts[2];
        p2 = (1 - t) * ctrlPts[2] + t * ctrlPts[3];
        p3 = (1 - t) * p0 + t * p1;
        p4 = (1 - t) * p1 + t * p2;
        p5 = (1 - t) * p3 + t * p4;
        lineRenderer->MidPoint2(Vector3f(lastPt.x(),lastPt.y(),0),Vector3f(p5.x(),p5.y(),0),color);
        lastPt = p5;
    }
}