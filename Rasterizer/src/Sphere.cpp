#include "Sphere.hpp"

Sphere::Sphere()
{

}

Sphere::~Sphere()
{

}

void Sphere::SetRecursion(int number)
{
    this->resursionNum = number;
}

void Sphere::ReadVertex()
{
    double beta = 0.618;
    verts.push_back({Vector3f(0,1,beta),Vector3f(0,0,0)});
    verts.push_back({Vector3f(0,1,-beta),Vector3f(1,0,0)});
    verts.push_back({Vector3f(1,beta,0),Vector3f(1,1,0)});
    verts.push_back({Vector3f(1,-beta,0),Vector3f(0,1,0)});
    verts.push_back({Vector3f(0,-1,-beta),Vector3f(0,0,1)});
    verts.push_back({Vector3f(0,-1,beta),Vector3f(1,0,1)});
    verts.push_back({Vector3f(beta,0,1),Vector3f(1,1,1)});
    verts.push_back({Vector3f(-beta,0,1),Vector3f(0,1,1)});

    verts.push_back({Vector3f(beta,0,-1),Vector3f(0,0,0)});
    verts.push_back({Vector3f(-beta,0,-1),Vector3f(1,0,0)});
    verts.push_back({Vector3f(-1,beta,0),Vector3f(1,1,0)});
    verts.push_back({Vector3f(-1,-beta,0),Vector3f(0,1,0)});
    //球心
    verts.push_back({Vector3f(0,0,0),Vector3f(1,0,1)});

    transform.SetVertDatas(&verts);
}

void Sphere::ReadFaces()
{
    faces.push_back({0,6,2});
    faces.push_back({2,6,3});
    faces.push_back({3,6,5});
    faces.push_back({5,6,7});
    faces.push_back({0,7,6});

    faces.push_back({2,3,8});
    faces.push_back({1,2,8});
    faces.push_back({0,2,1});
    faces.push_back({0,1,10});
    faces.push_back({1,9,10});

    faces.push_back({1,8,9});
    faces.push_back({3,4,8});
    faces.push_back({3,5,4});
    faces.push_back({4,5,11});
    faces.push_back({7,10,11});

    faces.push_back({0,10,7});
    faces.push_back({4,11,9});
    faces.push_back({4,9,8});
    faces.push_back({5,7,11});
    faces.push_back({9,11,10});
}

void Sphere::Draw(AppContext* context)
{
    int nFace = faces.size();
    for(int i = 0;i < nFace;i++)
    {
        Vector3f pt[faces[i].FNum];
        for(int j = 0;j < faces[i].FNum;j++)
        {
            pt[j] = verts[faces[i].vertIdxs[j]].pt;
        }
        SubDivide(context,pt[0],pt[1],pt[2],this->resursionNum);
    }
    
}

void Sphere::SubDivide(AppContext* context,Vector3f p0,Vector3f p1,Vector3f p2,int resursion)
{
    if(0 == resursion)
    {
        DrawTriangle(context,p0,p1,p2);
        return;
    }
    Vector3f p01,p12,p20;
    p01 = (p0 + p1) / 2.0;
    p12 = (p1 + p2) / 2.0;
    p20 = (p2 + p0) / 2.0;
    Normalize(p01);
    Normalize(p12);
    Normalize(p20);

    SubDivide(context,p0,p01,p20,resursion - 1);
    SubDivide(context,p1,p12,p01,resursion - 1);
    SubDivide(context,p2,p20,p12,resursion - 1);
    SubDivide(context,p01,p12,p20,resursion - 1);
}

void Sphere::Normalize(Vector3f& pt)
{
    Vector3f dir = verts[0].pt - verts[12].pt;
    radius = dir.norm();
    pt -= verts[12].pt;
    if(0 == pt.norm())
    {
        return;
    }
    pt /= pt.norm();
    pt *= radius;
    pt += verts[12].pt;
}

void Sphere::DrawTriangle(AppContext* context,Vector3f p0,Vector3f p1,Vector3f p2)
{
    auto lineRenderer = context->getLineRenderer();
    auto color = Vector3f(0,0,1);
    Vector3f triV[3] = {p0,p1,p2};
    Face3 triF{0,1,2};
    Vector2f screenPt[3];
    for(int i = 0;i < triF.FNum;i++)
    {
        screenPt[i] = {triV[triF.vertIdxs[i]].x(),triV[triF.vertIdxs[i]].y()};
    }
    lineRenderer->MidPoint2(Vector3f(screenPt[0].x(),screenPt[0].y(),0),Vector3f(screenPt[1].x(),screenPt[1].y(),0),color);
    lineRenderer->MidPoint2(Vector3f(screenPt[1].x(),screenPt[1].y(),0),Vector3f(screenPt[2].x(),screenPt[2].y(),0),color);
    lineRenderer->MidPoint2(Vector3f(screenPt[2].x(),screenPt[2].y(),0),Vector3f(screenPt[0].x(),screenPt[0].y(),0),color);
}

