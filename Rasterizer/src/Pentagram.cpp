#include "Pentagram.hpp"


void Pentagram::ReadPoints()
{
    const float PI = 3.1415926;
    double theta = 2 * PI / 5;
    double start = PI / 2 - theta;
    double radius = 100;
    auto center = Vector2f(SCREEN_HEIGHT / 2,SCREEN_WIDTH / 2);
    for(int i = 0;i < 5;i++)
    {
        auto x = cos(i * theta + start) * radius;
        auto y = sin(i * theta + start) * radius;
        auto pt = center + Vector2f(x,y);
        pts.push_back(pt);
    }

    transform.SetVerts(&pts);
}

void Pentagram::OnDraw(AppContext* context)
{
    auto lineRenderer = context->getLineRenderer();
    auto color = Vector3f(0,0,1);
    const int len = 5;
    for(int i = 0,j = 0;i < len;i ++,j += 2)
    {
        int start = j % len;
        int end = (j + 2) % len;
        auto startPt = Vector3f(pts[start].x(),pts[start].y(),0);
        auto endPt = Vector3f(pts[end].x(),pts[end].y(),0);
        lineRenderer->MidPoint2(startPt,endPt,color);
    }
}