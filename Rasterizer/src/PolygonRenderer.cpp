#include "PolygonRenderer.hpp"

void PolygonRenderer::FramePolygon(Polygon& polygon,Vector3f color)
{
    auto vertxNum = polygon.GetVerts().size();
     for(int i = 0;i < vertxNum;i++){
        auto pt1 = polygon.GetVerts()[i % vertxNum];
        auto pt2 = polygon.GetVerts()[(i + 1) % vertxNum];
        lineRender->MidPoint2(Vector3f(pt1.x(),pt1.y(),0),Vector3f(pt2.x(),pt2.y(),0),color);
    }
}

void PolygonRenderer::CreateBucket(Polygon& polygon)
{
    auto verts = polygon.GetVerts();
    auto scanyMinMax = std::minmax_element(begin(verts),end(verts),
    [&](Vector2f& a,Vector2f& b) { return a.y() < b.y(); });
    auto scanYMin = (int)std::min((*scanyMinMax.first).y(),(*scanyMinMax.second).y());
    auto scanYMax = (int)std::max((*scanyMinMax.first).y(),(*scanyMinMax.second).y());

    for(int i = scanYMin;i <= scanYMax;i++)
    {
        if(scanYMin == i)
		{
			headBucket = new BucketLinkNode;
			curBucket = headBucket;
			curBucket->ScanLine = scanYMin;
			curBucket->pEdgeLink = nullptr;
			curBucket->next = nullptr;
		}
		else
		{
			curBucket->next = new BucketLinkNode;
			curBucket = curBucket->next;
			curBucket->ScanLine = i;
            //这里还是得赋空值，防止一个未初始化的指针不知道指向的什么值
			curBucket->pEdgeLink = nullptr;
			curBucket->next = nullptr;				
		}
    }
}

void PolygonRenderer::CreateEdgeTable(Polygon& polygon)
{
    auto verts = polygon.GetVerts();
    auto vNumber = verts.size();
	edges.resize(vNumber + 1);
    for(int i = 0;i < vNumber;i++)
	{
		curBucket = headBucket;
		int j = i + 1;
        j = j == vNumber ? 0 : j;
		if(verts[j].y() > verts[i].y())
		{
			while(curBucket->ScanLine != verts[i].y())
			{
				curBucket = curBucket->next;
			}
			edges[i].x = verts[i].x();
			edges[i].yMax = verts[j].y();
			edges[i].k = double((verts[j].x() - verts[i].x())) / (verts[j].y() - verts[i].y());			
			edges[i].next = nullptr;
			curEdge = curBucket->pEdgeLink;
			if(curBucket->pEdgeLink == nullptr)
			{
				curEdge = &edges[i];
				curBucket->pEdgeLink = curEdge;
			}
			else
			{
				while(curEdge->next != nullptr)
				{
					curEdge = curEdge->next;				
				}
				curEdge->next = &edges[i];
			}
		}
		if(verts[j].y() < verts[i].y())
		{
			while(curBucket->ScanLine != verts[j].y())
			{
				curBucket = curBucket->next;				
			}
			edges[i].x = verts[j].x();
			edges[i].yMax = verts[i].y();
			edges[i].k = double((verts[i].x() - verts[j].x()))/(verts[i].y()-verts[j].y());			
			edges[i].next = nullptr;
			curEdge = curBucket->pEdgeLink;
			if(curEdge==nullptr)
			{
				curEdge = &edges[i];
				curBucket->pEdgeLink = curEdge;
			}
			else
			{
				while(curEdge->next != nullptr)
				{
					curEdge = curEdge->next;				
				}
				curEdge->next = &edges[i];
			}
		}
	}
	curBucket = nullptr;
	curEdge = nullptr;
}

void PolygonRenderer::AddEdge(EdgeLinkNode* edgeNode)
{
	edgeLinkList1 = headEdge;
	if(edgeLinkList1 == nullptr)
	{
		edgeLinkList1 = edgeNode;
		headEdge = edgeLinkList1;
	}
	else
	{
		//TODO:改成双链表
		while(edgeLinkList1->next != nullptr)
		{
			edgeLinkList1 = edgeLinkList1->next;
		}
		edgeLinkList1->next = edgeNode;
	}
}

void PolygonRenderer::EdgeSort()
{
	edgeLinkList1 = headEdge;
	if(edgeLinkList1 == nullptr)
	{
		return;
	}
	if(edgeLinkList1->next == nullptr)
	{
		return;
	}
	else
	{
		if(edgeLinkList1->next->x < edgeLinkList1->x)
		{
			edgeLinkList2 = edgeLinkList1->next;
			edgeLinkList1->next = edgeLinkList2->next;
			edgeLinkList2->next = edgeLinkList1;
			headEdge = edgeLinkList2;
		}
		edgeLinkList2 = headEdge;
		edgeLinkList1 = headEdge->next;		
		while(edgeLinkList1->next != nullptr)
		{
			if(edgeLinkList1->next->x < edgeLinkList1->x)
			{
				edgeLinkList2->next = edgeLinkList1->next;
				edgeLinkList1->next = edgeLinkList1->next->next;
				edgeLinkList2->next->next = edgeLinkList1;
				edgeLinkList2 = edgeLinkList2->next;
			}
			else
			{
				edgeLinkList2 = edgeLinkList1;
				edgeLinkList1 = edgeLinkList1->next;
			}
		}
	}
}

void PolygonRenderer::AETPolygonFill(Vector3f color)
{
	headEdge = nullptr;
	for(curBucket = headBucket;curBucket != nullptr;curBucket = curBucket->next)
	{
		for(curEdge = curBucket->pEdgeLink;curEdge != nullptr;curEdge = curEdge->next)			
		{
			EdgeLinkNode *tempEdge = new EdgeLinkNode;
			tempEdge->x = curEdge->x;
			tempEdge->yMax = curEdge->yMax;
			tempEdge->k = curEdge->k;
			tempEdge->next = nullptr;		
			AddEdge(tempEdge);
		}
		EdgeSort();
		edgeLinkList1 = headEdge;
		if(edgeLinkList1 == nullptr)
		{
			return;
		}
		while(curBucket->ScanLine >= edgeLinkList1->yMax)
		{
			edgeLinkList1 = edgeLinkList1->next;
			headEdge = edgeLinkList1;
			if(headEdge == nullptr)
			{
				return;
			}
		}
		if(edgeLinkList1->next != nullptr)
		{
			edgeLinkList2 = edgeLinkList1;
			edgeLinkList1 = edgeLinkList2->next;
		}
		while(edgeLinkList1 != nullptr)
		{
			if(curBucket->ScanLine >= edgeLinkList1->yMax)
			{
				edgeLinkList2->next = edgeLinkList1->next;
				edgeLinkList1->next = nullptr;
				edgeLinkList1 = edgeLinkList2->next;
			}
			else
			{
				edgeLinkList2 = edgeLinkList1;
				edgeLinkList1 = edgeLinkList2->next;
			}
		}
		bool In = false;
		double xb,xe;
		for(edgeLinkList1 = headEdge;edgeLinkList1 != nullptr;edgeLinkList1 = edgeLinkList1->next)
		{
			if(In == false)
			{
				xb = edgeLinkList1->x;
				In = true;
			}
			else
			{
				xe = edgeLinkList1->x - 1;
				for(double x = xb;x <= xe;x++)
				{
					rasterizer->setPixel(Vector3f(x,curBucket->ScanLine,0),color);
				}
				In=false;
			}		
		}
		for(edgeLinkList1 = headEdge;edgeLinkList1 != nullptr;edgeLinkList1 = edgeLinkList1->next)
		{
			edgeLinkList1->x = edgeLinkList1->x + edgeLinkList1->k;//x=x+1/k			 
		}				
	}
	delete headBucket; 
	delete curBucket;
	delete curEdge;
	delete headEdge;
}

void PolygonRenderer::AETPolygonFill(Polygon& polygon,Vector3f color)
{
	CreateBucket(polygon);
	CreateEdgeTable(polygon);
	AETPolygonFill(color);
}

//原理:
//对任意多边形而言，一条直线切过去必是偶数个交点
//参考：计算几何，拓扑学
void PolygonRenderer::FillWithBound(Polygon& polygon,Vector3f color,int maxX)
{
	Vector3f backGroundColor = Vector3f(0.0f,0.0f,0.0f);
	auto verts = polygon.GetVerts();
    auto vNumber = verts.size();
	int m,n,ymin,ymax;	
	double x,y,k;
	for(int i = 0;i <= vNumber;i++)
	{
		m = i,n = i + 1;
		n = vNumber == n ? 0 : n;
		k = (double(verts[m].x() - verts[n].x())) / (verts[m].y() - verts[n].y());

		ymin = std::min(verts[m].y(),verts[n].y());
		ymax = std::max(verts[m].y(),verts[n].y());
		x = verts[m].y() < verts[n].y() ? verts[m].x() : verts[n].x();
		
		for(y = ymin;y < ymax;y++)
		{			
			for(int jx = std::round(x);jx < maxX;jx++)
			{
				if(rasterizer->getPixel(jx,std::round(y)) == color)
				{
					rasterizer->setPixel(jx,std::round(y),backGroundColor);
				}
				else
				{
					rasterizer->setPixel(jx,std::round(y),color);
				}		
			}
			x += k;
		}		
	}	
}

//这个方法还有问题
void PolygonRenderer::StackSeedFill(Polygon& polygon,Vector3f fillcolor,Vector2f startPt)
{
	auto boundaryColor = Vector3f(0,0,1);
	auto headStk = new StackNode;
	headStk->pixelPoint = startPt;
	headStk->next = nullptr;

	auto comparePixelColor = [&](const Vector3f& pColor)->bool{
		return pColor != boundaryColor && pColor != fillcolor;
	};

	while(headStk != nullptr)
	{	
		StackNode *popPt = nullptr;
		popPt = StackNode::pop(headStk);
		if(popPt == nullptr)
		{
			std::cout << "stack is empty" << std::endl;
			return;
		}
		
		rasterizer->setPixel(popPt->pixelPoint.x(),popPt->pixelPoint.y(),fillcolor);
		auto ptLeft = Vector2f(popPt->pixelPoint.x() - 1,popPt->pixelPoint.y());
		auto pixelColor = rasterizer->getPixel(ptLeft);
		if(comparePixelColor(pixelColor))
		{
			StackNode::push(headStk,ptLeft);
		}
		
		auto ptTop = Vector2f(popPt->pixelPoint.x(),popPt->pixelPoint.y() + 1);
		pixelColor = rasterizer->getPixel(ptTop);
		if(comparePixelColor(pixelColor))
		{
			StackNode::push(headStk,ptTop);
		}

		auto ptRight = Vector2f(popPt->pixelPoint.x() + 1,popPt->pixelPoint.y());
		pixelColor = rasterizer->getPixel(ptRight);
		if(comparePixelColor(pixelColor))
		{
			StackNode::push(headStk,ptRight);
		}
		
		auto ptBottom = Vector2f(popPt->pixelPoint.x(),popPt->pixelPoint.y() - 1);
		pixelColor = rasterizer->getPixel(ptBottom);
		if(comparePixelColor(pixelColor))
		{
			StackNode::push(headStk,ptBottom);
		}
		delete popPt;
	}
}

void PolygonRenderer::BoundaryFill4(int x,int y,const Vector3f& boundColor,const Vector3f& seedColor)
{
	auto curColor = rasterizer->getPixel(x,y);
	if(curColor != boundColor && curColor != seedColor)
	{
		rasterizer->setPixel(x,y,seedColor);
		BoundaryFill4(x + 1,y,boundColor,seedColor);
		BoundaryFill4(x,y + 1,boundColor,seedColor);
		BoundaryFill4(x - 1,y,boundColor,seedColor);
		BoundaryFill4(x,y - 1,boundColor,seedColor);
	}
}

void PolygonRenderer::BoundaryFill8(int x,int y,const Vector3f& boundColor,const Vector3f& seedColor)
{
	auto curColor = rasterizer->getPixel(x,y);
	if(curColor != boundColor && curColor != seedColor)
	{
		rasterizer->setPixel(x,y,seedColor);
		for(int i = -1;i <= 1;i++)
		{
			for(int j = -1;j <= 1;j++)
			{
				if(i == 0 && j == 0)
				{
					continue;
				}
				BoundaryFill4(x + i,y + j,boundColor,seedColor);
			}
		}
	}
}