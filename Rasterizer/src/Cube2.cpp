#include "Cube2.hpp"

Cube2::Cube2()
{

}

Cube2::~Cube2()
{

}

void Cube2::SetLight(Lighting* light)
{
    pLighting = light;
}

void Cube2::ReadVertex()
{
    verts.push_back(Vector3f(0,0,0));
    verts.push_back(Vector3f(1,0,0));
    verts.push_back(Vector3f(1,1,0));
    verts.push_back(Vector3f(0,1,0));
    verts.push_back(Vector3f(0,0,1));
    verts.push_back(Vector3f(1,0,1));
    verts.push_back(Vector3f(1,1,1));
    verts.push_back(Vector3f(0,1,1));

    transform.SetVerts(&verts);
}

void Cube2::ReadFaces()
{
    faces.push_back(Face4(4,5,6,7));
    faces.push_back(Face4(0,3,2,1));
    faces.push_back(Face4(0,4,7,3));
    faces.push_back(Face4(1,2,6,5));
    faces.push_back(Face4(2,3,7,6));
    faces.push_back(Face4(0,1,5,4));
}

void Cube2::OnDraw(AppContext* context)
{
    auto color = Vector3f(0,0,1);
    const int faceNum = faces.size();
    Vector3f screenPts[4];
    Vector3f ptNormal[4];
    auto projection = context->getProjection();
    auto triRenderer = context->getTriangleRender();
    auto eyePt = projection->getEye();
    for(int nFace = 0;nFace < faceNum;nFace++)
    {
        Vector3f v1 = verts[faces[nFace].vertIdxs[1]] - verts[faces[nFace].vertIdxs[0]];
        Vector3f v2 = verts[faces[nFace].vertIdxs[2]] - verts[faces[nFace].vertIdxs[0]];
        Vector3f fNormal = v1.cross(v2);
        for(int nPt = 0;nPt < faces[nFace].vertIdxs.size();nPt++)
        {
            //screenPts[nPt] = projection->PerspectiveProjection(verts[faces[nFace].vertIdxs[nPt]]);
            Vector3f fPt = verts[faces[nFace].vertIdxs[nPt]];
            auto sPt = projection->PerspectiveProjection(fPt);
            screenPts[nPt] = Vector3f(sPt.x(),sPt.y(),fPt.z());
            ptNormal[nPt] = fNormal;
        }
        VertexData vertData[3];
        vertData[0].pt = screenPts[0];
        vertData[0].normal = ptNormal[0];
        vertData[1].pt = screenPts[1];
        vertData[1].normal = ptNormal[1];
        vertData[2].pt =screenPts[2];
        vertData[2].normal = ptNormal[2];
        triRenderer->SetTriangleVertData(vertData);
        triRenderer->PhongShader(eyePt,pLighting);

        VertexData vertData2[3];
        vertData2[0].pt = screenPts[0];
        vertData2[0].normal = ptNormal[0];
        vertData2[1].pt = screenPts[2];
        vertData2[1].normal = ptNormal[2];
        vertData2[2].pt = screenPts[3];
        vertData2[2].normal = ptNormal[3];
        triRenderer->SetTriangleVertData(vertData2);
        triRenderer->PhongShader(eyePt,pLighting);
    }
}