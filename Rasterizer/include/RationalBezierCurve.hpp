#pragma once

#include "AppContext.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class RationalBezierCurve
{
    public:
        RationalBezierCurve(){};
        virtual ~RationalBezierCurve(){};

        void ReadCtrlPts();
        void DrawCurve(AppContext* context);
        void DrawCirle(AppContext* context);
        void ClearPts(){}
    private:
        std::vector<Vector2f> ctrlPts;
        std::vector<Vector2f> Pts;
        std::vector<double> weights;
};