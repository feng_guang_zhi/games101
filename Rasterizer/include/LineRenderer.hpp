#pragma once

#include<Eigen/Eigen>
#include"Rasterizer.hpp"
#include<cmath>
#include<algorithm>

using Vector3f = Eigen::Vector3f;

class LineRenderer{
    private:
        Rasterizer* rasterizer;
    public:
        LineRenderer(Rasterizer* rast){
            rasterizer = rast;
        }
    
        void DDALine(Vector3f start,Vector3f end);
        void MidBresenhamLine1(Vector3f start,Vector3f end, Vector3f color);
        void MidPoint(Vector3f start,Vector3f end, Vector3f color);
        void MidPoint2(Vector3f start,Vector3f end, Vector3f color);
        void Fllower(Vector3f center,float radius);
        void MidPointCircle(Vector3f center,float radius,Vector3f color);
        void MidPointEllipse(Vector3f center,float a,float b,Vector3f color,float theta = 0);
        void CirclePoints(Vector3f center,int x,int y,Vector3f color);
        void EllipsePoints(Vector3f center,int x,int y,Vector3f color);
        void FlowCircle(Vector3f center,Vector3f color);
        Vector3f LinearInterp(double m,double start,double end,Vector3f cstart,Vector3f cend);
        void MidPointLerp(Vector3f start,Vector3f end, Vector3f startc,Vector3f endc);
};