#pragma once

#include "AppContext.hpp"

class IUpdate
{
public:
	virtual void Update() = 0;
};

class IOnKeyDown
{
public:
	virtual void OnKeyDown(int key) = 0;
};

class ICommand : public IUpdate,public IOnKeyDown
{
protected:
    AppContext* appContext;
public:
	virtual void Excute() = 0;
    virtual void Update() = 0;
    virtual void OnKeyDown(int key) = 0;
};

