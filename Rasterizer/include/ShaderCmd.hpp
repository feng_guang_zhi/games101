#pragma once

#include "ICommand.hpp"
#include "ObjLoader.hpp"
#include "Shader.hpp"
#include "ShaderFunc.hpp"

using Vector3f = Eigen::Vector3f;

class DrawRabitCmd : public ICommand
{
    protected:
        std::function<Vector3f(FragmentShader)> activeFragShader = textureFragmentShader;
        std::vector<Triangle2*> triangleList;
        ObjLoader loader;
        float angle = -140.0f;
        std::string obj_path;
        std::string objFile;
        std::string texture_path;
    public: 
        DrawRabitCmd(){};
        DrawRabitCmd(AppContext* context)
        {
            appContext = context;
            obj_path = "../models/spot/";
            objFile = "../models/spot/spot_triangulated_good.obj";
            texture_path = "spot_texture.png";
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);

            
            // Load .obj File
            loader.LoadFile(objFile);
            for (auto mesh: loader.getLoadedMeshes()) {
                for (int i = 0; i < mesh.Vertices.size(); i += 3) {
                    auto *t = new Triangle2();
                    for (int j = 0; j < 3; j++) {
                        t->setVertex(j, Vector4f(mesh.Vertices[i + j].pt.x(), mesh.Vertices[i + j].pt.y(),
                                                mesh.Vertices[i + j].pt.z(), 1.0));
                        t->setNormal(j, Vector3f(mesh.Vertices[i + j].normal.x(), mesh.Vertices[i + j].normal.y(),
                                                mesh.Vertices[i + j].normal.z()));
                        t->setTexCoord(j, Vector2f(mesh.Vertices[i + j].texCoord.x(),
                                                mesh.Vertices[i + j].texCoord.y()));
                    }
                    triangleList.push_back(t);
                }
            }
           
            appContext->getRasterizer()->setTexture(new Texture(obj_path + texture_path));

            Vector3f eye_pos = {0, 0, 10};
            appContext->getRasterizer()->setFragmentShader(activeFragShader);
            appContext->getRasterizer()->setVertexShader(vertexShader);
            appContext->getRasterizer()->setModel(getModelMatrix(angle));
            appContext->getRasterizer()->setView(getViewMatrix(eye_pos));
            appContext->getRasterizer()->setProjection(getProjectionMatrix(45.0, 2, 0.1, 50));

            appContext->getRasterizer()->drawTriangles(triangleList);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class DrawRabit2Cmd : public DrawRabitCmd
{
    public: 
        DrawRabit2Cmd(){};
        DrawRabit2Cmd(AppContext* context)
        {
            appContext = context;
            activeFragShader = phongFragmentShader;
        }
};

class DrawRabit3Cmd : public DrawRabitCmd
{
    public: 
        DrawRabit3Cmd(){};
        DrawRabit3Cmd(AppContext* context)
        {
            appContext = context;
            activeFragShader = bumpFragmentShader;
            obj_path = "../models/spot/";
            objFile = "../models/spot/spot_triangulated_good.obj";
            texture_path = "hmap.jpg";
        }
};

class DrawRabit4Cmd : public DrawRabitCmd
{
    public: 
        DrawRabit4Cmd(){};
        DrawRabit4Cmd(AppContext* context)
        {
            appContext = context;
            activeFragShader = normalFragmentShader;
            obj_path = "../models/spot/";
            objFile = "../models/spot/spot_triangulated_good.obj";
            texture_path = "hmap.jpg";
        }
};

class DrawRabit5Cmd : public DrawRabitCmd
{
    public: 
        DrawRabit5Cmd(){};
        DrawRabit5Cmd(AppContext* context)
        {
            appContext = context;
            activeFragShader = displacementFragmentShader;
            obj_path = "../models/spot/";
            objFile = "../models/spot/spot_triangulated_good.obj";
            texture_path = "hmap.jpg";
        }
};