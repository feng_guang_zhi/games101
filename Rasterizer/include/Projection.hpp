#pragma once

#include<Eigen/Eigen>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class Projection
{
    public:
        Projection();
        virtual ~Projection(){};
    public:
        //正交投影
        Vector2f OrthogonalProjection(const Vector3f& worldPt);
        //斜等侧投影
        Vector2f CavalierProjection(const Vector3f& worldPt);
        //斜二侧投影
        Vector2f CabinetProjection(const Vector3f& worldPt);
        //透视投影
        Vector2f PerspectiveProjection(const Vector3f& worldPt);

        Vector3f getEye();
        void setEye();
        void setEye(Vector3f eye)
        {
            eyePt = eye;
        }
    private:
        //视点坐标
        Vector3f eyePt;
        //视点半径
        double viewDis;
        //视点距离
        double eyeDis;
};