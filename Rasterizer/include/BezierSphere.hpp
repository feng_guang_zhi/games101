#pragma once

#include "AppContext.hpp"
#include "Patch.hpp"
#include "CubicBezierPatch.hpp"
#include "Transform3.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class BezierSphere
{
    public:
        BezierSphere(){};
        virtual ~BezierSphere(){};
        void readVertex();
        void readFaces();
        void draw(AppContext* context);
        Transform3 getTransform()
        {
            return transform;
        }
    private:
        std::vector<Vector3f> ctrlPts;
        std::vector<Patch<16>> faces;
        Transform3 transform;
};