#pragma once

#include "AppContext.hpp"
#include <Transform2.hpp>
#include "Util.hpp"
#include <cmath>

class Diamond
{
    public:
        Diamond();
        virtual ~Diamond();

        void OnDraw(AppContext* context);
        void ReadPoints();
        std::vector<Vector2f>* getVerts(){ 
            return &pts;
        }
        void SetParameter(int n,double r,Vector2f center);
    private:
        std::vector<Vector2f> pts;
        Transform2 transform;
        int divideNum;
        double radius;
        Vector2f center;
    public:
        Transform2 getTransform(){
            return transform;
        }
};