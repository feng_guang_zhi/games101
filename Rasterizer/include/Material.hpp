#pragma once

#include<Eigen/Eigen>

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;

class Material
{
public:
    Material();
    virtual ~Material();
    //设置环境光的反射率
    void SetAmbient(Vector3f color);
    //设置漫反射光的反射率
    void SetDiffuse(Vector3f color);
    //设置镜面反射光的反射率
    void SetSpecular(Vector3f color);
    //设置自身辐射的颜色
    void SetEmission(Vector3f color);
    //设置高光指数
    void SetExponent(double e);

    Vector3f GetEmission();
    Vector3f GetAmbient();
    Vector3f GetDiffuse();
    Vector3f GetSpecular();
    double GetExponent();
private:
    Vector3f ambient;
    Vector3f diffuse;
    Vector3f specular;
    Vector3f emission;
    double exp;
};