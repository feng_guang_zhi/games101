#pragma once

#include<Eigen/Eigen>
#include<iostream>
#include "ColorVec.h"
#include "CommonData.hpp"

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;
using Vector4f = Eigen::Vector4f;
using Matrix4f = Eigen::Matrix4f;
using Matrix3f = Eigen::Matrix3f;

const float PI = 3.1415926;

class Transform3
{
    public:
        Transform3(){};
        virtual ~Transform3(){};
    private:
        Matrix4f mat;
        std::vector<Vector3f>* pts;
        std::vector<VertexData>* vertDatas;
    public:
        void SetVerts(std::vector<Vector3f>* pts);
        void SetVertDatas(std::vector<VertexData>* verts);
        void Identity(){mat.Identity();};
        void Translate(double tx,double ty,double tz);
        void Scale(double sx,double sy,double sz);
        void Scale(double sx,double sy,double sz,Vector3f pt);
        void Rotate(double beta);
        void RotateX(double beta);
        void RotateY(double beta);
        void RotateZ(double beta);
        void Rotate(double beta,Vector3f pt);
        void RotateArbitraryAxis(double theta, Vector3f axis);
        //point 要变换的点
        //axisPoint 旋转轴的起点
        //axisDirection 旋转轴的方向向量
        //angle 要旋转的角度
        void rotatePointAroundArbitraryAxis(const Vector3f& axisPoint, const Vector3f& axisDirection, float angle);
        Matrix4f getRotMat(Vector3f axis, float angle);
        void rotateAroundAxis(Vector3f& v, const Vector3f& axis, double angle);
        void ReflectOrg();
        void ReflectX();
        void ReflectY();
        void Shear(double b,double c);
        void MultiplyMatrix();
        void MultiplyMatrix2();
        void MultiplyMatrix3();
};