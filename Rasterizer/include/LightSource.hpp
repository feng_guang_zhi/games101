#pragma once

#include<Eigen/Eigen>

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;

class LightSource
{
public:
    LightSource();
    virtual ~LightSource();
    //漫反射
    void SetDiffuse(Vector3f color);
    //镜面反射
    void SetSpecular(Vector3f color);
    //光源位置
    void SetLightPos(Vector3f pos);
    //光强衰减因子
    void SetAttenuationFactor(double c0,double c1,double c2);
    //设置光源开关
    void SetOnOff(bool on);

    bool GetOnOff();
    Vector3f GetLightPos();

    Vector3f GetDiffuse();
    Vector3f GetSpecular();
    double GetC0();
    double GetC1();
    double GetC2();
private:
    Vector3f diffuse;
    Vector3f specular;
    Vector3f postion;
    double c0;
    double c1;
    double c2;
    bool onOff;
};