#pragma once

#include <Eigen/Eigen>
#include <algorithm>
#include "ColorVec.h"
#include <vector>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

class Polygon{
    private:
        std::vector<Vector2f> vertexs;
    public:
        Polygon();
        Polygon(std::vector<Vector2f>& verts);
    public:
        std::vector<Vector2f>& GetVerts(){
            return vertexs;
        }
};