#pragma once

#include<Eigen/Eigen>
#include<algorithm>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class Util{
    public:
        static constexpr double PI = 3.14159265359;
    public:
        static int Det(const Vector3f& a,const Vector3f& b,const Vector3f& c){
            auto vab = b - a;
            auto vac = c - a;
            return (int)vab.cross(vac).z();
        }

        static double Det2(const Vector2f& a,const Vector2f& b,const Vector2f& c){
            auto vab = b - a;
            auto vac = c - a;
            // 计算两个向量的行列式
            double determinant = vab(0) * vac(1) - vab(1) * vac(0);
            return determinant;
        }

        static double sign(const Vector2f& p1, const Vector2f& p2, const Vector2f& p3) 
        {
            return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
        }

        static bool isPointInTriangle(const Vector2f& pt, const Vector2f& pt1, const Vector2f& pt2,const Vector2f& pt3) 
        {
            float d1, d2, d3;
            bool hasNeg, hasPos;

            d1 = sign(pt, pt1, pt2);
            d2 = sign(pt, pt2, pt3);
            d3 = sign(pt, pt3, pt1);

            hasNeg = (d1 < 0) || (d2 < 0) || (d3 < 0);
            hasPos = (d1 > 0) || (d2 > 0) || (d3 > 0);

            // 如果所有叉积的符号相同，点在三角形内部
            return !(hasNeg && hasPos);
        }

        static Vector3f colorInterpret(float mid,float st,float ed,Vector3f sc,Vector3f ec){
            Vector3f color;
            color = (ed - mid) / (ed - st) * sc + (mid - st) / (ed - st) * ec;
            return color;
        }

        static int getRand(int min, int max) {
            return ( rand() % (max - min + 1) ) + min ;
        }

        // Get first token of string
        static std::string firstToken(const std::string &in)
        {
            if (!in.empty())
            {
                size_t token_start = in.find_first_not_of(" \t");
                size_t token_end = in.find_first_of(" \t", token_start);
                if (token_start != std::string::npos && token_end != std::string::npos)
                {
                    return in.substr(token_start, token_end - token_start);
                }
                else if (token_start != std::string::npos)
                {
                    return in.substr(token_start);
                }
            }
            return "";
        }

        // Get tail of string after first token and possibly following spaces
        static std::string tail(const std::string &in)
        {
            size_t token_start = in.find_first_not_of(" \t");
            size_t space_start = in.find_first_of(" \t", token_start);
            size_t tail_start = in.find_first_not_of(" \t", space_start);
            size_t tail_end = in.find_last_not_of(" \t");
            if (tail_start != std::string::npos && tail_end != std::string::npos)
            {
                return in.substr(tail_start, tail_end - tail_start + 1);
            }
            else if (tail_start != std::string::npos)
            {
                return in.substr(tail_start);
            }
            return "";
        }

        // Split a String into a string array at a given token
        static void split(const std::string &in,
                          std::vector<std::string> &out,
                          std::string token)
        {
            out.clear();

            std::string temp;

            for (int i = 0; i < int(in.size()); i++)
            {
                std::string test = in.substr(i, token.size());

                if (test == token)
                {
                    if (!temp.empty())
                    {
                        out.push_back(temp);
                        temp.clear();
                        i += (int)token.size() - 1;
                    }
                    else
                    {
                        out.push_back("");
                    }
                }
                else if (i + token.size() >= in.size())
                {
                    temp += in.substr(i, token.size());
                    out.push_back(temp);
                    break;
                }
                else
                {
                    temp += in[i];
                }
            }
        }

        // Get element at given index position
        template <class T>
        static const T & getElement(const std::vector<T> &elements, std::string &index)
        {
            int idx = std::stoi(index);
            if (idx < 0)
                idx = int(elements.size()) + idx;
            else
                idx--;
            return elements[idx];
        }

        // Angle between 2 Vector3 Objects
        static float AngleBetweenV3(const Vector3f a, const Vector3f b)
        {
            float angle = a.dot(b);
            angle /= (a.norm() * a.norm());
            return angle = acosf(angle);
        }

        // Check to see if a Vector3 Point is within a 3 Vector3 Triangle
        static bool inTriangle(Vector3f point, Vector3f tri1, Vector3f tri2, Vector3f tri3)
        {
            // Test to see if it is within an infinite prism that the triangle outlines.
            bool within_tri_prisim = SameSide(point, tri1, tri2, tri3) && SameSide(point, tri2, tri1, tri3)
                                     && SameSide(point, tri3, tri1, tri2);

            // If it isn't it will never be on the triangle
            if (!within_tri_prisim)
                return false;

            // Calulate Triangle's Normal
            Vector3f n = GenTriNormal(tri1, tri2, tri3);

            // Project the point onto this normal
            Vector3f proj = ProjV3(point, n);

            // If the distance from the triangle to the point is 0
            //	it lies on the triangle
            if (proj.norm() == 0)
                return true;
            else
                return false;
        }

        // A test to see if P1 is on the same side as P2 of a line segment ab
        static bool SameSide(Vector3f p1, Vector3f p2, Vector3f a, Vector3f b)
        {
            Vector3f cp1 = (b - a).cross(p1 - a);
            Vector3f cp2 = (b - a).cross(p2 - a);

            if (cp1.dot(cp2) >= 0)
                return true;
            else
                return false;
        }

        // Generate a cross produect normal for a triangle
        static Vector3f GenTriNormal(Vector3f t1, Vector3f t2, Vector3f t3)
        {
            Vector3f u = t2 - t1;
            Vector3f v = t3 - t1;

            Vector3f normal = u.cross(v);

            return normal;
        }

        // Projection Calculation of a onto b
        static Vector3f ProjV3(const Vector3f a, const Vector3f b)
        {
            Vector3f bn = b / b.norm();
            return bn * (a.dot(bn));
        }

        static bool insideTriangle(int x, int y, const Vector4f *_v) 
        {
            Vector3f v[3];
            for (int i = 0; i < 3; i++)
                v[i] = {_v[i].x(), _v[i].y(), 1.0};
            Vector3f f0, f1, f2;
            f0 = v[1].cross(v[0]);
            f1 = v[2].cross(v[1]);
            f2 = v[0].cross(v[2]);
            Vector3f p(x, y, 1.);
            if ((p.dot(f0) * f0.dot(v[2]) > 0) && (p.dot(f1) * f1.dot(v[0]) > 0) && (p.dot(f2) * f2.dot(v[1]) > 0))
                return true;
            return false;
        }

        static std::tuple<float, float, float> computeBarycentric2D(float x, float y, const Vector4f *v) 
        {
            float c1 = (x * (v[1].y() - v[2].y()) + (v[2].x() - v[1].x()) * y + v[1].x() * v[2].y() - v[2].x() * v[1].y()) /
                    (v[0].x() * (v[1].y() - v[2].y()) + (v[2].x() - v[1].x()) * v[0].y() + v[1].x() * v[2].y() -
                        v[2].x() * v[1].y());
            float c2 = (x * (v[2].y() - v[0].y()) + (v[0].x() - v[2].x()) * y + v[2].x() * v[0].y() - v[0].x() * v[2].y()) /
                    (v[1].x() * (v[2].y() - v[0].y()) + (v[0].x() - v[2].x()) * v[1].y() + v[2].x() * v[0].y() -
                        v[0].x() * v[2].y());
            float c3 = (x * (v[0].y() - v[1].y()) + (v[1].x() - v[0].x()) * y + v[0].x() * v[1].y() - v[1].x() * v[0].y()) /
                    (v[2].x() * (v[0].y() - v[1].y()) + (v[1].x() - v[0].x()) * v[2].y() + v[0].x() * v[1].y() -
                        v[1].x() * v[0].y());
            return {c1, c2, c3};
        }

        static Eigen::Vector3f interpolate(float alpha, float beta, float gamma, const Eigen::Vector3f &vert1, const Eigen::Vector3f &vert2,
            const Eigen::Vector3f &vert3, float weight) 
        {
            return (alpha * vert1 + beta * vert2 + gamma * vert3) / weight;
        }

        static Vector2f interpolate(float alpha, float beta, float gamma, const Vector2f &vert1, const Vector2f &vert2,
            const Vector2f &vert3, float weight) 
        {
            auto u = (alpha * vert1[0] + beta * vert2[0] + gamma * vert3[0]);
            auto v = (alpha * vert1[1] + beta * vert2[1] + gamma * vert3[1]);

            u /= weight;
            v /= weight;

            return Vector2f(u, v);
        }

        static auto toVec4(const Eigen::Vector3f &v3, float w = 1.0f) 
        {
            return Vector4f(v3.x(), v3.y(), v3.z(), w);
        }
};