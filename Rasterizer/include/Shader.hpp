#pragma once

#include <Eigen/Eigen>
#include "Texture.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

struct FragmentShader
{
    FragmentShader()
    {
        texture = nullptr;
    }

    FragmentShader(Vector3f& col, Vector3f& nor,Vector2f& tc, Texture* tex) :
         color(col), normal(nor), texCoord(tc), texture(tex) {}

    Vector3f viewPos;
    Vector3f color;
    Vector3f normal;
    Vector2f texCoord;
    Texture* texture;
};

struct VertexShader
{
    Vector3f position;
};