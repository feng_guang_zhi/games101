#pragma once

#include "AppContext.hpp"
#include "Face4.hpp"
#include "Transform3.hpp"
#include "Lighting.hpp"
#include "ColorVec.h"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class Cube2
{
public:
    Cube2();
    virtual ~Cube2();
    void ReadVertex();
    void ReadFaces();
    void OnDraw(AppContext* context);
    void SetLight(Lighting* light);
private:
    std::vector<Vector3f> verts;
    std::vector<Face4> faces;
    Lighting* pLighting;
    Transform3 transform;
public:
    Transform3 getTransform(){
        return transform;
    }

    std::vector<Vector3f> getVerts() const
    {
        return verts;
    }

    std::vector<Face4> getFaces() const
    {
        return faces;
    }
};