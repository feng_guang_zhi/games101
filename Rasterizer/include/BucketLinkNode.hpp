#pragma once

#include "EdgeLinkNode.hpp"

//桶表
class BucketLinkNode
{
public:
	BucketLinkNode(){};
	virtual ~BucketLinkNode(){};
	int ScanLine;
	EdgeLinkNode *pEdgeLink;
	BucketLinkNode *next;
};