#pragma once

#include "AppContext.hpp"
#include "Transform3.hpp"
#include "Projection.hpp"
#include "Lighting.hpp"
#include "BezierMesh.hpp"
#include "ColorVec.h"
#include <array>
#include <cmath>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

//双三次Bezier曲面
class BezierSphereMesh : public BezierMesh
{
public:
    BezierSphereMesh();
    virtual ~BezierSphereMesh();
    virtual void ReadCtrlPts() override;
    void DrawCurvedPatch(AppContext* context,int recursion);
    void Draw(AppContext* context);
    void SetLight(Lighting* light);
    //细分曲面
    void Tessellate(AppContext* context,int recursion,Vector2f* uvs);
    //四边形网格
    void MeshGrid(AppContext* context,Vector2f* uvs);
private:
    //网格点坐标
    std::array<VertexData,4> gridPts;
    //网格点法向量
    std::array<Vector3f,4> gridNormals;
    Projection projection;
    Lighting* pLight;
};