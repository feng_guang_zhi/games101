#pragma once

#include <Eigen/Eigen>
#include <algorithm>
#include "ColorVec.h"
#include <vector>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

class StackNode  
{
public:
	StackNode(){};
	virtual ~StackNode(){};
	Vector2f pixelPoint;
	StackNode *next;
    static void push(StackNode* headNode,Vector2f pt);
    static StackNode* pop(StackNode* headNode);
};