#ifndef _AppContext_H
#define _AppContext_H

#include <iostream>
#include <cmath>
#define _USE_MATH_DEFINES
#include "Rasterizer.hpp"
#include "LineRenderer.hpp"
#include "TriangleRenderer.hpp"
#include "PolygonRenderer.hpp"
#include "Projection.hpp"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

enum class ProjectionType{
    Orthogonal = 1,
    Perspective = 2
};

class AppContext
{
    private:
        Rasterizer* rasterizer;
        LineRenderer* lineRenderer;
        TriangleRenderer* triangleRender;
        PolygonRenderer* polygonRenderer;
        Projection* projection;
    public:
        AppContext();
        virtual ~AppContext();

        Rasterizer* getRasterizer();
        LineRenderer* getLineRenderer();
        TriangleRenderer* getTriangleRender();
        PolygonRenderer* getPolygonRenderer();
        Projection* getProjection();
        
};

#endif