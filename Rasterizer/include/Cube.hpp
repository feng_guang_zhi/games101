#ifndef _Cube_H
#define _Cube_H

#include "AppContext.hpp"
#include "Face4.hpp"
#include "Transform3.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class Cube
{
    public:
        Cube(){};
        virtual ~Cube(){};

        void OnDraw(AppContext* context);
        void DrawAxis(AppContext* context,const Vector3f& start,const Vector3f& end);
        void ReadVertex();
        void ReadVertColor();
        void ReadFaces();
    private:
        std::vector<Vector3f> pts;
        std::vector<Face4> faces;
        std::vector<VertexData> verts;
        Transform3 transform;
    public:
        Transform3 getTransform(){
            return transform;
        }

        std::vector<Vector3f> getVerts() const
        {
            return pts;
        }

        std::vector<VertexData> getVertData() const
        {
            return verts;
        }

        std::vector<Face4> getFaces() const
        {
            return faces;
        }

        void DrawCube(AppContext* context,const ProjectionType projectionType);
        void DrawCubeCull(AppContext* context);
        void DrawCubeFill(AppContext* context);
};

#endif