#pragma once

#include<Eigen/Eigen>
#include<algorithm>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

class ColorVec3{
public:
    Vector3f pt;
    Vector3f color;

    ColorVec3(){
        pt = Vector3f(0,0,0);
        color = Vector3f(0,0,0);
    }

    ColorVec3(Vector3f ver){
        pt = ver;
        color = Vector3f(0,0,0);
    }

    ColorVec3(Vector3f ver,Vector3f c){
        pt = ver;
        color = c;
    }
};

struct VertexInfo{
    ColorVec3 cvertexs;
    Vector2f texCoord;
    Vector3f normal;
};