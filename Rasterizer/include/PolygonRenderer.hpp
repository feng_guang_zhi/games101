#pragma once

#include <Eigen/Eigen>
#include "Rasterizer.hpp"
#include "Polygon.hpp"
#include "LineRenderer.hpp"
#include "BucketLinkNode.hpp"
#include "EdgeLinkNode.hpp"
#include "StackNode.hpp"
#include "Util.hpp"
#include "ColorVec.h"
#include <cmath>
#include <algorithm>
#include <iostream>

using Vector3f = Eigen::Vector3f;

class PolygonRenderer{
    private:
        Rasterizer* rasterizer;
        LineRenderer* lineRender;
    protected:
        BucketLinkNode *headBucket,*curBucket;
        std::vector<EdgeLinkNode> edges;
        EdgeLinkNode *headEdge,*curEdge;
        //临时计算用
        EdgeLinkNode *edgeLinkList1,*edgeLinkList2;
    public:
        PolygonRenderer(Rasterizer* rast){
            rasterizer = rast;
        }

        PolygonRenderer(Rasterizer* rast,LineRenderer* lineRenderer){
            rasterizer = rast;
            lineRender = lineRenderer;
        }

        void FramePolygon(Polygon& polygon,Vector3f color);
        //创建桶表
        void CreateBucket(Polygon& polygon);
        //创建边表
        void CreateEdgeTable(Polygon& polygon);
        //有效边表填充算法
        void AddEdge(EdgeLinkNode* edgeNode);
        void EdgeSort();
        void AETPolygonFill(Vector3f color);
        void AETPolygonFill(Polygon& polygon,Vector3f color);
        //边缘填充算法
        void FillWithBound(Polygon& polygon,Vector3f color,int maxX);
        //种子填充算法
        void StackSeedFill(Polygon& polygon,Vector3f color,Vector2f startPt);

        void BoundaryFill4(int startx,int starty,const Vector3f& boundColor,const Vector3f& seedColor);
        void BoundaryFill8(int startx,int starty,const Vector3f& boundColor,const Vector3f& seedColor);
};