#pragma once

#include <Eigen/Eigen>
#include "Texture.hpp"
#include "Shader.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;
using Matrix4f = Eigen::Matrix4f;
using Matrix3f = Eigen::Matrix3f;

const double MY_PI = 3.14159265358;

static Matrix4f getViewMatrix(Vector3f eye_pos) 
{
    Matrix4f view = Matrix4f::Identity();

    Matrix4f translate;
    translate << 1, 0, 0, -eye_pos[0],
                0, 1, 0, -eye_pos[1],
                0, 0, 1, -eye_pos[2],
                0, 0, 0, 1;

    view = translate * view;

    return view;
}

static Matrix4f getModelMatrix(float angle) 
{
    Matrix4f rotation;
    angle = angle * MY_PI / 180.f;
    rotation << cos(angle), 0, sin(angle), 0,
            0, 1, 0, 0,
            -sin(angle), 0, cos(angle), 0,
            0, 0, 0, 1;

    Matrix4f scale;
    scale << 2.5, 0, 0, 0,
            0, 2.5, 0, 0,
            0, 0, 2.5, 0,
            0, 0, 0, 1;

    Matrix4f translate;
    translate << 1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1;

    return translate * rotation * scale;
}

static Matrix4f getProjectionMatrix(float eye_fov, float aspect_ratio, float zNear, float zFar) {
    // TODO: Use the same projection matrix from the previous assignments
    Matrix4f projection = Matrix4f::Identity();
    Matrix4f m;
    m << -zNear, 0, 0, 0,
            0, -zNear, 0, 0,
            0, 0, -zNear - zFar, -zNear * zFar,
            0, 0, 1, 0;

    float halve = (eye_fov / 2) * MY_PI / 180;
    float top = tan(halve) * zNear;
    float bottom = -top;
    float right = top * aspect_ratio;
    float left = -right;
    Matrix4f n, p;
    n << 2 / (right - left), 0, 0, 0,
            0, 2 / (top - bottom), 0, 0,
            0, 0, 2 / (zFar - zNear), 0,
            0, 0, 0, 1;

    p << 1, 0, 0, -(right + left) / 2,
            0, 1, 0, -(top + bottom) / 2,
            0, 0, 1, -(-zFar + -zNear) / 2,
            0, 0, 0, 1;

    projection = n * p * m;

    return projection;
}

// 获取顶点位置
static Vector3f vertexShader(const VertexShader &vertexShader) {
    return vertexShader.position;
}

// 可视化法线信息
static Vector3f normalFragmentShader(const FragmentShader &frag) {
    Vector3f return_color = (frag.normal.head<3>().normalized() + Vector3f(1.0f, 1.0f, 1.0f)) / 2.f;
    Vector3f result;
    result << return_color.x() * 255, return_color.y() * 255, return_color.z() * 255;
    return result;
}

static Vector3f reflect(const Vector3f &vec, const Vector3f &axis) {
    auto costheta = vec.dot(axis);
    return (2 * costheta * axis - vec).normalized();
}

struct Light {
    Vector3f position;
    Vector3f intensity;
};

// 可视化纹理信息
inline Vector3f textureFragmentShader(const FragmentShader &frag) {
    Vector3f return_color = {0, 0, 0};
    if (frag.texture)
    {
        // TODO: Get the texture value at the texture coordinates of the current fragment
        return_color = frag.texture->getColor(frag.texCoord.x(), frag.texCoord.y()); //关键部分
        return_color /= 255.0f;
    }
    Vector3f texture_color;
    texture_color << return_color.x(), return_color.y(), return_color.z();

    Vector3f ka = Vector3f(0.005, 0.005, 0.005);
    Vector3f kd = texture_color;
    Vector3f ks = Vector3f(0.7937, 0.7937, 0.7937);

    auto l1 = Light{{20, 20, 20}, {500, 500, 500}};
    auto l2 = Light{{-20, 20, 0}, {500, 500, 500}};

    std::vector<Light> lights = {l1, l2};
    Vector3f amb_light_intensity{10, 10, 10};
    Vector3f eye_pos{0, 0, 0};

    float p = 150;

    Vector3f color = texture_color;
    Vector3f point = frag.viewPos;
    Vector3f normal = frag.normal;

    Vector3f result_color = {0, 0, 0};

    for (auto& light : lights)
    {
        // TODO: For each light source in the code, calculate what the *ambient*, *diffuse*, and *specular*
        // components are. Then, accumulate that result on the *result_color* object.
        auto v = eye_pos - point; //v为出射光方向（指向眼睛）
        auto l = light.position - point; //l为指向入射光源方向
        auto h = (v.normalized() + l.normalized()).normalized(); //h为半程向量即v+l归一化后的单位向量
        auto r = l.dot(l); //衰减因子
        auto ambient = ka.cwiseProduct(amb_light_intensity);
        auto diffuse = kd.cwiseProduct(light.intensity / r) * std::max(0.0f, normal.normalized().dot(l.normalized()));
        auto specular = ks.cwiseProduct(light.intensity / r) * std::pow(std::max(0.0f, normal.normalized().dot(h)), p);
        result_color += (ambient + diffuse + specular);
    }

    return result_color;
}

// Blinn-Phong着色模型
static Vector3f phongFragmentShader(const FragmentShader &frag) 
{
    Vector3f ka = Vector3f(0.005, 0.005, 0.005);
    Vector3f kd = frag.color;
    Vector3f ks = Vector3f(0.7937, 0.7937, 0.7937);

    auto l1 = Light{{20,  20,  20},
                    {500, 500, 500}};
    auto l2 = Light{{-20, 20,  0},
                    {500, 500, 500}};

    std::vector<Light> lights = {l1, l2};
    Vector3f amb_light_intensity{10, 10, 10};
    Vector3f eye_pos{0, 0, 0};

    float p = 150;

    Vector3f color = frag.color;
    Vector3f point = frag.viewPos;
    Vector3f normal = frag.normal;

    Vector3f result_color = {0, 0, 0};
    for (auto &light: lights) {
        // TODO: For each light source in the code, calculate what the *ambient*, *diffuse*, and *specular* 
        // components are. Then, accumulate that result on the *result_color* object.
        auto v = eye_pos - point; // v从着色点指向眼睛，并不是出射光
        auto l = light.position - point; // l为指向入射光源方向
        auto h = (v.normalized() + l.normalized()).normalized(); // h 为半程向量即 v + l 归一化后的单位向量
        auto r_2 = l.dot(l); //光源到 shading point的距离的平方，用于计算光能量的的损失

        // a.cwiseProduct(b)将返回一个新的向量，其中第 i个元素等于 a中第 i个元素与 b中第 i个元素的乘积。

        // 环境光照，可以看清物体的形状，但没有立体感
        auto ambient = ka.cwiseProduct(amb_light_intensity);

        // 漫反射，具有一定的立体感
        auto diffuse = kd.cwiseProduct(light.intensity / r_2) * std::max(0.0f, normal.normalized().dot(l.normalized()));

        // 高光，更加贴近现实
        auto specular = ks.cwiseProduct(light.intensity / r_2) * std::pow(std::max(0.0f, normal.normalized().dot(h)), p);

        result_color += (ambient + diffuse + specular);
    }

    return result_color;
}

// 可视化置换信息
static Vector3f displacementFragmentShader(const FragmentShader& frag)
{

    Vector3f ka = Vector3f(0.005, 0.005, 0.005);
    Vector3f kd = frag.color;
    Vector3f ks = Vector3f(0.7937, 0.7937, 0.7937);

    auto l1 = Light{{20, 20, 20}, {500, 500, 500}};
    auto l2 = Light{{-20, 20, 0}, {500, 500, 500}};

    std::vector<Light> lights = {l1, l2};
    Vector3f amb_light_intensity{10, 10, 10};
    Vector3f eye_pos{0, 0, 0};

    float p = 150;

    Vector3f color = frag.color;
    Vector3f point = frag.viewPos;
    Vector3f normal = frag.normal;

    float kh = 0.2, kn = 0.1;

    // TODO: Implement displacement mapping here
    // Let n = normal = (x, y, z)
    // Vector t = (x*y/sqrt(x*x+z*z),sqrt(x*x+z*z),z*y/sqrt(x*x+z*z))
    // Vector b = n cross product t
    // Matrix TBN = [t b n]
    // dU = kh * kn * (h(u+1/w,v)-h(u,v))
    // dV = kh * kn * (h(u,v+1/h)-h(u,v))
    // Vector ln = (-dU, -dV, 1)
    // Position p = p + kn * n * h(u,v)
    // Normal n = normalize(TBN * ln)

    float x = normal.x();
    float y = normal.y();
    float z = normal.z();

    Vector3f t = Vector3f(x * y / std::sqrt(x * x + z * z), std::sqrt(x * x + z * z), z * y / std::sqrt(x * x + z * z));
    Vector3f b = normal.cross(t);

    Matrix3f TBN;
    TBN <<
        t.x(), b.x(), normal.x(),
            t.y(), b.y(), normal.y(),
            t.z(), b.z(), normal.z();

    float u = frag.texCoord.x();
    float v = frag.texCoord.y();
    float w = frag.texture->width;
    float h = frag.texture->height;

    float dU = kh * kn * (frag.texture->getColor(u + 1.0f / w, v).norm() - frag.texture->getColor(u, v).norm());
    float dV = kh * kn * (frag.texture->getColor(u, v + 1.0f / h).norm() - frag.texture->getColor(u, v).norm());

    Vector3f ln = Vector3f(-dU, -dV, 1.0f);

    point += (kn * normal * frag.texture->getColor(u, v).norm());

    normal = (TBN * ln).normalized();

    Vector3f result_color = {0, 0, 0};

    for (auto& light : lights)
    {
        // TODO: For each light source in the code, calculate what the *ambient*, *diffuse*, and *specular*
        // components are. Then, accumulate that result on the *result_color* object.

        Vector3f light_dir = (light.position - point).normalized();
        Vector3f view_dir = (eye_pos - point).normalized();
        Vector3f half_vector = (light_dir + view_dir).normalized();

        // 距离衰减
        float r2 = (light.position - point).dot(light.position - point);

        //环境光
        //cwiseProduct()：矩阵点对点相乘
        Vector3f La = ka.cwiseProduct(amb_light_intensity);

        //漫反射
        Vector3f Ld = kd.cwiseProduct(light.intensity / r2);
        Ld *= std::max(0.0f, normal.normalized().dot(light_dir));

        //高光
        Vector3f Ls = ks.cwiseProduct(light.intensity / r2);
        Ls *= std::pow(std::max(0.0f, normal.normalized().dot(half_vector)), p);

        result_color += (La + Ld + Ls);
    }

    return result_color;
}

// 可视化凹凸信息 (凹凸贴图)
static Eigen::Vector3f bumpFragmentShader(const FragmentShader& frag)
{

    Vector3f ka = Vector3f(0.005, 0.005, 0.005);
    Vector3f kd = frag.color;
    Vector3f ks = Vector3f(0.7937, 0.7937, 0.7937);

    auto l1 = Light{{20, 20, 20}, {500, 500, 500}};
    auto l2 = Light{{-20, 20, 0}, {500, 500, 500}};

    std::vector<Light> lights = {l1, l2};
    Vector3f amb_light_intensity{10, 10, 10};
    Vector3f eye_pos{0, 0, 0};

    float p = 150;

    Vector3f color = frag.color;
    Vector3f point = frag.viewPos;
    Vector3f normal = frag.normal;


    float kh = 0.2, kn = 0.1;

    // TODO: Implement bump mapping here
    // Let n = normal = (x, y, z)
    // Vector t = (x*y/sqrt(x*x+z*z),sqrt(x*x+z*z),z*y/sqrt(x*x+z*z))
    // Vector b = n cross product t
    // Matrix TBN = [t b n]
    // dU = kh * kn * (h(u+1/w,v)-h(u,v))
    // dV = kh * kn * (h(u,v+1/h)-h(u,v))
    // Vector ln = (-dU, -dV, 1)
    // Normal n = normalize(TBN * ln)

    float x = normal.x();
    float y = normal.y();
    float z = normal.z();

    Vector3f t = Vector3f(x * y / std::sqrt(x * x + z * z), std::sqrt(x * x + z * z), z * y / std::sqrt(x * x + z * z));
    Vector3f b = normal.cross(t);

    Matrix3f TBN;
    TBN <<  t.x(), b.x(), normal.x(),
            t.y(), b.y(), normal.y(),
            t.z(), b.z(), normal.z();

    float u = frag.texCoord.x();
    float v = frag.texCoord.y();
    float w = frag.texture->width;
    float h = frag.texture->height;

    float dU = kh * kn * (frag.texture->getColor(u + 1.0f / w, v).norm() - frag.texture->getColor(u, v).norm());
    float dV = kh * kn * (frag.texture->getColor(u, v + 1.0f / h).norm() - frag.texture->getColor(u, v).norm());

    Vector3f ln = Vector3f(-dU, -dV, 1.0f);
    normal = TBN * ln;

    Vector3f result_color = {0, 0, 0};
    result_color = normal.normalized();

    return result_color;
}