#pragma once

#include<Eigen/Eigen>
#include<algorithm>
#include"ColorVec.h"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

class Triangle{
    private:
        VertexInfo vertexs[3];
    public:
        Triangle();
        Triangle(Vector3f a,Vector3f b,Vector3f c);
        Triangle(Vector2f a,Vector2f b,Vector2f c);
        Triangle(Vector3f a,Vector3f b,Vector3f c,Vector3f ca,Vector3f cb,Vector3f cc);
        Triangle(Vector2f a,Vector2f b,Vector2f c,Vector3f ca,Vector3f cb,Vector3f cc);
    
    public:
        Vector3f getA() const{
            return vertexs[0].cvertexs.pt;
        }

        Vector3f getB() const{
            return vertexs[1].cvertexs.pt;
        }

        Vector3f getC() const{
            return vertexs[2].cvertexs.pt;
        }

        ColorVec3 getCA() const{
            return vertexs[0].cvertexs;
        }

        ColorVec3 getCB() const{
            return vertexs[1].cvertexs;
        }

        ColorVec3 getCC() const{
            return vertexs[2].cvertexs;
        }

        Vector3f getAColor() const{
            return vertexs[0].cvertexs.color;
        }

        Vector3f getBColor() const{
            return vertexs[1].cvertexs.color;
        }

        Vector3f getCColor() const{
            return vertexs[2].cvertexs.color;
        }

    public:
        void setVertex(int vindex,Vector3f ver);
        void setNormal(int nindex,Vector3f normal);
        void setColor(int cindex,Vector3f color);
        void setColor(int cindex,float r,float g,float b);
        void setTexCoord(int tindex,Vector2f uv);
        void setTexCoord(int tindex,float u,float v);

        void sortY();

        std::array<Vector4f,3> toVec4()const;
};