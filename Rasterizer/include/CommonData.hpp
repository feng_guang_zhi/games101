#pragma once

#include <Eigen/Eigen>
#include <algorithm>

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;

struct VertexData
{
    Vector3f pt;
    Vector3f color;
    Vector2f texCoord;
    Vector3f normal;
};

struct MaterialData
{
    MaterialData()
    {
        name;
        specualrExp = 0.0f;
        density = 0.0f;
        dissolve = 0.0f;
        illum = 0;
    }

    // Material Name
    std::string name;
    // Ambient Color
    Vector3f kambient;
    // Diffuse Color
    Vector3f kdiffuse;
    // Specular Color
    Vector3f kspecular;
    // Specular Exponent
    float specualrExp;
    // Optical Density
    float density;
    // Dissolve
    float dissolve;
    // Illumination
    int illum;
    // Ambient Texture Map
    std::string mapAmbient;
    // Diffuse Texture Map
    std::string mapDiffuse;
    // Specular Texture Map
    std::string mapSpecular;
    // Specular Hightlight Map
    std::string mapHightlight;
    // Alpha Texture Map
    std::string mapAlpha;
    // Bump Map
    std::string mapBump;
};

struct MeshData
{
    // Default Constructor
    MeshData(){}
    // Variable Set Constructor
    MeshData(std::vector<VertexData>& vertices, std::vector<int>& indices)
    {
        Vertices = vertices;
        Indices = indices;
    }
    // Mesh Name
    std::string MeshName;
    // Vertex List
    std::vector<VertexData> Vertices;
    // Index List
    std::vector<int> Indices;
    // Material
    MaterialData MeshMaterial;
};