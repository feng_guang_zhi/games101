#pragma once

#include<Eigen/Eigen>
#include<iostream>

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;

class Transform2
{
    public:
        Transform2(){};
        virtual ~Transform2(){};
    private:
        Eigen::Matrix3f mat;
        std::vector<Vector2f>* pts;
    public:
        void SetVerts(std::vector<Vector2f>* pts);
        void Identity(){mat.Identity();};
        void Translate(double tx,double ty);
        void Scale(double sx,double sy);
        void Scale(double sx,double sy,Vector2f pt);
        void Rotate(double beta);
        void Rotate(double beta,Vector2f pt);
        void ReflectOrg();
        void ReflectX();
        void ReflectY();
        void Shear(double b,double c);
        void MultiplyMatrix();
};