#pragma once

#include "CmdFactory.hpp"
#include "Transform2.hpp"
#include "Cube.hpp"
#include "CubicBezierCurve.hpp"
#include "CubicBezierPatch.hpp"
#include "RationalBezierCurve.hpp"
#include "BezierSphere.hpp"
#include "Sphere.hpp"
#include "Lighting.hpp"
#include "BezierSphereMesh.hpp"
#include "Sphere2.hpp"
#include "Cube2.hpp"

class DrawLineCmd : public ICommand
{
    public: 
        DrawLineCmd(){};
        DrawLineCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            appContext->getLineRenderer()->Fllower(Vector3f(SCREEN_HEIGHT / 2 - 100,SCREEN_WIDTH / 2,0),260);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class DrawCircleCmd : public ICommand
{
    public: 
        DrawCircleCmd(){};
        DrawCircleCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            appContext->getLineRenderer()->FlowCircle(Vector3f(SCREEN_HEIGHT / 2,SCREEN_WIDTH / 2,0),Vector3f(1.0f,0.0f,0.0f));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class DrawEllipseCmd : public ICommand
{
    public: 
        DrawEllipseCmd(){};
        DrawEllipseCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            appContext->getLineRenderer()->MidPointEllipse(Vector3f(SCREEN_HEIGHT / 2,SCREEN_WIDTH / 2,0),150,300,Vector3f(1.0f,0.0f,0.0f));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class PolygonFrameCmd : public ICommand
{
    public: 
        PolygonFrameCmd(){};
        PolygonFrameCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            std::vector<Vector2f> pts;
            pts.push_back(Vector2f(450,310));
            pts.push_back(Vector2f(250,350));
            pts.push_back(Vector2f(130,240));
            pts.push_back(Vector2f(250,30));
            pts.push_back(Vector2f(390,120));
            pts.push_back(Vector2f(500,30));
            pts.push_back(Vector2f(505,350));
            Polygon polygon(pts);
            appContext->getPolygonRenderer()->FramePolygon(polygon,Vector3f(1,0,0));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class PolygonFillAETCmd : public ICommand
{
    public: 
        PolygonFillAETCmd(){};
        PolygonFillAETCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            std::vector<Vector2f> pts;
            pts.push_back(Vector2f(200,250));
            pts.push_back(Vector2f(200,350));
            pts.push_back(Vector2f(300,350));
            pts.push_back(Vector2f(300,250));
            Polygon polygon(pts);
            appContext->getPolygonRenderer()->AETPolygonFill(polygon,Vector3f(1,0,0));
        }
        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class PolygonFillBoundCmd : public ICommand
{
    public: 
        PolygonFillBoundCmd(){};
        PolygonFillBoundCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            std::vector<Vector2f> pts;
            pts.push_back(Vector2f(550,400));
            pts.push_back(Vector2f(350,600));
            pts.push_back(Vector2f(250,350));
            pts.push_back(Vector2f(350,50));
            pts.push_back(Vector2f(500,250));
            pts.push_back(Vector2f(600,50));
            pts.push_back(Vector2f(800,450));
            Polygon polygon(pts);
            appContext->getPolygonRenderer()->FillWithBound(polygon,Vector3f(1,0,0),SCREEN_HEIGHT);
        }
        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class PolygonFillSeedCmd : public ICommand
{
    public: 
        PolygonFillSeedCmd(){};
        PolygonFillSeedCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            std::vector<Vector2f> pts;
            pts.push_back(Vector2f(200,250));
            pts.push_back(Vector2f(200,350));
            pts.push_back(Vector2f(300,350));
            pts.push_back(Vector2f(300,250));
            Polygon polygon(pts);
            auto startPt = Vector2f(250,300);
            appContext->getPolygonRenderer()->FramePolygon(polygon,Vector3f(1,0,0));
            appContext->getPolygonRenderer()->BoundaryFill4(startPt.x(),startPt.y(),Vector3f(1,0,0),Vector3f(1,0,1));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class PolygonFillBound8Cmd : public ICommand
{
    public: 
        PolygonFillBound8Cmd(){};
        PolygonFillBound8Cmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            std::vector<Vector2f> pts;
            pts.push_back(Vector2f(200,250));
            pts.push_back(Vector2f(200,350));
            pts.push_back(Vector2f(300,350));
            pts.push_back(Vector2f(320,250));
            pts.push_back(Vector2f(270,100));
            Polygon polygon(pts);
            auto startPt = Vector2f(250,300);
            appContext->getPolygonRenderer()->FramePolygon(polygon,Vector3f(1,0,0));
            appContext->getPolygonRenderer()->BoundaryFill8(startPt.x(),startPt.y(),Vector3f(1,0,0),Vector3f(1,0,1));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class BezierCmd : public ICommand
{
    private:
        CubicBezierCurve bezier;
    public: 
        BezierCmd(){};
        BezierCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            bezier.ReadPoints();
            bezier.DrawCurveIterator(appContext);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};

        void DrawCircle()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            bezier.ClearPts();
            bezier.ReadCirclePts();
            bezier.DrawCurveIterator(appContext);
        }
};

class BezierPatchCmd : public ICommand
{
    private:
        CubicBezierPatch bezierPatch;
    public: 
        BezierPatchCmd(){};
        BezierPatchCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            bezierPatch.ReadCtrlPts();
            bezierPatch.ReadCoffients();
            bezierPatch.DrawCurvePatch(appContext);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class RationalBezierCmd : public ICommand
{
    private:
        RationalBezierCurve rationalBezier;
    public: 
        RationalBezierCmd(){};
        RationalBezierCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            rationalBezier.ReadCtrlPts();
            rationalBezier.DrawCirle(appContext);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class BezierSphereCmd : public ICommand
{
    private:
        BezierSphere sphere;
    public: 
        BezierSphereCmd(){};
        BezierSphereCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            sphere.readVertex();
            sphere.readFaces();
            sphere.getTransform().Scale(200,200,200);
            sphere.getTransform().Translate(200,200,0);
            sphere.draw(appContext);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class SphereFrameCmd : public ICommand
{
    private:
        Sphere sphere;
    public: 
        SphereFrameCmd(){};
        SphereFrameCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            sphere.SetRecursion(3);
            sphere.ReadVertex();
            sphere.ReadFaces();
            
            sphere.getTransform().Scale(100,100,100);
            sphere.getTransform().Translate(200,200,0);

            sphere.Draw(appContext);
        }
        virtual void Update()
        {
           
        }
        virtual void OnKeyDown(int key){};
};

class LightingCmd : public ICommand
{
    private:
        Lighting* lighting;
        int lightSrcNum = 2;
        Sphere2 sphere;
    public: 
        LightingCmd(){};
        virtual ~LightingCmd()
        {
            delete lighting;
        }
        LightingCmd(AppContext* context)
        {
            appContext = context;
            initLighting();
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            
            sphere.ReadVertex();
            sphere.ReadPatch();
            sphere.getTransform().Scale(200,200,200);
            sphere.getTransform().Translate(300,500,0);
            sphere.getMesh().SetLight(lighting);
            sphere.Draw(appContext);
        }
        virtual void Update()
        {
           
        }
        virtual void OnKeyDown(int key){};
    private:
        void initLighting()
        {
            lighting = new Lighting(lightSrcNum);
            lighting->getLightSource()[0].SetLightPos(Vector3f(1000,1000,1000));
            lighting->getLightSource()[1].SetLightPos(Vector3f(-1000,-1000,1000));
            for(int i = 0;i < lightSrcNum;i++)
            {
                lighting->getLightSource()[i].SetDiffuse(Vector3f(1,1,1));
                lighting->getLightSource()[i].SetSpecular(Vector3f(1,1,1));
                lighting->getLightSource()[i].SetAttenuationFactor(1,1e-7,1e-8);
                lighting->getLightSource()[i].SetOnOff(true);
            }
            lighting->getMaterial()->SetAmbient({0.175,0.012,0.012});
            lighting->getMaterial()->SetDiffuse({0.614,0.041,0.041});
            lighting->getMaterial()->SetSpecular({0.728,0.528,0.528});
            lighting->getMaterial()->SetExponent(30);
        }
};

class LightCubeCmd : public ICommand
{
    private:
        Lighting* lighting;
        int lightSrcNum = 1;
        Cube2 cube2;
        Vector3f center;
    public: 
        LightCubeCmd(){};
        virtual ~LightCubeCmd()
        {
            delete lighting;
        }
        LightCubeCmd(AppContext* context)
        {
            appContext = context;
            initLighting();
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            appContext->getProjection()->setEye(Vector3f(200,400,1000));
            cube2.ReadVertex();
            cube2.ReadFaces();
            cube2.SetLight(lighting);
            cube2.getTransform().Scale(200,200,200);
            cube2.getTransform().Translate(200,400,-100);
            
            cube2.OnDraw(appContext);
        }
        virtual void Update()
        {
           
        }
        virtual void OnKeyDown(int key)
        {
            // appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            // switch (key)
            // {
            //     case 'a':
            //         cube2.getTransform().Translate(0,-10,0);
            //         break;
            //     case 's':
            //         cube2.getTransform().Translate(10,0,0);
            //         break;
            //     case 'd':
            //         cube2.getTransform().Translate(0,10,0);
            //         break;
            //     case 'w':
            //         cube2.getTransform().Translate(-10,0,0);
            //         break;
            //     case 'q':
                    
            //         break;
            //     case 'e':
                   
            //         break;
            //     case 'z':
            //         cube2.getTransform().rotatePointAroundArbitraryAxis(center,Vector3f(0,1,0),50);
            //         break;
            //     case 'x':
            //        cube2.getTransform().rotatePointAroundArbitraryAxis(center,Vector3f(0,1,0),-50);
            //         break;
            //     default:
            //         break;
            // }
            // center = (cube2.getVerts()[4] + cube2.getVerts()[2]) / 2;
            // cube2.OnDraw(appContext);
        }
    private:
        void initLighting()
        {
            lighting = new Lighting(lightSrcNum);
            lighting->getLightSource()[0].SetLightPos(Vector3f(200,400,-1000));
            for(int i = 0;i < lightSrcNum;i++)
            {
                lighting->getLightSource()[i].SetDiffuse(Vector3f(1,1,1));
                lighting->getLightSource()[i].SetSpecular(Vector3f(1,1,1));
                lighting->getLightSource()[i].SetAttenuationFactor(0.65,2e-5,1e-6);
                lighting->getLightSource()[i].SetOnOff(true);
            }
            //0.247,0.2,0.075
            lighting->getMaterial()->SetAmbient({0,0,1});
            lighting->getMaterial()->SetDiffuse({0.752,0.606,0.226});
            lighting->getMaterial()->SetSpecular({0.628,0.556,0.336});
            lighting->getMaterial()->SetExponent(100);
        }
};