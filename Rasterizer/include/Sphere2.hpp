#pragma once

#include "BezierSphereMesh.hpp"
#include "Patch.hpp"
#include "AppContext.hpp"
#include "BezierSphereMesh.hpp"
#include "Transform3.hpp"

class Sphere2
{
public:
    Sphere2();
    virtual ~Sphere2();
    void ReadVertex();
    void ReadPatch();
    void Draw(AppContext* context);
    Transform3 getTransform()
    {
        return transform;
    }
    BezierSphereMesh getMesh()
    {
        return mesh;
    }
private:
    //球面控制数组
    std::vector<Vector3f> verts;
    std::vector<Patch2<4,4>> faces;
    BezierSphereMesh mesh;
    Transform3 transform;
};