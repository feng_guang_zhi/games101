#pragma once

#include "AppContext.hpp"
#include "BezierMesh.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class CubicBezierPatch : public BezierMesh
{
    public:
        CubicBezierPatch(){};
        virtual ~CubicBezierPatch(){};
        virtual void ReadCtrlPts() override;
        void DrawCurvePatch(AppContext* context);
        void ClearPts()
        {
            
        }  
};