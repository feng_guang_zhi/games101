#pragma once

#include "ICommand.hpp"
#include "Transform2.hpp"
#include "Cube.hpp"
#include "Pentagram.hpp"
#include "Diamond.hpp"

class TriangleFillCmd : public ICommand
{
    public: 
        TriangleFillCmd(){};
        TriangleFillCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            Triangle angle(Vector3f(Util::getRand(0,300),Util::getRand(0,300),0),Vector3f(Util::getRand(0,300),Util::getRand(0,300),0),Vector3f(Util::getRand(0,300),Util::getRand(0,300),0));
            appContext->getTriangleRender()->FillTriangle(angle,Vector3f(1,0,0));
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class TriangleLerpCmd : public ICommand
{
    public: 
        TriangleLerpCmd(){};
        TriangleLerpCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            Triangle angle(Vector3f(Util::getRand(0,300),Util::getRand(0,300),0),Vector3f(Util::getRand(0,300),Util::getRand(0,300),0),Vector3f(Util::getRand(0,300),Util::getRand(0,300),0),Vector3f(1,0,0),Vector3f(0,1,0),Vector3f(0,0,1));
            appContext->getTriangleRender()->FillLerp(angle);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class DiamondCmd : public ICommand
{
    private:
        Diamond diamond;
    public: 
        DiamondCmd(){};
        DiamondCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            int divideNum = 21;
            double radius = 250;
            auto center = Vector2f(300,300);
            diamond.SetParameter(divideNum,radius,center);
            diamond.ReadPoints();
            diamond.OnDraw(appContext);
        }
        virtual void Update(){}

        virtual void OnKeyDown(int key)
        {
            
        }
};

class Transform2Cmd : public ICommand
{
    private:
        Vector2f suspensionPt;
        Pentagram pentagram;
    public: 
        Transform2Cmd(){};
        Transform2Cmd(AppContext* context)
        {
            appContext = context;
            suspensionPt = Vector2f(0,10);
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            pentagram.ReadPoints();
            pentagram.OnDraw(appContext);
        }
        virtual void Update(){}

        virtual void OnKeyDown(int key)
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            switch (key)
            {
                case 'a':
                    pentagram.getTransform().Translate(0,-10);
                    break;
                case 's':
                    pentagram.getTransform().Translate(10,0);
                    break;
                case 'd':
                    pentagram.getTransform().Translate(0,10);
                    break;
                case 'w':
                    pentagram.getTransform().Translate(-10,0);
                    break;
                case 'q':
                    pentagram.getTransform().Rotate(-5,suspensionPt);
                    break;
                case 'e':
                    pentagram.getTransform().Rotate(5,suspensionPt);
                    break;
                default:
                    break;
            }
            
            pentagram.OnDraw(appContext);
        }
};

class Transform3Cmd : public ICommand
{
    private:
        Cube cube;
        Vector3f rotAxis;
        Vector3f rotAxisSt,rotAxisEd;
    public: 
        Transform3Cmd(){};
        Transform3Cmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            cube.ReadVertex();
            cube.ReadFaces();
            
            cube.getTransform().Scale(100,100,100);
            cube.getTransform().Translate(100,100,100);

            rotAxisEd = cube.getVerts()[4];
            rotAxisSt = cube.getVerts()[2];
            rotAxis = rotAxisEd - rotAxisSt;

            cube.OnDraw(appContext);
            cube.DrawAxis(appContext,rotAxisSt,rotAxisEd);
        }
        virtual void Update(){};

        virtual void OnKeyDown(int key)
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            rotAxisEd = cube.getVerts()[4];
            rotAxisSt = cube.getVerts()[2];
            rotAxis = rotAxisEd - rotAxisSt;
            switch (key)
            {
                case 'a':
                    cube.getTransform().rotatePointAroundArbitraryAxis(rotAxisSt,rotAxis,6);
                    break;
                case 'd':
                    cube.getTransform().rotatePointAroundArbitraryAxis(rotAxisSt,rotAxis,-6);
                    break;
                default:
                    break;
            }
            cube.OnDraw(appContext);
            cube.DrawAxis(appContext,rotAxisSt,rotAxisEd);
        };
};

class ProjectionCmd : public ICommand
{
    private:
        Cube cube;
        Vector3f rotAxis;
        Vector3f rotAxisSt,rotAxisEd;
    public: 
        ProjectionCmd(){};
        ProjectionCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            cube.ReadVertex();
            cube.ReadFaces();
            
            cube.getTransform().Scale(100,100,100);
            cube.getTransform().Translate(100,100,100);

            rotAxisEd = cube.getVerts()[4];
            rotAxisSt = cube.getVerts()[2];
            rotAxis = rotAxisEd - rotAxisSt;

            cube.DrawCube(appContext,ProjectionType::Perspective);
        }
        virtual void Update()
        {
           
        }
        virtual void OnKeyDown(int key){};
};

class TriangleZBufferCmd : public ICommand
{
    public: 
        TriangleZBufferCmd(){};
        TriangleZBufferCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            Triangle angle1(Vector3f(400,450,150),Vector3f(10,200,50),Vector3f(180,89,67),Vector3f(1,0,0),Vector3f(1,0,0),Vector3f(1,0,0));
            Triangle angle2(Vector3f(380,30,150),Vector3f(240,460,250),Vector3f(100,400,167),Vector3f(0,1,0),Vector3f(0,1,0),Vector3f(0,1,0));
            Triangle angle3(Vector3f(140,180,50),Vector3f(400,20,250),Vector3f(600,350,250),Vector3f(0,0,1),Vector3f(0,0,1),Vector3f(0,0,1));
            Triangle angle4(Vector3f(400,550,150),Vector3f(10,300,50),Vector3f(180,89,67),Vector3f(1,0,0),Vector3f(1,0,0),Vector3f(1,0,0));
            //appContext->getTriangleRender()->FillTriangleBox(angle1);
            //appContext->getTriangleRender()->FillTriangle(angle1,Vector3f(1,0,0));
            //appContext->getTriangleRender()->FillTriangleBox(angle2);
            //appContext->getTriangleRender()->FillTriangleBox(angle3);
            //appContext->getTriangleRender()->FillTriangle(angle3,Vector3f(1,0,0));
            appContext->getTriangleRender()->FillTriangleBoxZBuffer(angle1);
            appContext->getTriangleRender()->FillTriangleBoxZBuffer(angle2);
            appContext->getTriangleRender()->FillTriangleBoxZBuffer(angle3);
        }

        virtual void Update(){};
        virtual void OnKeyDown(int key){};
};

class CubeCullCmd : public ICommand
{
    private:
        Cube cube;
        Vector3f rotAxis;
        Vector3f rotAxisSt,rotAxisEd;
    public: 
        CubeCullCmd(){};
        CubeCullCmd(AppContext* context)
        {
            appContext = context;
        }
        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            cube.ReadVertex();
            cube.ReadFaces();
            
            cube.getTransform().Scale(100,100,100);
            cube.getTransform().Translate(100,100,100);

            rotAxisEd = cube.getVerts()[4];
            rotAxisSt = cube.getVerts()[2];
            rotAxis = rotAxisEd - rotAxisSt;

            cube.DrawCubeCull(appContext);
        }
        virtual void Update()
        {
           
        }
        virtual void OnKeyDown(int key){};
};

class CubeCullFillCmd : public ICommand
{
    private:
        Cube cube;
        Vector3f center;
    public: 
        CubeCullFillCmd(){};
        CubeCullFillCmd(AppContext* context)
        {
            appContext = context;
        }

        virtual void Excute()
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            cube.ReadVertColor();
            cube.ReadFaces();
            
            cube.getTransform().Scale(100,100,100);
            cube.getTransform().Translate(200,200,0);

            center = (cube.getVertData()[4].pt + cube.getVertData()[2].pt) / 2;

            cube.DrawCubeFill(appContext);
        }

        virtual void Update()
        {
           
        }

        virtual void OnKeyDown(int key)
        {
            appContext->getRasterizer()->clear(BufferType::Color | BufferType::Depth);
            switch (key)
            {
                case 'a':
                    cube.getTransform().Translate(0,-10,0);
                    break;
                case 's':
                    cube.getTransform().Translate(10,0,0);
                    break;
                case 'd':
                    cube.getTransform().Translate(0,10,0);
                    break;
                case 'w':
                    cube.getTransform().Translate(-10,0,0);
                    break;
                case 'q':
                    cube.getTransform().Translate(-center.x(),-center.y(),-center.y());
                    cube.getTransform().RotateX(-5);
                    cube.getTransform().Translate(center.x(),center.y(),center.y());
                    break;
                case 'e':
                    cube.getTransform().Translate(-center.x(),-center.y(),-center.y());
                    cube.getTransform().RotateX(5);
                    cube.getTransform().Translate(center.x(),center.y(),center.y());
                    break;
                case 'z':
                    cube.getTransform().rotatePointAroundArbitraryAxis(center,Vector3f(0,1,0),50);
                    break;
                case 'x':
                   cube.getTransform().rotatePointAroundArbitraryAxis(center,Vector3f(0,1,0),-50);
                    break;
                default:
                    break;
            }
            center = (cube.getVertData()[4].pt + cube.getVertData()[2].pt) / 2;
            cube.DrawCubeFill(appContext);
        };
};