#pragma once

#include <array>
#include<Eigen/Eigen>

template<int TI>
class Patch
{
    public:
        std::array<int,TI> ptIndex;
};

template<int T1,int T2>
class Patch2
{
public:
    Eigen::Matrix<int, T1, T2> ptIndex;
};
