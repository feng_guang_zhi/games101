#pragma once

#include "AppContext.hpp"
#include <array>
#include <Transform2.hpp>

class Pentagram
{
    public:
        Pentagram(){};
        virtual ~Pentagram(){};

        void OnDraw(AppContext* context);
        void ReadPoints();
        std::vector<Eigen::Vector2f>* getVerts(){ 
            return &pts;
        }
    private:
        std::vector<Eigen::Vector2f> pts;
        Transform2 transform;
    public:
        Transform2 getTransform(){
            return transform;
        }
};