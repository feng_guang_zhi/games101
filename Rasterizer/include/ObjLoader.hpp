#pragma once

#include "CommonData.hpp"
#include <iostream>
#include <fstream>
#include <string>

class ObjLoader
{
public:
    ObjLoader();
    virtual ~ObjLoader();
    bool LoadFile(std::string path);
private:
    //Generate vertices from a list of positions,
    //texcoords, normals and a face line
    void GenVerticesFromRawOBJ(std::vector<VertexData>& oVerts,
                                const std::vector<Vector3f>& iPositions,
                                const std::vector<Vector2f>& iTCoords,
                                const std::vector<Vector3f>& iNormals,
                                std::string icurline);
    //Triangulate a list of vertices into a face by printing
    //inducies corresponding with triangles within it
    void VertexTriangluation(std::vector<unsigned int>& oIndices,
                                const std::vector<VertexData>& iVerts);
    // Load Materials from .mtl file
    bool LoadMaterials(std::string path);
private:
    // Loaded Mesh Objects
    std::vector<MeshData> loadedMeshes;
    // Loaded Vertex Objects
    std::vector<VertexData> loadedVertices;
    // Loaded Index Positions
    std::vector<int> loadedIndices;
    // Loaded Material Objects
    std::vector<MaterialData> loadedMaterials;
public:
    std::vector<MeshData> getLoadedMeshes(){
        return loadedMeshes;
    }
    std::vector<VertexData> getLoadedVertexData(){
        return loadedVertices;
    }
    std::vector<int> getLoadedIndices(){
        return loadedIndices;
    }
    std::vector<MaterialData> getLoadedMaterials(){
        return loadedMaterials;
    }
};