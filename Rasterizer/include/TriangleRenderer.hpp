#pragma once

#include <Eigen/Eigen>
#include "Rasterizer.hpp"
#include "Triangle.hpp"
#include "LineRenderer.hpp"
#include "Util.hpp"
#include "ColorVec.h"
#include "CommonData.hpp"
#include "Lighting.hpp"
#include <cmath>
#include <algorithm>
#include <array>

using Vector3f = Eigen::Vector3f;

class TriangleRenderer{
    private:
        Rasterizer* rasterizer;
        LineRenderer* lineRender;
        ColorVec3* spanLeft;
        ColorVec3* spanRight;
        int edgeidx;
        std::array<VertexData,3> triData;
    public:
        TriangleRenderer(Rasterizer* rast){
            rasterizer = rast;
            edgeidx = 0;
        }

        TriangleRenderer(Rasterizer* rast,LineRenderer* lineRenderer){
            rasterizer = rast;
            lineRender = lineRenderer;
            edgeidx = 0;
        }

        void SetTriangleVertData(VertexData* vertData);
        void PhongShader(Vector3f eys,Lighting* lighting);

        void FillTriangle(Triangle& triangle,Vector3f color);
        void FillTriangleBoxZBuffer(Triangle& triangle);
        void FillTriangleBox(Triangle& triangle);
        void FillTriangleBoxLerp(Triangle& triangle);
        void edgeFlag(const Vector3f& start,const Vector3f& end,bool isLeft);

        void FillLerp(Triangle& triangle);
        void edgeFlagLerp(const ColorVec3& start,const ColorVec3& end,bool isLeft);
};