#pragma once

#include<Eigen/Eigen>
#include <cmath>
#include <iostream>
#include "Material.hpp"
#include "LightSource.hpp"

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;

class Lighting
{
public:
    Lighting();
    Lighting(int lightNum);
    virtual ~Lighting();
    void SetLightNum(int lightNum);
    Material* getMaterial()
    {
        return material;
    }
    LightSource* getLightSource()
    {
        return lightSource;
    }
    //计算光照
    Vector3f Illuminate(Vector3f viewPt,Vector3f pt,Vector3f ptCol,Vector3f normal,Material* material);
private:
    //光源数
    int lightNum;
    //光源数组
    LightSource* lightSource;
    //环境光
    Vector3f ambient;
    //材质属性
    Material* material;
};