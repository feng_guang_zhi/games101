#pragma once

#include<Eigen/Eigen>
#include "Face4.hpp"
#include "ColorVec.h"
#include "AppContext.hpp"
#include "Transform3.hpp"

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;

class Sphere
{
public:
    Sphere();
    virtual ~Sphere();
    void SetRecursion(int number);
    void ReadVertex();
    void ReadFaces();
    void SubDivide(AppContext* context,Vector3f p0,Vector3f p1,Vector3f p2,int resursion);
    void Normalize(Vector3f& pt);
    void Draw(AppContext* context);
    void DrawTriangle(AppContext* context,Vector3f p0,Vector3f p1,Vector3f p2);

    Transform3 getTransform()
    {
        return transform;
    }
    std::vector<VertexData> getVertData() const
    {
        return verts;
    }

    std::vector<Face3> getFaces() const
    {
        return faces;
    }
private:
    double radius;
    std::vector<VertexData> verts;
    int resursionNum;
    std::vector<Face3> faces;
    Transform3 transform;
};

