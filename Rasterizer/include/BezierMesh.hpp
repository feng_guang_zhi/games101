#pragma once

#include "AppContext.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class BezierMesh
{
public:
    BezierMesh();
    virtual ~BezierMesh();
    virtual void ReadCtrlPts(){};
    virtual void ReadCtrlPtsMat(const Eigen::Matrix<Vector3f, 4, 4>& mat);
    void ReadCoffients();
    Eigen::Matrix<Vector3f, 4, 4> leftMultiplyMat(const Eigen::Matrix<double, 4, 4>& cMat,const Eigen::Matrix<Vector3f, 4, 4>& ptMat);
    Eigen::Matrix<Vector3f, 4, 4> rightMultiplyMat(const Eigen::Matrix<Vector3f, 4, 4>& ptMat,const Eigen::Matrix<double, 4, 4>& cMat);
protected:
    Eigen::Matrix<double, 4, 4> coffients;
    //三维控制点
    Eigen::Matrix<Vector3f, 4, 4> matrix;
};