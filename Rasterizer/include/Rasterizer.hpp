#pragma once

#include "Triangle.hpp"
#include "Triangle2.hpp"
#include "Texture.hpp"
#include "Util.hpp"
#include "Shader.hpp"
#include "ShaderFunc.hpp"

#include <algorithm>
#include <iostream>
#include <Eigen/Eigen>
#include <optional>

using Vector3i = Eigen::Vector3i;
using Vector2i = Eigen::Vector2i;
using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;
using Vector4f = Eigen::Vector4f;
using Matrix4f = Eigen::Matrix4f;
using Matrix3f = Eigen::Matrix3f;

enum class BufferType{
    Color = 1,
    Depth = 2
};

enum class Primitive{
    Line,
    Triangle
};

inline BufferType operator | (BufferType a,BufferType b){
    return BufferType(int(a) | int(b));
}

inline BufferType operator & (BufferType a,BufferType b){
    return BufferType(int(a) & int(b));
}

//再包装一层，为了区别是哪种类型的id 索引，而不是直接用int
//直接用int 就区分不出来
struct PosBufId{
    int posId = 0;
};

struct IndexBufId{
    int indexId = 0;
};

class Rasterizer{
    public:
        Rasterizer(int w,int h);

        PosBufId loadPositions(const std::vector<Vector3f>& positions);
        IndexBufId loadIndices(const std::vector<Vector3i>& indices);

        void setModel(const Matrix4f& m);
        void setView(const Matrix4f& m);
        void setProjection(const Matrix4f& m);

        void setPixel(const Vector3f& pt,const Vector3f& color);
        void setPixel(int x,int y,const Vector3f& color);
        Eigen::Vector3f getPixel(int x,int y);
        Eigen::Vector3f getPixel(Vector2f xy);

        void setZBuffer(Vector2f xy,float zvalue);
        void setZBuffer(int x,int y,float zvalue);
        double getZBuffer(Vector2f xy);
        double getZBuffer(int x,int y);

        void clear(BufferType buffType);

        std::vector<Vector3f>& getFrameBuffer(){
            return frameBuffer;
        }

        int getBufferIndex(int x,int y);

        void rasterizeTriangle(const Triangle2 &triangle, const std::array<Vector3f, 3> &viewPos);
        void drawTriangles(std::vector<Triangle2*> &triangleList);
        void setTexture(Texture* tex){ mtexture = tex;}
        void setVertexShader(std::function<Vector3f(VertexShader)> vertShader);
        void setFragmentShader(std::function<Vector3f(FragmentShader)> fragShader);

    private:
        Matrix4f model;
        Matrix4f view;
        Matrix4f projection;

        std::map<int,std::vector<Vector3f>> posBuf;
        std::map<int,std::vector<Vector3i>> indicesBuf;

        std::vector<Vector3f> frameBuffer;
        std::vector<float> depthBuffer;

        std::function<Vector3f(FragmentShader)> fragmentShaderFunc;
        std::function<Vector3f(VertexShader)> vertexShaderFunc;

        int width;
        int height;

        Texture* mtexture = nullptr;

        int nextId = 0;
        int getNextId()
        {
            return nextId++;
        }
};