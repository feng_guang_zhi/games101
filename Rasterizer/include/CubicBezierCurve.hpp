#pragma once

#include "AppContext.hpp"

using Vector3f = Eigen::Vector3f;
using Vector2f = Eigen::Vector2f;

class CubicBezierCurve
{
    public:
        CubicBezierCurve(){};
        virtual ~CubicBezierCurve(){};
        void ReadPoints();
        void ReadCirclePts();
        void DrawCurveIterator(AppContext* context);
        void DrawCurveFunc(AppContext* context);
        void ClearPts()
        {
            ctrlPts.clear();
        }
    private:
        std::vector<Vector2f> ctrlPts;
};