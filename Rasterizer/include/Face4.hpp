#pragma once

#include <array>

template<int F>
class Face
{
    public:
        std::array<int,F> vertIdxs;
        int FNum = F;
};

class Face4 : public Face<4>
{
    public:
        Face4();
        Face4(int a,int b,int c,int d)
        {
            vertIdxs[0] = a;
            vertIdxs[1] = b;
            vertIdxs[2] = c;
            vertIdxs[3] = d;
        }
        virtual ~Face4(){};
};

class Face3 : public Face<3>
{
    public:
        Face3();
        Face3(int a,int b,int c)
        {
            vertIdxs[0] = a;
            vertIdxs[1] = b;
            vertIdxs[2] = c;
        }
        virtual ~Face3(){};
};