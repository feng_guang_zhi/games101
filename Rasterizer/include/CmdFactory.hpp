#include <iostream>
#include <map>
#include <memory>
#include <string>
#include "ICommand.hpp"

// 工厂类
struct CmdFactory
{
    public:
    template<typename T, typename... Args>
    static std::shared_ptr<T> Create(Args&&... args) 
    {
        static_assert(std::is_base_of<ICommand, T>::value, "T must be an Animal type");
        std::string key = typeid(T).name();
        if(instances.find(key) != instances.end())
        {
            return std::static_pointer_cast<T>(instances[key]);
        }
        // 使用args构造实例
        auto instance = std::make_shared<T>(std::forward<Args>(args)...);
        // 存储实例
        instances[key] = instance;
        return instance;
    }

    static std::shared_ptr<ICommand> Get(const std::string& key) {
        return instances[key];
    }
private:
    static std::map<std::string, std::shared_ptr<ICommand>> instances;
};

std::map<std::string, std::shared_ptr<ICommand>> CmdFactory::instances;